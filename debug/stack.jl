
using Revise
using LoopVectorization
using MinfluxBED

prior = ParamGrid(
  0:5:500,
  0:5:500;
  alpha = 0.5:0.1:1.5,
  weights = GaussProfile(; fwhm = 200),
)

protocol = MinstedProtocol((250, 250))

stop = trace -> trace.photons >= 10

sim = MinfluxBED.Simulator(protocol, prior; stop)

MinfluxBED.LocalizationStack(sim, 5)

stack = localize(sim, [Param((100, 100)) for _ in 1:1000])

# display(stack.traces[1])
# display(stack.traces[2])

stack[7, 2]

using Serialization

serialize("teststack.mbed", stack)
stack2 = deserialize("teststack.mbed")