using Revise, BenchmarkTools
using Statistics, Printf

using NanoLoc
# using LoopVectorization

ops = [
  ("mean", prior -> mean(prior)),
  ("mode", prior -> findmax(prior)),
  ("total var", prior -> TotalVar()(prior)),
  ("total entropy", prior -> TotalEntropy()(prior)),
  ("position entropy", prior -> PositionEntropy()(prior)),
]

designs = [
  PoissonDesign((0, 0)),
  BernoulliDesign((0, 0)),
  ExponentialDesign((0, 0)),
  CircleDesign((0, 0), 10),
  CompoundDesign(PoissonDesign((0, 0)), PoissonDesign((10, 10))),
  MultinomialDesign(10, PoissonDesign((0, 0)), PoissonDesign((10, 10))),
  settiming(
    MultinomialDesign(10, PoissonDesign((0, 0)), PoissonDesign((10, 10))),
  ),
  settiming(CircleDesign((0, 0), 10)),
  settiming(CircleDesign((0, 0), 10), (timeout = 1.0, relative = true)),
]

policies = [
  Minflux(70),
  MinfluxBayes(),
  MinfluxBED(; samples = 1000),
  MinstedBayes(),
  MinstedBED(; samples = 1000),
  SimpleBED(
    BernoulliDesign,
    [GaussProfile(; fwhm) for fwhm in [50, 100]];
    samples = 1000,
  ),
]

function getatypes()
  atypes = [:default32, :default64]
  if @isdefined CUDA
    push!(atypes, :cuda32, :cuda64)
  end
  return atypes
end

function benchmark(
  n = 250,
  m = 250;
  atypes = getatypes(),
  designs = designs,
  policies = policies,
)
  println("running benchmarks for parameter grid of size $(n)x$(m)x11")

  println()
  println("1. basic operations")
  for (name, op) in ops
    println(" $name")
    for atype in atypes
      prior = ParamGrid(1:n, 1:m; alpha = 0:10, atype)
      @printf " %10s:" atype
      @btime $op($prior)
    end
  end

  println()
  println("2. posterior calculation")
  for design in designs
    println(" $(NanoLoc.pp(design))")
    for atype in atypes
      prior = ParamGrid(1:n, 1:m; alpha = 0.5:0.1:1.5, atype)
      @printf " %10s:" atype
      @btime posterior($prior, $design, rand($design, $prior))
    end
  end

  println()
  println("3. localization (10 photons)")
  for policy in policies
    println(" $(NanoLoc.policyname(policy))")
    for atype in atypes
      method = BayesGrid(1:n, 1:m; alpha = 0.5:0.1:1.5, atype)
      param = Param((n / 2, m / 2); alpha = 1, noise = 0.1)
      @printf " %10s:" atype
      @btime localize($policy, $method, $param, stop = t -> t.photons >= 10)
    end
  end
end
