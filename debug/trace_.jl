
using Revise, MinfluxBED, CUDA

method = BayesGrid(0:3:300, 0:3:300; alpha = 0.5:0.1:1.5)
protocol = MinfluxProtocol(50, GaussProfile(; fwhm = 60))
protocol = MinstedProtocol()
protocol = RastminProtocol(50, GaussProfile(; fwhm = 60))
protocol = MinfluxBayesProtocol()
protocol = MinstedBayesProtocol()
protocol = MinstedBEDProtocol(;
  fwhm_min = 24,
  samples = 500,
  observations = 8,
  fwhm_to_radius = [1.5],
)

method2 = BayesGrid(0:1:300, 0:1:300; alpha = 0.5:0.1:1.5, atype = :cuda)

method1 = BayesGrid(0:6:300, 0:6:300; alpha = :known, noise = :known)
method2 =
  BayesGrid(0:3:300, 0:3:300; alpha = :known, noise = :known, atype = :cuda)

# profiles = [GaussProfile(;alpha = 10, fwhm) for fwhm in [24, 40, 80, 160, 300]]
# protocol = SimpleBEDProtocol(ExponentialDesign, BernoulliDesign, profiles, normalsbr = 1.6, samples = 1000, gridsize = 10)

stop = t -> t.photons >= 50

# @time t = localize(protocol, method, Param((100, 100), noise = 0.1); stop)
# @time t2 = localize(t, method2)
# @time t3 = localize(t, method3)

protocol = MinstedBayesProtocol(; fwhm_to_radius = 1.5, normalsbr = 1)
protocol = MinstedBEDProtocol(;
  fwhm_min = 40,
  samples = 1000,
  observations = 8,
  fwhm_to_radius = [1.5],
  normalsbr = 1,
)

# @time t1 = localize(protocol, method1, Param((143, 52), noise = 0.1); stop, verbose = false)
# @time t2 = localize(protocol, method2, Param((143, 52), noise = 0.1); stop, verbose = false)

fwhm_to_radius = 1.5
design = CircleDesign((0, 0), 1, GaussProfile(; fwhm = fwhm_to_radius))
s = [sum(MinfluxBED.intensity(design, Param((0, x)))) for x in 0:0.1:2]
# display(s / s[1])

protocol1 = MinfluxBEDProtocol(; samples = 1000, gridsize = 10, levels = 2)
protocol2 = MinstedBayesProtocol()

# t1 = localize(protocol1, method2, Param((143, 52), noise = 0.1); stop, verbose = false)
# t2 = localize(protocol2, method2, Param((143, 52), noise = 0.1); stop, verbose = false)

# t1 = localize(protocol1, method2, Param((143, 52), noise = 0.1); stop, verbose = false)
# t2 = localize(protocol1, method2, Param((143, 52), noise = 0.1); stop, verbose = false)

params = fill(Param((143, 52); noise = 0.1), 1000)

# stack = localize(protocol2, method2, params; stop, verbose = true)

# @btime CUDA.@sync MinfluxBED.update(t2.method, t2.prior, t2.ms[1], t2.ms)
# @btime CUDA.@sync MinfluxBED.randbg(t2.ms[1].design, t2.param)
# @btime CUDA.@sync MinfluxBED._computemetrics(t2, (;))
# @btime CUDA.@sync t2.protocol(t2)

# p = t2.protocol

# x = @btime CUDA.@sync MinfluxBED.estimate(t2.state).x
# r = @btime CUDA.@sync MinfluxBED.estimateextent(t2.state) / 2
# r = clamp(r, 20, 300)

# design = @btime CUDA.@sync MinfluxDesign(x, r, p.profile; p.spots, p.timed, photons = 1)
# @btime CUDA.@sync MinfluxBED.scalealpha(design; p.normalsbr)

# @btime CUDA.@sync begin
#   x = MinfluxBED.estimate(t2.state).x
#   r = MinfluxBED.estimateextent(t2.state) / 2
#   r = clamp(r, 20, 300)

#   design = MinfluxDesign(x, r, p.profile; p.spots, p.timed, photons = 1)
#   MinfluxBED.scalealpha(design; p.normalsbr)
# end

stop = t -> t.photons >= 50

params = fill(Param((150, 50); noise = 0.0), 10000)

profiles = [GaussProfile(; fwhm) for fwhm in [40, 80, 160, 300]]
protocol2 = MinfluxProtocol(50)
protocol2 = MinfluxBayesProtocol(; rmin = 50)
protocol2 = SimpleBEDProtocol(
  ExponentialDesign,
  BernoulliDesign,
  profiles;
  samples = 100,
  normalsbr = 2,
  levels = 2,
)

protocol2 = MinstedBayesProtocol(; fwhm_min = 40, fwhm_to_radius = 1.5)
# protocol2 = MinstedBEDProtocol(fwhm_min = 40, fwhm_to_radius = [1.5], samples = 100, levels = 2)
# protocol2 = MinstedProtocol(fwhm_min = 40, fwhm_to_radius = 2)

# st1 = @time localize(protocol2, method1, params[1:1000]; stop, verbose = true, threads = true)

protocol1 = MinfluxProtocol(50)
protocol2 = MinfluxBayesProtocol(; rmin = 50, rmax = 70, radius_to_std = 1)
# protocol3 = MinfluxBEDProtocol(rmin = 50, rmax = 90, samples = 500, levels = 2)

# st1 = @time localize(protocol1, method1, params[1:1000]; stop, verbose = true, threads = true)
# st2 = @time localize(protocol2, method1, params[1:1000]; stop, verbose = true, threads = true)
# st3 = @time localize(protocol3, method1, params[1:1000]; stop, verbose = true, threads = true)

method = BayesGrid(0:20:1000, 0:20:1000; alpha = 0.5:0.1:1.5, noise = :known)

params =
  rand(MinfluxBED.init(method, Param((0, 0); alpha = 1, noise = 0.1)), 10000)

profiles = [GaussProfile(; fwhm) for fwhm in [40, 80, 160, 300]]
protocol1 = MinstedBayesProtocol(; fwhm_min = 40, fwhm_to_radius = 1.5)
protocol2 = SimpleBEDProtocol(
  BernoulliDesign,
  profiles;
  samples = 100,
  normalsbr = 2,
  levels = 2,
)

st1 = @time localize(
  protocol1,
  method,
  params;
  stop,
  verbose = true,
  threads = true,
)
display(st1)
st2 = @time localize(
  protocol2,
  method,
  params;
  stop,
  verbose = true,
  threads = true,
)
