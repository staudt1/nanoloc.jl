using Revise
using StatsBase
using CUDA

using MinfluxBED

abstract type CuParamDist{D, F} <: ParamDist{D, F} end

struct CuParamList{D, F} <: CuParamDist{D, F}
  params::CuArray{Param{D, F}}
  weights::CuArray{F}
end

function Base.convert(::Type{CuParamList{D, F}}, p::ParamDist{D}) where {D, F}
  params = convert(CuVector{Param{D, F}}, params(p))
  weights = convert(CuVector{F}, weights(p))
  return CuParamList(params, weights)
end

function Base.convert(::Type{ParamList{D, F}}, p::CuParamList{D}) where {D, F}
  params = convert(Vector{Param{D, F}}, params(p))
  weights = convert(Vector{F}, weights(p))
  return ParamList(params, weights)
end

function Base.convert(::Type{ParamList}, p::CuParamList{D, F}) where {D, F}
  return convert(ParamList{D, F}, p)
end

function Base.convert(::Type{ParamDist{D, F}}, p::CuParamList{D}) where {D, F}
  return convert(CuParamList{D, F}, p)
end

function ParamDist(list::CuParamList{D, F}, weights::CuVector{F}) where {D, F}
  return CuParamList(params(list), weights)
end

function CUDA.cu(p::ParamList{D})
  return convert(CuParamList{D, Float32}, p)
end

struct CuParamGrid{D, F} <: CuParamGrid
  x::NTuple{D, CuVector{F}}
  alpha::CuVector{F}
  noise::CuVector{F}
  params::CuVector{Param{D, F}}
  weights::CuVector{F}
end

function Base.convert(::Type{CuParamGrid{D, F}}, p::ParamGrid{D}) where {D, F}
  x = convert.(CuVector{F}, p.x)
  alpha = convert(CuVector{F}, p.alpha)
  noise = convert(CuVector{F}, p.noise)
  weights = convert(CuVector{F}, p.weights)
  params = convert(CuVector{Param{D, F}}, p.params)
  return CuParamGrid(x, alpha, noise, params, weights)
end

function Base.convert(::Type{ParamGrid{D, F}}, p::CuParamGrid{D}) where {D, F}
  x = convert.(Vector{F}, p.x)
  alpha = convert(Vector{F}, p.alpha)
  noise = convert(Vector{F}, p.noise)
  weights = convert(Vector{F}, p.weights)
  params = convert(Vector{Param{D, F}}, p.params)
  return ParamGrid(x, alpha, noise, params, weights)
end

function Base.convert(::Type{ParamGrid}, p::CuParamGrid{D, F}) where {D, F}
  return convert(ParamGrid{D, F}, p)
end

function ParamDist(grid::CuParamGrid{D, F}, weights::CuVector{F}) where {D, F}
  return CuParamGrid(grid.x, grid.alpha, grid.noise, params(grid), weights)
end

function CUDA.cu(p::ParamGrid{D})
  return convert(CuParamGrid{D, Float32}, p)
end

function CuParamGrid(args...; kwargs...)
  grid = ParamGrid(args...; kwargs...)
  return convert(CuParamGrid, grid)
end
