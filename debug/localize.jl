
using Revise, LoopVectorization, Statistics

using MinfluxBED

## Prior distribution
sigma = 150
center = (250, 250)
weight = (x, _, _) -> exp(-sum(abs2, x .- center) / 2sigma^2)
prior = ParamGrid(0:5:500, 0:5:500; alpha = 0.5:0.1:2, weight = weight)

# True parameter
truth = Param((180, 220); alpha = 1.7)
@show truth

# Localization stopping criterion
stop = stack -> stack.photons >= 20

## Ordinary MINFLUX protocol
protocol =
  stack -> begin
    profile = QuadraticProfile(; width = 400, alpha = 2)
    CompoundDesign(
      PoissonDesign((100, 100), profile),
      PoissonDesign((400, 100), profile),
      PoissonDesign((250, 400), profile),
      PoissonDesign((250, 250), profile),
    )
  end

stack = localize(protocol, prior, truth; stop, verbose = false)
display(stack)

## Naive BED protocol
design_options = [
  PoissonDesign((x1, x2), GaussProfile(; width, alpha = 1)) for
  x1 in 0:50:500, x2 in 0:50:500, width in [50, 100, 150]
]

protocol =
  stack -> begin
    utils = map(design_options) do design
      return utility(PositionEntropy(), design, stack.dist, 0:2)
    end
    util, index = findmin(utils)
    design_options[index]
  end

stack = localize(protocol, prior, truth; stop, verbose = false)
display(stack)
