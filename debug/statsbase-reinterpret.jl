
using StatsBase

struct Observation
  a::Float64
  b::Float64
end

obs = [Observation(1.0, 2.0), Observation(2.0, 4.0)]
weights = [0.5, 0.5]

data = reinterpret(reshape, Float64, obs)
typeof(data) <: DenseMatrix
cov(data; dims = 2, corrected = false) # works as expected
# cov(data, Weights(weights), 2) # fails

data = unsafe_wrap(Array, pointer(data), size(data))
cov(data, Weights(weights), 2) # works as expected
