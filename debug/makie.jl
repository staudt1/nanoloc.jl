using Revise
using MinfluxBED
using GLMakie
using ColorSchemes
using Observables

cmap = ColorSchemes.viridis

#
# Auxiliary functions
#

function aspect_ratio(grid::ParamGrid{2})
  return abs(grid.x[2][end] - grid.x[2][1]) / abs(grid.x[1][end] - grid.x[1][1])
end

function design_geometry(design::PoissonDesign{2})
  return [(x = design.x, width = design.profile.width)]
end

function design_geometry(design::CompoundDesign{2})
  return mapreduce(design_geometry, vcat, design.designs)
end

#
# Plotting
#

function add_widget_prior!(layout, context)
  prior = context[:prior]

  # TODO: handle this via theming!
  linecolor = cmap[0.3]
  fillcolor = (cmap[0.25], 0.4)

  weights_x = @lift MinfluxBED.marginalize($prior, :x)
  weights_x1 = @lift MinfluxBED.marginalize($prior, :x1)
  weights_x2 = @lift MinfluxBED.marginalize($prior, :x2)
  weights_alpha = @lift MinfluxBED.marginalize($prior, :alpha)
  weights_noise = @lift MinfluxBED.marginalize($prior, :noise)

  data_x1 = @lift $(prior).x[1]
  data_x2 = @lift $(prior).x[2]
  data_alpha = @lift $prior.alpha
  data_noise = @lift $prior.noise

  data_x = lift(prior) do p
    return reshape([(x1, x2) for x1 in p.x[1], x2 in p.x[2]], :)
  end

  axis_x = Axis(
    layout[2, 1];
    xgridvisible = true,
    ygridvisible = true,
    backgroundcolor = cmap[0.0],
    xlabel = "nm",
    ylabel = "nm",
  )

  axis_x1 = Axis(
    layout[1, 1];
    limits = @lift((extrema($(prior).x[1]), (0, nothing))),
    xgridvisible = false,
    ygridvisible = false,
    yticklabelsvisible = false,
    yticksvisible = false,
    xticklabelsvisible = false,
    xticksvisible = false,
    topspinevisible = false,
    leftspinevisible = false,
    rightspinevisible = false,
  )

  axis_x2 = Axis(
    layout[2, 2];
    limits = @lift(((0, nothing), extrema($(prior).x[2]))),
    xgridvisible = false,
    ygridvisible = false,
    yticklabelsvisible = false,
    yticksvisible = false,
    xticklabelsvisible = false,
    xticksvisible = false,
    topspinevisible = false,
    bottomspinevisible = false,
    rightspinevisible = false,
  )

  layout_alpha_noise = layout[3, 1:end] = GridLayout()

  axis_alpha = Axis(
    layout_alpha_noise[1, 1];
    limits = lift(_ -> (nothing, (0, nothing)), prior),
    xlabel = "brightness (alpha)",
    xgridvisible = false,
    ygridvisible = false,
    topspinevisible = false,
    rightspinevisible = false,
  )

  axis_noise = Axis(
    layout_alpha_noise[1, 2];
    limits = lift(_ -> (nothing, (0, nothing)), prior),
    xlabel = "noise level",
    xgridvisible = false,
    ygridvisible = false,
    topspinevisible = false,
    rightspinevisible = false,
  )

  # Layouting

  linkxaxes!(axis_x, axis_x1)
  linkyaxes!(axis_x, axis_x2)

  rowsize!(layout, 2, Aspect(1, aspect_ratio(grid)))
  rowsize!(layout, 1, Fixed(50))
  rowsize!(layout, 3, Fixed(100))
  colsize!(layout, 2, Fixed(50))

  rowgap!(layout, 1, 15)
  colgap!(layout, 1, 15)

  # Plot the position heatmap

  hmap = heatmap!(axis_x, data_x1, data_x2, weights_x)
  scatter!(axis_x, data_x; color = :black, markersize = 1)

  # Plot the x1-marginal distribution

  band!(axis_x1, data_x1, 0, weights_x1; color = fillcolor)
  lines!(axis_x1, data_x1, weights_x1; color = linecolor, linewidth = 2.5)
  scatter!(
    axis_x1,
    data_x1,
    weights_x1;
    color = :black,
    marker = :vline,
    markersize = 6,
  )

  # Plot the x2-marginal distribution

  band_points_a = @lift Point.(0, $data_x2)
  band_points_b = @lift Point.($weights_x2, $data_x2)
  band!(axis_x2, band_points_a, band_points_b; color = fillcolor)
  lines!(axis_x2, weights_x2, data_x2; color = linecolor, linewidth = 2.5)
  scatter!(
    axis_x2,
    weights_x2,
    data_x2;
    color = :black,
    marker = :hline,
    markersize = 8,
  )

  # Plot the colorbar

  Colorbar(
    layout[2, 3];
    colorrange = @lift((0, maximum($weights_x) * prod(length, $(prior).x))),
    tellheight = false,
  )

  # Plot the distributions of alpha and noise

  scaled_weights_alpha = @lift $weights_alpha * length($weights_alpha)
  band!(axis_alpha, data_alpha, 0, scaled_weights_alpha; color = fillcolor)
  lines!(
    axis_alpha,
    data_alpha,
    scaled_weights_alpha;
    linewidth = 2.5,
    color = linecolor,
  )
  scatter!(
    axis_alpha,
    data_alpha,
    scaled_weights_alpha;
    color = :black,
    marker = :vline,
    markersize = 4,
  )

  band!(
    axis_noise,
    data_noise,
    0,
    @lift($weights_noise * length($weights_noise));
    color = fillcolor,
  )
  lines!(
    axis_noise,
    data_noise,
    @lift($weights_noise * length($weights_noise));
    linewidth = 2.5,
    color = linecolor,
  )

  # Mark true parameters if provided

  truth_x = Observable{Vector{Point2}}([])
  truth_x1 = Observable{Vector{Float32}}([])
  truth_x2 = Observable{Vector{Float32}}([])
  truth_alpha = Observable{Vector{Float32}}([])

  on(context[:truth]; update = true) do param
    isnothing(param) && return
    truth_x[] = [Point(param.x[1], param.x[2])]
    truth_x1[] = [param.x[1]]
    truth_x2[] = [param.x[2]]
    return truth_alpha[] = [param.alpha]
  end

  scatter!(axis_x, truth_x; color = :green, strokewidth = 1)
  vlines!(axis_x1, truth_x1; color = :green, linewidth = 3)
  hlines!(axis_x2, truth_x2; color = :green, linewidth = 3)
  vlines!(axis_alpha, truth_alpha; color = :green, linewidth = 3)

  # Mark design points if provided

  design_points = Observable{Vector{Point2}}([])
  design_radii = Observable{Vector{Float32}}([])

  on(context[:design]; update = true) do design
    isnothing(design) && return
    geom = design_geometry(design)
    design_radii.val = [2sqrt(2) * g.width for g in geom] # Prevent double triggering plot update
    return design_points[] = [g.x for g in geom]
  end

  scatter!(axis_x, design_points; color = :white, strokewidth = 1)
  scatter!(
    axis_x,
    design_points;
    color = :transparent,
    marker = :circle,
    markersize = design_radii,
    markerspace = :data,
    strokewidth = 2,
    strokecolor = (:white, 0.15),
  )

  # Register axes

  context[:widget_prior] = Dict{Symbol, Any}()
  context[:widget_prior][:axis_x] = axis_x
  context[:widget_prior][:axis_x1] = axis_x1
  context[:widget_prior][:axis_x2] = axis_x2
  return context[:widget_prior][:axis_alpha] = axis_alpha
end

grid = ParamGrid(0:5:450, 0:5:450; alpha = 1:0.1:10)
design = CompoundDesign(
  PoissonDesign((100, 100); profile = GaussProfile(; alpha = 10)),
  PoissonDesign((350, 200); profile = GaussProfile(; alpha = 10)),
  PoissonDesign((200, 350); profile = GaussProfile(; alpha = 10)),
)

obs = rand(design, Param((200, 100); alpha = 5))
@show obs

prior = posterior(design, grid, obs)

context = Dict{Symbol, Any}(
  :truth => Observable{Any}(nothing),
  :prior => Observable(prior),
  :design => Observable{Any}(design),
)

fig = Figure(; font = :Ubuntu, resolution = (800, 800))
layout = fig[1, 1] = GridLayout()

add_widget_prior!(layout, context)

fig

# weights_x = MinfluxBED.marginalize(grid, :x)
# weights_x1 = MinfluxBED.marginalize(grid, :x1)
# weights_x2 = MinfluxBED.marginalize(grid, :x2)
# weights_alpha = MinfluxBED.marginalize(grid, :alpha)
# weights_noise = MinfluxBED.marginalize(grid, :noise)

# axmain = Axis(
#   layout[2,1],
#   # aspect = DataAspect(),
#   xgridvisible = true,
#   ygridvisible = true,
#   xlabel = "nm",
#   ylabel = "nm",
# )

# axtop = Axis(
#   layout[1, 1],
#   limits = (extrema(grid.x[1]), (0, nothing)),
#   xgridvisible = false,
#   ygridvisible = false,
#   yticklabelsvisible = false,
#   yticksvisible = false,
#   xticklabelsvisible = false,
#   xticksvisible = false,
#   topspinevisible = false,
#   # bottomspinevisible = false,
#   leftspinevisible = false,
#   rightspinevisible = false,
# )

# axright = Axis(
#   layout[2, 2],
#   limits = ((0, nothing), extrema(grid.x[2])),
#   xgridvisible = false,
#   ygridvisible = false,
#   yticklabelsvisible = false,
#   yticksvisible = false,
#   xticklabelsvisible = false,
#   xticksvisible = false,
#   topspinevisible = false,
#   bottomspinevisible = false,
#   # leftspinevisible = false,
#   rightspinevisible = false,
# )

# axalpha = Axis(
#   layout[3, 1],
#   limits = (nothing, (0, nothing)),
#   xlabel = "brightness (alpha)",
#   xgridvisible = false,
#   ygridvisible = false,
#   topspinevisible = false,
#   rightspinevisible = false,
# )

# linecolor = cmap[0.3]
# fillcolor = (cmap[0.25], 0.4)

# hmap = heatmap!(axmain, grid.x[1], grid.x[2], weights_x)

# band!(axtop, grid.x[1], 0, weights_x1, color = fillcolor)
# lines!(axtop, grid.x[1], weights_x1, color = linecolor, linewidth = 2.5)

# band!(axright, Point.(0, grid.x[2]), Point.(weights_x2, grid.x[2]), color = fillcolor)
# lines!(axright, weights_x2, grid.x[2], color = linecolor, linewidth = 2.5)

# # ONLY DO THIS IF THERE ARE NOT TOO MANY POINTS ON SCREEN!
# if prod(length, grid.x) <= 1e5
#   points = reshape([(x, y) for x in grid.x[1], y in grid.x[2]], :)
#   scatter!(axmain, points, color = :black, markersize = 1)
# end

# Colorbar(
#   layout[2,3],
#   colorrange = (0, maximum(weights_x) * prod(length, grid.x)),
#   tellheight = false,
# )

# band!(axalpha, grid.alpha, 0, weights_alpha * length(grid.alpha), color = fillcolor)
# lines!(axalpha, grid.alpha, weights_alpha * length(grid.alpha), linewidth = 2.5, color = linecolor)
