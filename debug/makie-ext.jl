
using Revise, LoopVectorization
using MinfluxBED
using GLMakie

noise = 0.05
noise_support = 0.05

prior = ParamGrid(
  0:10:500,
  0:10:500;
  alpha = 0.5:0.1:1.5,
  noise = noise_support,
  weights = GaussProfile(; fwhm = 250),
)
protocol = MinstedProtocol((250, 250); rmax = 150, rmin = 8)
truth = Param((270, 220); alpha = 1, noise = noise)
stop = stack -> stack.photons >= 50

# protocol = MinfluxDesign((250, 250), DonutProfile(alpha = 0.75, fwhm = 300), radius = 70)
# protocol = stack -> begin
#   x1s = range(stack.dist.x[1][1], stack.dist.x[1][end], 11)
#   x2s = range(stack.dist.x[2][1], stack.dist.x[2][end], 11)
#   # fwhms = [25, 50, 100, 200]
#   # fwhms = [100]
#   alpha = 1
#   profiles = [DonutProfile(; fwhm = 100, alpha), GaussProfile(; fwhm = 300)]
#   # profiles = [GaussProfile(; fwhm = fwhm) for fwhm in [20, 50, 100, 150, 200, 250]]
#   designs = [PoissonDesign((x1, x2), profile) for x1 in x1s, x2 in x2s, profile in profiles]
#   utilities = zeros(Float64, size(designs)...)
#   Threads.@threads for I in CartesianIndices(utilities)
#     utilities[I] = utility(TotalEntropy(), designs[I], stack.dist, 0:2)
#   end
#   ut, index = findmin(utilities)
#   designs[index]
# end

stack = localize(
  protocol,
  prior,
  truth;
  stop,
  verbose = true,
  masstol = 5e-3,
  stdtol = 1,
)

fig = Figure()
lp = plot!(fig[1, 1], stack)

fig
