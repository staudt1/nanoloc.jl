
using Documenter, NanoLoc
using .Remotes: GitLab

makedocs(;
  sitename = "NanoLoc Documentation",
  repo = GitLab("gitlab.gwdg.de", "staudt1", "nanoloc.jl"),
  pages = [
    "Overview" => "index.md",
    "Traces and Stacks" => "traces-stacks.md",
    "Designs and Policies" => "designs-policies.md",
    "Inference" => "inference.md",
    "Examples" => "examples.md",
    "References" => "references.md",
  ],
  format = Documenter.HTMLWriter.HTML(;
    size_threshold_ignore = ["references.md"],
  ),
)
