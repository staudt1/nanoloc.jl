
# NanoLoc

NanoLoc is a julia package for testing and developing localization strategies
for structured-illumination based super-resolution microscopy.

This package is under heavy development and not yet fully documented.
The current documentation can be found [here](https://staudt1.pages.gwdg.de/nanoloc.jl).

