
module CudaExt

using CUDA
using Random, Statistics

using NanoLoc: NanoLoc
import NanoLoc: Param, GenericParamList, GenericParamGrid
import NanoLoc: TotalEntropy
import NanoLoc: Design, Trace

NanoLoc._takenth(a::CuArray, n) = CUDA.@allowscalar a[n]
NanoLoc._striparrayparams(::Type{<:CuArray}) = CuArray

function __init__()
  NanoLoc._atypes[:cuda] = CuArray{Float64}
  NanoLoc._atypes[:cuda64] = CuArray{Float64}
  return NanoLoc._atypes[:cuda32] = CuArray{Float32}
end

"""
Cuda-supported weighted parameter list based on the `CuArray` type.
"""
const CuParamList{D, F, B} =
  GenericParamList{D, F, CuVector{Param{D, F}, B}, CuVector{F, B}}

function Base.show(io::IO, params::CuParamList)
  return print(io, "CuParamList($(length(params)))")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  params::CuParamList{D, F},
) where {D, F}
  return print(io, "CuParamList{$D, $F}($(length(params)))")
end

"""
Cuda-supported weighted parameter grid based on the `CuArray` type.
"""
const CuParamGrid{D, F, B} =
  GenericParamGrid{D, F, CuVector{Param{D, F}, B}, CuVector{F, B}}

function Base.show(io::IO, grid::CuParamGrid)
  dims = join(size(grid), "×")
  return print(io, "CuParamGrid($dims)")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  grid::CuParamGrid{D, F},
) where {D, F}
  dims = join(size(grid), "×")
  return print(io, "CuParamGrid{$D, $F}($dims)")
end

const CuParamDist{D, F, B} = Union{CuParamList{D, F, B}, CuParamGrid{D, F, B}}

function Base.rand(rng::AbstractRNG, dist::CuParamDist, n::Integer)
  weights = convert(Array, NanoLoc.weights(dist))
  indices = NanoLoc.sample_indices(rng, weights, n)
  return NanoLoc.params(dist)[indices]
end

function Base.rand(
  rng::AbstractRNG,
  design::Design{D},
  dist::CuParamDist{D, F},
  n::Integer,
) where {D, F}
  dist = NanoLoc.setatype(dist, Array)
  observations = rand(rng, design, dist, n)
  return convert(CuArray, observations)
end

# Default to broadcasting based batch utility calculation
function NanoLoc.utility(
  ::TotalEntropy,
  prior::CuParamDist{D, F},
  designs::Array{<:Design{D}},
  observations,
) where {D, F}
  return NanoLoc.utilityb(prior, designs, observations)
end

function NanoLoc.utilitys(
  ::TotalEntropy,
  prior::CuParamDist{D, F},
  designs::Array{<:Design{D}},
  observations,
) where {D, F}
  return NanoLoc.utilitysb(prior, designs, observations)
end

# TODO: refinement on GPU
function NanoLoc.refine(grid::CuParamGrid{D, F}; dims = 1:(D + 2)) where {D, F}
  grid = NanoLoc.setatype(grid, Array{F})
  grid = NanoLoc.refine(grid; dims)
  return NanoLoc.setatype(grid, CuArray{F})
end

import NanoLoc: BayesGrid, _policies_precompile
using PrecompileTools: @setup_workload, @compile_workload

@setup_workload begin
  NanoLoc._atypes[:cuda] = CuArray{Float64}
  NanoLoc._atypes[:cuda64] = CuArray{Float64}
  NanoLoc._atypes[:cuda32] = CuArray{Float32}

  _methods_precompile = [
    BayesGrid(0:10, 0:10; atype = :cuda32),
    BayesGrid(0:10, 0:10; atype = :cuda64),
    BayesGrid(0:10, 0:10; alpha = 1.0:1.1, atype = :cuda32),
    BayesGrid(0:10, 0:10; alpha = 1.0:1.1, atype = :cuda64),
  ]

  @compile_workload begin
    NanoLoc._precompile(_policies_precompile, _methods_precompile)
  end
end

end  # module CudaExt
