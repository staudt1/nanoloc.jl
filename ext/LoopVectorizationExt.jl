
module LoopVectorizationExt

using LoopVectorization
using NanoLoc: NanoLoc
import NanoLoc: PoissonDesign, MultinomialDesign, TimedMultinomialDesign
import NanoLoc: ParamGrid, ParamDist
import NanoLoc: posteriorm!, setprecision

function logfac(x::Int)
  s = 0e0
  for y in 2:x
    s += log(y)
  end
  return s
end

#
# PoissonDesign
#

# Accelerated method for calculating the posterior mass for PoissonDesigns in
# one dimension
function posteriorm!(
  post::ParamGrid{1, F},
  prior::ParamGrid{1, F},
  design::PoissonDesign{1},
  obs::Int,
)::Tuple{ParamGrid{1, F}, F} where {F}

  # Bring the buffer in shape
  @assert size(post) == size(prior) """
  The shape of the posterior distribution to be modified does not match the
  shape of the prior.
  """

  # Make sure the design uses the same floating point precision as the prior
  design = NanoLoc.setprecision(design, F)

  # Logarithm of obs!, which is needed repeatedly and thus cached
  lfac = F(logfac(obs))

  # Several length parameters needed for indexing in the loop over the grid
  lnoise = length(prior.noise)
  lalpha = length(prior.alpha)
  lx1 = length(prior.x[1])

  l1 = lx1
  l2 = l1 * lalpha

  # Variable that stores the sum of the unnormalized posterior values
  mass = zero(F)

  # In order to prevent 0 * Inf, we add a tiny epsilon to the logarithm in
  # the kernel below for obs = 0
  eps = iszero(obs) ? nextfloat(zero(F)) : zero(F)

  # In these loops, we unwrap the parameters and inline the calculation of the
  # model likeilhood such that AXV instructions can be used by the @turbo macro
  @turbo for inoise in 1:lnoise, ialpha in 1:lalpha, ix1 in 1:lx1

    # Extract parameters
    noise = prior.noise[inoise]
    alpha = prior.alpha[ialpha]
    x1 = prior.x[1][ix1]

    # Explicitly calculate the likelihood
    r = sqrt(abs2(x1 - design.x[1]))
    lambda = alpha * design.profile(r) + noise
    value = exp(obs * log(lambda + eps) - lambda - lfac)

    # Find the right index in buffer
    index = ix1 + (ialpha - 1) * l1 + (inoise - 1) * l2
    post.weights[index] = value * prior.weights[index]
    mass += post.weights[index]
  end

  # Calculate and return the proper posterior
  post.weights ./= mass
  return post, mass
end

# Accelerated method for calculating the posterior mass for PoissonDesigns in
# two dimension
function posteriorm!(
  post::ParamGrid{2, F},
  prior::ParamGrid{2, F},
  design::PoissonDesign{2},
  obs::Int,
)::Tuple{ParamGrid{2, F}, F} where {F}
  @assert size(post) == size(prior) """
  The shape of the posterior distribution to be modified does not match the
  shape of the prior.
  """

  # Make sure the design uses the same floating point precision as the prior
  design = NanoLoc.setprecision(design, F)

  # Logarithm of obs!, which is needed repeatedly and thus cached
  lfac = F(logfac(obs))

  # Several length parameters needed for indexing in the loop over the grid
  lnoise = length(prior.noise)
  lalpha = length(prior.alpha)
  lx1 = length(prior.x[1])
  lx2 = length(prior.x[2])

  l1 = lx1
  l2 = l1 * lx2
  l3 = l2 * lalpha

  # Variable that stores the sum of the unnormalized posterior values
  mass = zero(F)

  # In order to prevent 0 * Inf, we add a tiny epsilon to the logarithm in
  # the kernel below for obs = 0
  eps = iszero(obs) ? nextfloat(zero(F)) : zero(F)

  # In these loops, we unwrap the parameters and inline the calculation of the
  # model likeilhood such that AXV instructions can be used by the @turbo macro
  @turbo for inoise in 1:lnoise, ialpha in 1:lalpha, ix2 in 1:lx2, ix1 in 1:lx1

    # Extract parameters
    noise = prior.noise[inoise]
    alpha = prior.alpha[ialpha]
    x1 = prior.x[1][ix1]
    x2 = prior.x[2][ix2]

    # Explicitly calculate the likelihood
    r = sqrt(abs2(x1 - design.x[1]) + abs2(x2 - design.x[2]))
    lambda = alpha * design.profile(r) + noise
    value = exp(obs * log(lambda + eps) - lambda - lfac)

    # Find the right index in buffer
    index = ix1 + (ix2 - 1) * l1 + (ialpha - 1) * l2 + (inoise - 1) * l3
    post.weights[index] = value * prior.weights[index]
    mass += post.weights[index]
  end

  # Calculate and return the proper posterior
  post.weights ./= mass
  return post, mass
end

#
# MultinomialDesign
#

function posteriorm!(
  post::ParamGrid{2, F},
  prior::ParamGrid{2, F},
  design::MultinomialDesign{2, R, N, NTuple{N, PD}},
  obs::NTuple{N, Int},
)::Tuple{ParamGrid{2, F}, F} where {F, R, N, PD}
  @assert size(post) == size(prior) """
  The shape of the posterior distribution to be modified does not match the
  shape of the prior.
  """

  # Make sure the design uses the same floating point precision as the prior
  design = NanoLoc.setprecision(design, F)

  # Logarithm of the multinomial, which is needed repeatedly and thus cached
  lmult = F(logfac(design.photons) - sum(logfac, obs))

  # Several length parameters needed for indexing in the loop over the grid
  lnoise = length(prior.noise)
  lalpha = length(prior.alpha)
  lx1 = length(prior.x[1])
  lx2 = length(prior.x[2])

  dx1 = map(d -> d.x[1], design.designs)
  dx2 = map(d -> d.x[2], design.designs)
  profiles = map(d -> d.profile, design.designs)
  profile = profiles[1]
  @assert all(isequal(profile), profiles) """
  Error: Different profiles not yet supported for vectorized multinomial designs
  """

  l1 = lx1
  l2 = l1 * lx2
  l3 = l2 * lalpha

  # Variable that stores the sum of the unnormalized posterior values
  mass = zero(F)

  # In order to prevent 0 * Inf, we add a tiny epsilon to the logarithm in
  # the kernel below for obs = 0
  eps = any(iszero, obs) ? nextfloat(zero(F)) : zero(F)

  # In these loops, we unwrap the parameters and inline the calculation of the
  # model likeilhood such that AXV instructions can be used by the @turbo macro
  @turbo for inoise in 1:lnoise, ialpha in 1:lalpha, ix2 in 1:lx2, ix1 in 1:lx1

    # Extract parameters
    noise = prior.noise[inoise]
    alpha = prior.alpha[ialpha]
    x1 = prior.x[1][ix1]
    x2 = prior.x[2][ix2]

    # Calculate the likelihood
    s_lambda = zero(F)
    s = lmult

    for iobs in 1:N
      r = sqrt(abs2(x1 - dx1[iobs]) + abs2(x2 - dx2[iobs]))
      lambda = alpha * profile(r) + noise  # TODO: Currently, we only support schemes where all profiles are the same with LoopVectorization !!
      s_lambda += lambda
      s += obs[iobs] * log(lambda + eps)
    end

    value = exp(s - design.photons * log(s_lambda + eps))

    # Find the right index in buffer
    index = ix1 + (ix2 - 1) * l1 + (ialpha - 1) * l2 + (inoise - 1) * l3
    post.weights[index] = value * prior.weights[index]
    mass += post.weights[index]
  end

  # Calculate and return the proper posterior
  post.weights ./= mass
  return post, mass
end

function posteriorm!(
  post::ParamGrid{2, F},
  prior::ParamGrid{2, F},
  design::TimedMultinomialDesign{2},
  obs::Tuple{NTuple{N, Int}, Float64},
)::Tuple{ParamGrid{2, F}, F} where {F, N}

  # Bring the buffer in shape
  @assert size(post) == size(prior) """
  The shape of the posterior distribution to be modified does not match the
  shape of the prior.
  """

  # Make sure the design uses the same floating point precision as the prior
  design = NanoLoc.setprecision(design, F)

  # Destructure the timed observation
  obs, time = obs

  # Logarithm of the multinomial, which is needed repeatedly and thus cached
  lmult = F(logfac(design.base.photons) - sum(logfac, obs))

  # Several length parameters needed for indexing in the loop over the grid
  lnoise = length(prior.noise)
  lalpha = length(prior.alpha)
  lx1 = length(prior.x[1])
  lx2 = length(prior.x[2])

  dx1 = map(d -> d.x[1], design.base.designs)
  dx2 = map(d -> d.x[2], design.base.designs)
  profiles = map(d -> d.profile, design.base.designs)
  profile = profiles[1]
  @assert all(isequal(profile), profiles) """
  Error: Different profiles not yet supported for vectorized multinomial designs
  """

  l1 = lx1
  l2 = l1 * lx2
  l3 = l2 * lalpha

  # Variable that stores the sum of the unnormalized posterior values
  mass = zero(F)

  # In order to prevent 0 * Inf, we add a tiny epsilon to the logarithm in
  # the kernel below for obs = 0
  eps = any(iszero, obs) ? nextfloat(zero(F)) : zero(F)

  # In these loops, we unwrap the parameters and inline the calculation of the
  # model likeilhood such that AXV instructions can be used by the @turbo macro
  @turbo for inoise in 1:lnoise, ialpha in 1:lalpha, ix2 in 1:lx2, ix1 in 1:lx1

    # Extract parameters
    noise = prior.noise[inoise]
    alpha = prior.alpha[ialpha]
    x1 = prior.x[1][ix1]
    x2 = prior.x[2][ix2]

    # Calculate the likelihood
    s_lambda = zero(F)
    s = lmult

    for iobs in 1:N
      r = sqrt(abs2(x1 - dx1[iobs]) + abs2(x2 - dx2[iobs]))
      lambda = alpha * profile(r) + noise  # TODO: Currently, we only support schemes where all profiles are the same with LoopVectorization !
      s_lambda += lambda
      s += obs[iobs] * log(lambda + eps)
    end

    # Contribution from counts obs
    value = exp(s - design.base.photons * log(s_lambda + eps))
    # Contribution from time
    value *= s_lambda * exp(-s_lambda * time)

    # Find the right index in buffer
    index = ix1 + (ix2 - 1) * l1 + (ialpha - 1) * l2 + (inoise - 1) * l3
    post.weights[index] = value * prior.weights[index]
    mass += post.weights[index]
  end

  # Calculate and return the proper posterior
  post.weights ./= mass
  return post, mass
end

end # module LoopVectorizationExt
