
struct TracesGraph <: StackGraph
  getdata::Function
  xlabel::Observable{String}
  ylabel::Observable{String}
  xscale::Observable{Any}
  yscale::Observable{Any}
  cschemes::Observable{Vector{ColorScheme}}

  photons::Observable{Int}
  traces::Observable{Bool}
  median::Observable{Bool}
  quantiles::Observable{Bool}

  selected::Observable{Vector{Int}}
  hovered::Observable{Vector{Int}}
end

function TracesGraph(
  getdata::Function;
  xlabel = "photons",
  ylabel = "",
  xscale = identity,
  yscale = identity,
  cschemes = defaultcolorschemes(),
  photons = 0,
  traces = true,
  median = true,
  quantiles = true,
  selected = Int[],
  hovered = Int[],
)
  cschemes = getcscheme.(cschemes)
  return TracesGraph(
    getdata,
    wrapobs(xlabel),
    wrapobs(ylabel),
    wrapobs(xscale),
    wrapobs(yscale),
    wrapobs(cschemes),
    wrapobs(photons),
    wrapobs(traces),
    wrapobs(median),
    wrapobs(quantiles),
    wrapobs(selected),
    wrapobs(hovered),
  )
end

function plotelement!(
  axis::Axis,
  graph::TracesGraph,
  stack::Stack;
  colorindex = 1,
  label = "",
)
  cscheme = getcscheme(graph.cschemes, colorindex)
  traces = stack.traces
  photons = traces[1].photons
  selected = graph.selected
  hovered = graph.hovered

  @assert all(t -> t.photons == photons, traces) "Inconsistent photon number in stack"

  data = mapreduce(hcat, traces) do trace
    return photonmap(graph.getdata, trace)
  end

  for index in 1:length(stack.traces)
    color = map(selected, hovered) do sel, hov
      return attribute(index, (:black, 0.5), hov => :orange, sel => :red)
    end
    linewidth = map(selected, hovered) do sel, hov
      return attribute(index, 0.05, hov => 1.0, sel => 1.0)
    end
    lines!(
      axis,
      1:photons,
      data[:, index];
      linewidth,
      color,
      visible = graph.traces,
    )
  end

  for alpha in [0.01, 0.2, 0.5]
    qmin = mapslices(data; dims = 2) do vals
      return quantile(vals, alpha / 2)
    end
    qmax = mapslices(data; dims = 2) do vals
      return quantile(vals, 1 - alpha / 2)
    end
    band!(
      axis,
      1:photons,
      qmin[:, 1],
      qmax[:, 1];
      color = @lift(($cscheme[0.50 - alpha / 2], 0.20)),
      visible = graph.quantiles,
    )
  end

  lines!(
    axis,
    1:photons,
    median(data; dims = 2)[:, 1];
    linewidth = 2,
    color = @lift($cscheme[0.25]),
    visible = graph.median,
    label = label,
  )

  vlines!(
    axis,
    graph.photons;
    linewidth = 2,
    color = :gray30,
    visible = @lift($(graph.photons) > 1),
  )

  return axis
end
