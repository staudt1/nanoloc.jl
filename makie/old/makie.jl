
using Revise
using Statistics, LinearAlgebra

using NanoLoc
using Makie
using ColorSchemes
using GZip

defaultcolorschemes() = [:bamako, :ocean, :hot, :viridis, :magma]

getcscheme(str::String) = ColorSchemes.findcolorscheme(str)[1]
getcscheme(sym::Symbol) = ColorSchemes.colorschemes[sym]
getcscheme(cs::ColorScheme) = cs

function getcscheme(cschemes, colorindex)
  lift(cschemes) do cschemes
    index = mod1(colorindex, length(cschemes))
    return cschemes[index]
  end
end

wrapobs(val::Any) = Observable(val)
wrapobs(val::Observable) = val

function attribute(traceindex::Int, default, pairs...)
  for (set, val) in pairs
    if traceindex in set
      return val
    end
  end
  return default
end

function attribute(indices::AbstractVector{Int}, args...)
  return [attribute(index, args...) for index in indices]
end

function attribute(T::Type, indices::AbstractVector{Int}, args...)
  return T[attribute(index, args...) for index in indices]
end

## Makie.Block elements
include("photonslider.jl")

## Plottable elements
abstract type StackGraph end

function Base.setproperty!(graph::StackGraph, name::Symbol, value)
  obs = getproperty(graph, name)
  return obs[] = value
end

function plotelement!(layout::GridPosition, graph::StackGraph, stacks...)
  axis = Axis(
    layout;
    ylabel = graph.ylabel,
    xlabel = graph.xlabel,
    yscale = graph.yscale,
    xscale = graph.xscale,
  )

  for (index, stack) in enumerate(stacks)
    if stack isa Stack
      plotelement!(axis, graph, stack)
    else
      plotelement!(axis, graph, stack[1]; stack[2:end]...)
    end
  end

  return axis
end

include("traces.jl")
include("scatter.jl")
include("boxplot.jl")
include("photonbar.jl")
