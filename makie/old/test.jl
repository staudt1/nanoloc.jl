
include("makie.jl")

function loadstack(name::String, simdir = "simulations/narrow")
  path = joinpath(simdir, name * ".stack.gz")
  println("Opening gzipped stack file '$path'")
  GZip.open(path) do io
    return NanoLoc.load(io, Stack)
  end
end

# st00 = loadstack("bMINSTED-rmax150-rmin20-ratio1.0-n0.0-1000-0")
# st01 = loadstack("bMINSTED-rmax150-rmin20-ratio0.8-n0.0-1000-0")
# st02 = loadstack("tbMINSTED-rmax150-rmin20-ratio1.0-n0.0-1000-0")
# st03 = loadstack("tbMINSTED-rmax150-rmin20-ratio0.8-n0.0-1000-0")
# st04 = loadstack("MINSTED-rmax100-rmin20-n0.0-1000-0")
# st05 = loadstack("MINSTED-rmax150-rmin20-n0.0-1000-0")
# st06 = loadstack("tMINSTED-rmax100-rmin20-n0.0-1000-0")
# st07 = loadstack("tMINSTED-rmax150-rmin20-n0.0-1000-0")

# st10 = loadstack("bMINSTED-rmax150-rmin20-ratio1.0-n0.1-1000-0")
# st11 = loadstack("bMINSTED-rmax150-rmin20-ratio0.8-n0.1-1000-0")
# st12 = loadstack("tbMINSTED-rmax150-rmin20-ratio1.0-n0.1-1000-0")
# st13 = loadstack("tbMINSTED-rmax150-rmin20-ratio0.8-n0.1-1000-0")
# st14 = loadstack("MINSTED-rmax100-rmin20-n0.1-1000-0")
# st15 = loadstack("MINSTED-rmax150-rmin20-n0.1-1000-0")
# st16 = loadstack("tMINSTED-rmax100-rmin20-n0.1-1000-0")
# st17 = loadstack("tMINSTED-rmax150-rmin20-n0.1-1000-0")

# st22 = loadstack("bMINSTED-rmax150-rmin20-ratio0.8-n0.2-1000-0")
# st23 = loadstack("bMINSTED-rmax150-rmin20-ratio1.0-n0.2-1000-0")
# st22 = loadstack("tbMINSTED-rmax150-rmin20-ratio0.8-n0.2-1000-0")
# st23 = loadstack("tbMINSTED-rmax150-rmin20-ratio1.0-n0.2-1000-0")
# st24 = loadstack("MINSTED-rmax100-rmin20-n0.2-1000-0")
# st25 = loadstack("MINSTED-rmax150-rmin20-n0.2-1000-0")
# st26 = loadstack("tMINSTED-rmax100-rmin20-n0.2-1000-0")
# st27 = loadstack("tMINSTED-rmax150-rmin20-n0.2-1000-0")

figure = Figure()
ps = PhotonSlider(figure[1, 1], st00)

# graph = ScatterGraph(ylabel = "posterior std / nm"; ps.photons) do trace, step
#   # x = sum(trace.bg[1:step])
#   # x = trace.param.alpha
#   x = sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
#   y = trace[step].adaptations
#   (x, y)
# end

# plotelement!(
#   figure[2, 1],
#   graph,
#   [st02, st12, st22]; 
#   labels = ["first", "second", "third"],
# )

graph = PhotonbarGraph() do trace, step
  return sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
end

plotelement!(
  figure[2, 1],
  graph,
  [
    [st02, st06, st05, st07],
    [st02, st06, st05, st07],
    [st02, st06, st05, st07],
  ];
  intervals = [20, 10, 5],
  grouplabels = ["20 nm", "10 nm", "5 nm"],
  itemlabels = [
    "rmax 100 rmin 20",
    "timed rmax 100 rmin 20",
    "rmax 150 rmin 20",
    "timed rmax 150 rmin 20",
  ],
)

# graph = BoxplotGraph(ylabel = "posterior std / nm"; ps.photons) do trace, step
#   sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
# end

# plotelement!(
#   figure[3, 1],
#   graph,
#   [
#     [st04, st06, st05, st07],
#     [st14, st16, st15, st17],
#     [st24, st26, st25, st27],
#   ],
#   grouplabels = ["noise 0", "noise 0.1", "noise 0.2"],
#   itemlabels = [
#     "rmax 100 rmin 20",
#     "timed rmax 100 rmin 20",
#     "rmax 150 rmin 20",
#     "timed rmax 150 rmin 20",
#   ],
# )

# level = [0.5, 0.8, 0.9]
# maxerr = 3

# pc00 = photoncount(st00, (0, maxerr), level) do trace, step
#   # sqrt(sum(abs2, metric.std.x))
#   sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
# end
# @show pc00

# pc02 = photoncount(st02, (0, maxerr), level) do trace, step
#   # sqrt(sum(abs2, metric.std.x))
#   sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
# end
# @show pc02

# pc10 = photoncount(st10, (0, maxerr), level) do trace, step
#   # sqrt(sum(abs2, metric.std.x))
#   sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
# end
# @show pc10

# pc12 = photoncount(st12, (0, maxerr), level) do trace, step
#   # sqrt(sum(abs2, metric.std.x))
#   sqrt(sum(abs2, trace[step].mean.x - trace.param.x))
# end
# @show pc12

# # graph = TracesGraph(ylabel = "posterior std / nm", cschemes = [:bamako, :ocean]) do metric, trace, step
# #   sqrt(sum(abs2, metric.std.x))
# #   sqrt(sum(abs2, metric.mean.x - trace.param.x))
# #   # bg = sum(trace.bg[1:step])
# #   # bg / metric.photons
# # end

# # graph = ScatterGraph(ylabel = "posterior std / nm",) do metric, trace, step
#   # x = sum(trace.bg[1:step])
#   # x = mean(NanoLoc.sbratio.(trace.designs[1:step], trace.param))
#   # x = trace.param.alpha
#   # x = sqrt(sum(abs2, trace.param.x .- (150, 150)))
#   y = sqrt(sum(abs2, metric.std.x))
#   # y = sqrt(sum(abs2, metric.mean.x .- trace.param.x))
#   (x, y)
# end

# ax = plotelement!(
#   figure[2, 1],
#   graph,
#   [[st00, st01, st02, st03], [st10, st11, st12, st13]], #, [st20, st21, st22, st23]],
#   grouplabels = ["noise 0", "noise 0.1"],
# )

# ax = plotelement!(
#   figure[1, 1],
#   graph,
#   (st0, :label => "n0.0", :colorindex => 1),
#   (st1, :label => "n0.05", :colorindex => 2),
#   (st2, :label => "n0.10", :colorindex => 3),
#   (st3, :label => "n0.15", :colorindex => 4),
#   (st4, :label => "n0.20", :colorindex => 5),
# )

# axislegend(ax, merge = true, unique = true)

# @time ax = plotelement!(figure, pg, st)
# @time ax1 = plotelement!(figure[1,1], graph, st1)
# @time ax2 = plotelement!(figure[2,1], graph, st2)
# @time ax3 = plotelement!(figure[3,1], graph, st3)
# @time ax4 = plotelement!(figure[4,1], graph, st4)
# @time ax5 = plotelement!(figure[5,1], graph, st5)
# og.ylabel = ""
# linkxaxes!(ax1, ax2, ax3, ax4, ax5)
# linkyaxes!(ax1, ax2, ax3, ax4, ax5)

figure
