
struct PhotonSlider
  photons::Observable{Int}
  limits::Tuple{Int, Int}
end

function PhotonSlider(parent_layout, limits::NTuple{2, Integer} = (0, 100))
  if limits isa Int
    limits = (0, limits)
  end

  validator = str -> begin
    n = tryparse(Int, str)
    !isnothing(n) && limits[1] <= n <= limits[2]
  end

  photons = Observable{Int}(limits[2])

  layout = parent_layout[1, 1] = GridLayout(; default_colgap = 10)
  Label(layout[1, 1], "Photons")
  box = Textbox(
    layout[1, 2];
    placeholder = " ",
    validator = validator,
    width = 50,
    textpadding = (10, 10, 5, 5),
  )

  Label(layout[1, 3], string(limits[1]))
  Label(layout[1, 5], string(limits[2]))
  slider = Slider(
    layout[1, 4];
    range = limits[1]:1:limits[2],
    startvalue = photons[],
    snap = true,
  )

  on(photons; update = true) do photons
    if slider.value != photons && limits[1] <= photons <= limits[2]
      set_close_to!(slider, photons)
    end
    photonstring = string(photons)
    if box.stored_string[] != photonstring
      box.stored_string = photonstring
      box.displayed_string = photonstring
    end
  end

  on(box.stored_string; update = true) do str
    n = parse(Int, str)
    if photons[] != n
      photons[] = n
    end
  end

  on(slider.value; update = true) do val
    if photons[] != val
      photons[] = val
    end
  end

  return PhotonSlider(photons, limits)
end

PhotonSlider(layout, photons::Int) = PhotonSlider(layout, (0, photons))
PhotonSlider(layout, trace::Trace) = PhotonSlider(layout, trace.photons)

function PhotonSlider(layout, stack::Stack)
  photons = minimum(trace -> trace.photons, stack.traces)
  return PhotonSlider(layout, photons)
end
