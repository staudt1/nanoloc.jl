
struct BoxplotGraph <: StackGraph
  getdata::Function
  xlabel::Observable{String}
  ylabel::Observable{String}
  xscale::Observable{Any}
  yscale::Observable{Any}

  cschemes::Observable{Vector{ColorScheme}}
  photons::Observable{Int}
  ceiling::Observable{Any}
  markersize::Observable{Float64}
end

function BoxplotGraph(
  getdata::Function;
  xlabel = "",
  ylabel = "",
  xscale = identity,
  yscale = identity,
  cschemes = defaultcolorschemes(),
  photons = -1,
  ceiling = nothing,
  markersize = 8,
)
  cschemes = getcscheme.(cschemes)
  return BoxplotGraph(
    getdata,
    wrapobs(xlabel),
    wrapobs(ylabel),
    wrapobs(xscale),
    wrapobs(yscale),
    wrapobs(cschemes),
    wrapobs(photons),
    wrapobs(ceiling),
    wrapobs(markersize),
  )
end

"""
    ceildata!(data, ceiling)

Scale down values in the vector `data` that are greater than `ceiling` and
return the scaling factor.
"""
function ceildata!(data, ceiling)
  outliers = data .>= ceiling
  if count(outliers) > 0
    diff = data[outliers] .- ceiling
    scale = max(1, 10 * maximum(diff) / ceiling)
    data[outliers] .= diff ./ scale .+ ceiling
  else
    scale = 1.0
  end
  return scale
end

function plotelement!(
  axis::Axis,
  graph::BoxplotGraph,
  stacks::Vector{Vector{Stack{D, F}}};
  grouplabels = 1:length(stacks),
  itemlabels = string.(1:maximum(length, stacks)),
) where {D, F}
  @assert length(stacks) == length(grouplabels) "Each group must receive a label"
  axis.xticks[] = (1:length(stacks), grouplabels)

  for group in stacks, stack in group
    traces = stack.traces
    photons = traces[1].photons
    @assert all(t -> t.photons == photons, traces) "Inconsistent photon number in stack"
  end

  if graph.photons[] < 0
    graph.photons[] = stacks[1][1].traces[1].photons
  end

  xvals = mapreduce(vcat, 1:length(stacks)) do groupindex
    mapreduce(vcat, 1:length(stacks[groupindex])) do itemindex
      stack = stacks[groupindex][itemindex]
      return fill(groupindex, length(stack.traces))
    end
  end

  ceilscale = Observable{Float64}(1.0)

  data = lift(graph.photons, graph.ceiling) do photons, ceiling
    data = mapreduce(vcat, stacks) do group
      mapreduce(vcat, group) do stack
        return photonmap(graph.getdata, stack, photons)
      end
    end
    if !isnothing(ceiling)
      ceilscale[] = ceildata!(data, ceiling)
    end
    return data
  end

  dodge = mapreduce(vcat, 1:length(stacks)) do groupindex
    mapreduce(vcat, 1:length(stacks[groupindex])) do itemindex
      stack = stacks[groupindex][itemindex]
      return fill(itemindex, length(stack.traces))
    end
  end

  items = length(itemlabels)
  itemcolors = lift(graph.cschemes) do cschemes
    map(1:items) do index
      cindex = mod1(index, length(cschemes))
      return (cschemes[cindex][0.5], 0.5)
    end
  end

  color = lift(itemcolors) do colors
    mapreduce(vcat, 1:length(stacks)) do groupindex
      mapreduce(vcat, 1:length(stacks[groupindex])) do itemindex
        stack = stacks[groupindex][itemindex]
        return fill(colors[itemindex], length(stack.traces))
      end
    end
  end

  onany(data, graph.ceiling) do data, ceiling
    empty!(axis)
    boxplot = boxplot!(
      axis,
      xvals,
      data;
      dodge,
      gap = 0.1,
      width = 0.75,
      whiskerwidth = 0.5,
      linewidth = 2,
      markersize = graph.markersize,
      strokewidth = 1.5,
      dodge_gap = 0.05,
      color = color,
    )
    if !isnothing(ceiling)
      hlines!(
        axis,
        ceiling;
        color = :gray50,
        linewidth = 2.0,
        linestyle = :dash,
      )
      cs = round(ceilscale[]; digits = 1)
      text!(
        axis,
        [(0.01, 0.96)];
        text = ["/ $cs"],
        space = :relative,
        color = :gray30,
      )
    end
  end

  data[] = data[]

  # TODO: this does not update if colorschemes are changed
  axislegend(
    axis,
    [PolyElement(; color = color) for color in itemcolors[]],
    itemlabels,
  )

  return axis
end

function plotelement!(
  layout::GridPosition,
  graph::StackGraph,
  stacks::Vector{Vector{Stack{D, F}}};
  kwargs...,
) where {D, F}
  axis = Axis(
    layout;
    ylabel = graph.ylabel,
    xlabel = graph.xlabel,
    yscale = graph.yscale,
    xscale = graph.xscale,
  )
  return plotelement!(axis, graph, stacks; kwargs...)
end
