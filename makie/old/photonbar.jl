
struct PhotonbarGraph <: StackGraph
  getdata::Function
  xlabel::Observable{String}
  ylabel::Observable{String}
  xscale::Observable{Any}
  yscale::Observable{Any}

  levels::Observable{Vector{Float64}}
  cschemes::Observable{Vector{ColorScheme}}
end

function PhotonbarGraph(
  getdata::Function;
  xlabel = "",
  ylabel = "photons",
  xscale = identity,
  yscale = identity,
  levels = [0.5, 0.8, 0.9],
  cschemes = defaultcolorschemes(),
)
  cschemes = getcscheme.(cschemes)
  return PhotonbarGraph(
    getdata,
    wrapobs(xlabel),
    wrapobs(ylabel),
    wrapobs(xscale),
    wrapobs(yscale),
    wrapobs(levels),
    wrapobs(cschemes),
  )
end

function plotelement!(
  axis::Axis,
  graph::PhotonbarGraph,
  stacks::Vector{Vector{Stack{D, F}}};
  intervals, # one interval per group
  grouplabels = 1:length(stacks),
  itemlabels = string.(1:maximum(length, stacks)),
) where {D, F}
  @assert length(stacks) == length(grouplabels) "Each group must receive a label"
  axis.xticks[] = (1:length(stacks), grouplabels)

  for group in stacks, stack in group
    traces = stack.traces
    photons = traces[1].photons
    @assert all(t -> t.photons == photons, traces) "Inconsistent photon number in stack"
  end

  data = lift(graph.levels) do levels
    map(sort(levels; rev = true)) do level
      map(1:length(stacks)) do groupindex
        interval = intervals[groupindex]
        interval = interval isa Real ? (0, interval) : interval
        map(stacks[groupindex]) do stack
          photons = photonsneeded(stack) do trace, step
            value = graph.getdata(trace, step)
            return interval[1] <= value <= interval[2]
          end
          photons[isnothing.(photons)] .= typemax(Int)
          return quantile(photons, level)
        end
      end
    end
  end

  items = length(itemlabels)
  itemcolors = lift(graph.cschemes) do cschemes
    map(1:items) do index
      cindex = mod1(index, length(cschemes))
      return cschemes[cindex][0.6]
    end
  end

  gap = 0.3

  on(data; update = true) do data
    empty!(axis)
    for levelindex in 1:length(graph.levels[])
      for groupindex in 1:length(stacks)
        values = data[levelindex][groupindex]
        groupwidth = (1 - gap)
        barwidth = groupwidth / length(values)
        offset = groupwidth / 2 - barwidth / 2
        x = range(groupindex - offset, groupindex + offset, length(values))

        color = lift(itemcolors) do colors
          map(colors) do color
            alpha = range(0.25, 0.75, length(graph.levels[]))[levelindex]
            return (color, alpha)
          end
        end

        barplot!(
          axis,
          x,
          values;
          color = color,
          width = barwidth,
          strokewidth = 2.5,
          gap = 0.4,
          strokecolor = :gray20,
        )
      end
    end
  end

  axislegend(
    axis,
    [PolyElement(; color = color) for color in itemcolors[]],
    itemlabels;
    position = :lt,
  )

  return axis
end

function plotelement!(
  layout::GridPosition,
  graph::PhotonbarGraph,
  stacks::Vector{Vector{Stack{D, F}}};
  kwargs...,
) where {D, F}
  axis = Axis(
    layout;
    ylabel = graph.ylabel,
    xlabel = graph.xlabel,
    yscale = graph.yscale,
    xscale = graph.xscale,
  )
  return plotelement!(axis, graph, stacks; kwargs...)
end
