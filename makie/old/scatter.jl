
struct ScatterGraph <: StackGraph
  getdata::Function
  xlabel::Observable{String}
  ylabel::Observable{String}
  xscale::Observable{Any}
  yscale::Observable{Any}
  cschemes::Observable{Vector{ColorScheme}}
  photons::Observable{Int}

  xmedian::Observable{Bool}
  ymedian::Observable{Bool}

  markersize::Observable{Float64}
  selected::Observable{Vector{Int}}
  hovered::Observable{Vector{Int}}
end

function ScatterGraph(
  getdata;
  xlabel = "offset / nm",
  ylabel = "",
  xscale = identity,
  yscale = identity,
  photons = -1,
  cschemes = defaultcolorschemes(),
  markersize = 9,
  xmedian = false,
  ymedian = true,
  selected = Int[],
  hovered = Int[],
)
  cschemes = getcscheme.(cschemes)
  return ScatterGraph(
    getdata,
    wrapobs(xlabel),
    wrapobs(ylabel),
    wrapobs(xscale),
    wrapobs(yscale),
    wrapobs(cschemes),
    wrapobs(photons),
    wrapobs(xmedian),
    wrapobs(ymedian),
    wrapobs(markersize),
    wrapobs(selected),
    wrapobs(hovered),
  )
end

function plotelement!(
  axis::Axis,
  graph::ScatterGraph,
  stack::Stack;
  colorindex = 1,
  label = "",
)
  traces = stack.traces
  photons = traces[1].photons
  cscheme = getcscheme(graph.cschemes, colorindex)
  selected = graph.selected
  hovered = graph.hovered

  @assert all(t -> t.photons == photons, traces) "Inconsistent photon number in stack"

  if graph.photons[] < 0
    graph.photons[] = photons
  end

  data = lift(graph.photons) do photons
    return photonmap(graph.getdata, stack, photons)
  end

  colors = lift(cscheme, selected, hovered) do cscheme, sel, hov
    indices = 1:length(traces)
    default = (cscheme[0.5], 0.5)
    return attribute(Any, indices, default, hov => :orange, sel => :red)
  end

  sizes = lift(graph.markersize, selected, hovered) do size, sel, hov
    indices = 1:length(traces)
    return attribute(indices, size, sel => 1.25size, hov => 1.25size)
  end

  plot = scatter!(axis, data; markersize = sizes, color = colors)
  scatter!( # this plot is used for hacking the legend but not displayed
    axis,
    [Point2f(1, 1)];
    graph.markersize,
    color = @lift(($cscheme[0.5], 0.5)),
    label,
    visible = false,
  )

  ymedian = @lift [median(last.($data))]
  xmedian = @lift [median(first.($data))]

  lines = [
    hlines!(
      axis,
      ymedian;
      linewidth = 3.0,
      color = :white,
      visible = graph.ymedian,
    )
    hlines!(
      axis,
      ymedian;
      linewidth = 2.5,
      color = @lift($cscheme[0.5]),
      visible = graph.ymedian,
      label,
    )
    vlines!(
      axis,
      xmedian;
      linewidth = 3.0,
      color = :white,
      visible = graph.xmedian,
    )
    vlines!(
      axis,
      xmedian;
      linewidth = 2.5,
      color = @lift($cscheme[0.5]),
      visible = graph.xmedian,
      label,
    )
  ]
  foreach(l -> translate!(l, 0, 0, 1), lines)

  events = Makie.events(axis.scene)

  # Update graph.selected if we select a datapoint
  on(events.mousebutton) do event
    if is_mouseinside(axis.scene)
      if event.button == Mouse.left && event.action == Mouse.release
        plt, index = pick(axis)
        if plt === plot
          if index in graph.selected[]
            graph.selected = filter!(!isequal(index), graph.selected[])
          else
            graph.selected = push!(graph.selected[], index)
          end
        end
      end
    end
  end

  # # Update graph.hovered if mouse hovers over a datapoint
  # on(events.mouseposition) do pos
  #   if is_mouseinside(axis.scene)
  #     plt, index = pick(axis)
  #     graph.hovered = plt == plot ? [index] : []
  #   end
  # end

  on(graph.photons) do _
    return autolimits!(axis)
  end

  return axis
end

function plotelement!(
  layout::GridPosition,
  graph::ScatterGraph,
  stacks::Vector{<:Stack};
  labels = string.(1:length(stacks)),
  colorindices = 1:length(stacks),
)
  n = length(stacks)
  @assert length(labels) == n "Wrong number of labels"
  @assert length(colorindices) == n "Wrong number of colorindices"

  axis = Axis(
    layout;
    ylabel = graph.ylabel,
    xlabel = graph.xlabel,
    yscale = graph.yscale,
    xscale = graph.xscale,
  )

  for index in 1:n
    stack = stacks[index]
    label = labels[index]
    colorindex = colorindices[index]
    plotelement!(axis, graph, stack; label, colorindex)
  end

  axislegend(axis; unique = true, merge = true, position = :lt)

  return axis
end
