
"""
Boxplot of the distribution of a metric of stack traces for a given number of
photons.
"""
@kwdef struct StackMetricPanel <: Panel
  count_bg::Obs = true
  metric::Obs = a -> a.errx
  photons::Obs = nothing
  color::Obs = :darkgreen
  outliercolor::Obs = :darkred
  direction::Obs = :y
  offset::Obs = 0
  width::Obs = 1
  linewidth::Obs = 3
  scattersize::Obs = 8
  ceiling::Obs = nothing
  show_scatter::Obs = true
  show_outliers::Obs = true
  show_ceiling::Obs = true
end

function _preparedata(p::StackMetricPanel, stack)
  photons = photoncount(p, [stack])
  args = (p.metric, photons, p.count_bg, p.ceiling)
  return multilift(args...) do f, photons, count_bg, ceiling
    ceiling = isnothing(ceiling) ? Inf : ceiling
    values = photonmap(stack, photons; count_bg) do trace, step
      return f(trace[step])
    end
    outliers = [v for v in values if v > ceiling]
    values = [v for v in values if v <= ceiling]
    return (; values, outliers = length(outliers))
  end
end

function Makie.plot(layout, p::StackMetricPanel, stack::Stack)
  ax = Axis(layout[1, 1])
  return plot!(ax, p, stack)
end

function Makie.plot!(ax::Axis, p::StackMetricPanel, stack::Stack)
  data = _preparedata(p, stack)

  return rainbox!(
    ax,
    data.values,
    data.outliers;
    p.color,
    p.outliercolor,
    p.direction,
    p.offset,
    p.width,
    p.linewidth,
    p.scattersize,
    p.ceiling,
    p.show_scatter,
    p.show_outliers,
    p.show_ceiling,
  )
end

"""
Plot a boxplot that compares a metric of choice for different stacks.
"""
@kwdef struct StacksMetricPanel <: Panel
  count_bg::Obs = true
  metric::Obs = a -> a.errx # nm localization error
  photons::Obs = nothing
  outliercolor::Obs = :darkred
  direction::Obs = :y
  linewidth::Obs = 3
  scattersize::Obs = 8
  ceiling::Obs = nothing
  show_scatter::Obs = true
  show_outliers::Obs = true
  show_ceiling::Obs = true

  xlabel::Obs = ""
  ylabel::Obs = "photons"
  groupgap::Obs = 0.2
  classgap::Obs = 1.0
  grouplabels::Obs = nothing
  classlabels::Obs = nothing
  classcolors::Obs = [:darkgreen]
  legend::Obs = (;)
end

function Makie.plot(layout, p::StacksMetricPanel, entries...)
  data = _groupclassdata(p, entries)
  ax = _groupclassaxis(layout, p, data)
  return Makie.plot!(ax, p, entries...)
end

function Makie.plot!(ax::Axis, p::StacksMetricPanel, entries...)
  to_delete = [
    :xlabel,
    :ylabel,
    :groupgap,
    :classgap,
    :grouplabels,
    :classlabels,
    :classcolors,
    :legend,
  ]
  kwargs = deletefields(p, to_delete)
  data = _groupclassdata(p, entries)

  for entry in entries
    entry_kwargs = deletefields(entry, [:stack, :group, :class])
    color = lift(data.ccolors) do ccolors
      return ccolors[entry.class]
    end
    position = lift(data.positions, data.nclasses) do p, nc
      index = (entry.group - 1) * nc + entry.class
      return p[index]
    end

    panel =
      StackMetricPanel(; kwargs..., entry_kwargs..., color, offset = position)
    plot!(ax, panel, entry.stack)
  end

  return ax
end
