
"""
Panel that plots the localization error of stack traces as function of the
photon number.
"""
@kwdef struct StackAccuracyPanel <: Panel
  count_bg::Obs = true
  min_photons::Obs = 1
  max_photons::Obs = nothing
  title::Obs = ""
  xlabel::Obs = "photons"
  ylabel::Obs = "localization error / nm"
  yscale::Obs = log10
  xscale::Obs = log10
  levels::Obs = [(0.3, 0.5), (0.2, 0.8)]
  color::Obs = :darkblue
  stdcolor::Obs = :darkgreen
  linewidth::Obs = 2.0
  tracewidth::Obs = 0.5
  show_std::Obs = true
  show_traces::Obs = true
  show_asymptote::Obs = true
  show_quantiles::Obs = true
  asymptotecolor::Obs = :black
  asymptotewidth::Obs = 1.5
end

function addmissing(x, n)
  if length(x) < n
    vcat(x, fill(missing, n - length(x)))
  else
    convert(Array{Union{Missing, Float64}}, x[1:n])
  end
end

function _preparedata(p::StackAccuracyPanel, stack)
  args = (p.min_photons, p.max_photons, p.count_bg, p.levels)
  partial = multilift(args...) do min_photons, max_photons, count_bg, levels
    # get maximal photon number to plot
    if count_bg
      max = maximum(t -> t.photons, stack.traces)
    else
      max = maximum(t -> t.photons - t[end].bgphotons, stack.traces)
    end
    @assert max >= 1 """
    Some traces have no $(count_bg ? "photons" : "signal photons")
    """
    max_photons = isnothing(max_photons) ? max : clamp(max_photons, 1, max)

    # get localization error data in matrix format
    errdata = mapreduce(vcat, stack.traces) do trace
      data = photonmap(trace; count_bg) do trace, step
        return trace[step].errx
      end
      data = data[(min_photons + 1):end]
      return addmissing(data, max_photons - min_photons + 1) |> transpose
    end

    # get standard deviation data in matrix format
    stddata = mapreduce(vcat, stack.traces) do trace
      data = photonmap(trace; count_bg) do trace, step
        return trace[step].stdx
      end
      data = data[(min_photons + 1):end]
      return addmissing(data, max_photons - min_photons + 1) |> transpose
    end

    median = Statistics.median.(skipmissing.(eachcol(errdata)))
    median_std = Statistics.median.(skipmissing.(eachcol(stddata)))

    constant = median[end] * sqrt(max_photons)
    asymptote = n -> constant / sqrt(n)

    return (
      photons = min_photons:max_photons,
      errdata,
      stddata,
      median,
      median_std,
      asymptote,
    )
  end

  lines = map(1:length(stack.traces)) do index
    multilift(partial.errdata, partial.photons) do data, photons
      return (photons, data[index, :])
    end
  end

  # This "hardcodes" the number of quantiles - cannot be updated dynamically
  quantiles = map(1:length(p.levels[])) do index
    multilift(p.levels, partial.errdata, partial.photons) do levels, data, photons
      alpha, level = levels[index]

      lower = mapslices(data; dims = 1) do vals
        return quantile(skipmissing(vals), (1 - level) / 2)
      end
      upper = mapslices(data; dims = 1) do vals
        return quantile(skipmissing(vals), (1 - level) / 2 + level)
      end

      return (
        lower = reshape(lower, :),
        upper = reshape(upper, :),
        photons,
        alpha,
      )
    end
  end

  return (; lines, quantiles, partial...)
end

function Makie.plot!(ax, p::StackAccuracyPanel, stack)
  data = _preparedata(p, stack)

  # individual trace lines
  for trace_line in data.lines
    lines!(
      trace_line[1],
      trace_line[2];
      linewidth = p.tracewidth,
      alpha = 0.25,
      color = :black,
      visible = p.show_traces,
    )
  end

  # quantile bands
  linewidth = lift(x -> 0.5x, p.linewidth)
  for quantile in data.quantiles
    band!(
      quantile.photons,
      quantile.lower,
      quantile.upper;
      color = p.color,
      alpha = quantile.alpha,
      visible = p.show_quantiles,
    )
    lines!(
      quantile.photons,
      quantile.lower;
      color = p.color,
      alpha = @lift(2 * $(quantile.alpha)),
      linewidth = linewidth,
      visible = p.show_quantiles,
    )
    lines!(
      quantile.photons,
      quantile.upper;
      color = p.color,
      alpha = @lift(2 * $(quantile.alpha)),
      linewidth = linewidth,
      visible = p.show_quantiles,
    )
  end

  # # median line of errors
  lines!(data.photons, data.median; color = p.color, linewidth = p.linewidth)

  # # median line of std
  lines!(
    data.photons,
    data.median_std;
    color = p.stdcolor,
    # linestyle = :dot,
    linewidth = p.linewidth,
    visible = p.show_std,
  )

  # # asymptote
  lines!(
    data.photons,
    data.asymptote;
    color = p.asymptotecolor,
    linestyle = :dash,
    linewidth = p.asymptotewidth,
    visible = p.show_asymptote,
  )

  return ax
end

function Makie.plot(layout, p::StackAccuracyPanel, stack)
  ax = Axis(
    layout[1, 1];
    title = p.title,
    titlesize = 14,
    ylabel = p.ylabel,
    xlabel = p.xlabel,
    yscale = p.yscale,
    xscale = p.xscale,
    limits = (nothing, nothing),
  )

  return plot!(ax, p, stack)
end

"""
Panel that plots the localization error of several stacks as function of the
photon number.

Essentially the same as [`StackAccuracyPanel`](@ref), but with different defaults and accepts multiple stacks in `Makie.plot`.
"""
@kwdef struct StacksAccuracyPanel <: Panel
  count_bg::Obs = true
  min_photons::Obs = 1
  max_photons::Obs = nothing
  title::Obs = ""
  xlabel::Obs = "photons"
  ylabel::Obs = "localization error / nm"
  yscale::Obs = log10
  xscale::Obs = log10
  levels::Obs = [(0.3, 0.5), (0.2, 0.8)]
  color::Obs = :darkblue
  stdcolor::Obs = :darkgreen
  linewidth::Obs = 2.0
  tracewidth::Obs = 0.5
  show_std::Obs = false
  show_traces::Obs = false
  show_asymptote::Obs = true
  show_quantiles::Obs = false
  asymptotecolor::Obs = :black
  asymptotewidth::Obs = 1.5
end

function Makie.plot(layout, p::StacksAccuracyPanel, entries...)
  ax = Axis(
    layout[1, 1];
    title = p.title,
    titlesize = 14,
    ylabel = p.ylabel,
    xlabel = p.xlabel,
    yscale = p.yscale,
    xscale = p.xscale,
    limits = (nothing, nothing),
  )

  return plot!(ax, p, entries...)
end

function Makie.plot!(ax, p::StacksAccuracyPanel, entries...)
  kwargs = deletefields(p, [])
  for entry in entries
    additional_kwargs = deletefields(entry, [:stack])
    panel = StackAccuracyPanel(; kwargs..., additional_kwargs...)
    plot!(ax, panel, entry.stack)
  end
  return ax
end
