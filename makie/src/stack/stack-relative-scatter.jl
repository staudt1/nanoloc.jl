
function _handleoutliers(deviations, ceiling, extension)
  @assert !isempty(deviations) """
  Unexpected empty deviation vector.
  """
  outliers = filter(x -> sqrt(sum(abs2, x)) > ceiling, deviations)
  deviations = filter(x -> sqrt(sum(abs2, x)) <= ceiling, deviations)
  max_deviation = maximum(x -> sqrt(sum(abs2, x)), deviations)
  max_outlier = maximum(x -> sqrt(sum(abs2, x)), outliers; init = max_deviation)

  scaled_outliers = map(outliers) do x
    r = sqrt(sum(abs2, x))
    extrusion = (r - ceiling) / (max_outlier - ceiling)
    factor = ceiling * (1 + extension * extrusion) / r
    return factor .* x
  end

  if isempty(outliers)
    outliers = scaled_outliers = Point2f[]
    radius = max_deviation
  else
    radius = maximum(x -> sqrt(sum(abs2, x)), scaled_outliers)
  end

  return (; deviations, outliers, scaled_outliers, radius)
end

"""
Plot all estimates in a stack as differences from the respective true parameter.
"""
@kwdef struct StackRelativeScatterPanel <: Panel
  photons::Obs = nothing
  count_bg::Obs = false
  title::Obs = ""
  color::Obs = :darkgreen
  outliercolor::Obs = :brown
  paramcolor::Obs = :gray20
  paramsize::Obs = 9
  scattersize::Obs = 2
  linewidth::Obs = 1.5
  show_covellipse::Obs = true
  show_mean::Obs = true
  show_outliers::Obs = true
  ceiling::Obs = Inf
  extension::Obs = 0.1
  normalize::Obs = false
  center::Obs = (0, 0)
end

function _preparedata(p::StackRelativeScatterPanel, stack)
  photons = photoncount(p, [stack])
  args = (photons, p.count_bg, p.ceiling, p.extension, p.normalize, p.center)

  return multilift(args...) do photons, count_bg, ceiling, ext, normalize, c
    deviations = photonmap(stack, photons; count_bg) do trace, step
      return trace[step].est.x .- trace.param.x
    end
    res = _handleoutliers(deviations, ceiling, ext)
    mean = Statistics.mean(res.deviations)
    s = normalize ? res.radius : 1
    return (
      mean = Point2f[mean ./ s .+ c],
      tomean = Point2f[c, mean ./ s .+ c],
      deviations = Point2f[x ./ s .+ c for x in res.deviations],
      outliers = Point2f[x ./ s .+ c for x in res.scaled_outliers],
      covellipse = ellipsepoints(mean ./ s .+ c, cov(res.deviations) ./ s^2),
      outcircle = circlepoints(c, ceiling ./ s; points = 128),
    )
  end
end

function Makie.plot!(ax::Axis, p::StackRelativeScatterPanel, stack::Stack)
  data = _preparedata(p, stack)

  lines!(
    ax,
    data.tomean;
    color = @lift(($(p.color), 0.5)),
    linewidth = p.linewidth,
    visible = p.show_mean,
  )

  scatter!(
    ax,
    data.mean;
    color = p.color,
    markersize = @lift(0.75 * ($(p.paramsize))),
    visible = p.show_mean,
  )

  scatter!(ax, p.center; color = p.paramcolor, markersize = p.paramsize)

  scatter!(ax, data.deviations; color = p.color, markersize = p.scattersize)

  lines!(
    ax,
    data.covellipse;
    color = p.color,
    linewidth = p.linewidth,
    visible = p.show_covellipse,
  )

  poly!(
    ax,
    data.covellipse;
    color = @lift(($(p.color), 0.05)),
    strokecolor = @lift(($(p.color), 0.5)),
    strokewidth = p.linewidth,
    visible = p.show_covellipse,
  )

  scatter!(
    ax,
    data.outliers;
    color = p.outliercolor,
    markersize = p.scattersize,
    visible = p.show_outliers,
  )

  lines!(
    ax,
    data.outcircle;
    color = @lift(($(p.outliercolor), 0.25)),
    linewidth = @lift(2 * $(p.linewidth)),
    visible = p.show_outliers,
  )

  on(data.outcircle) do _
    return reset_limits!(ax)
  end

  return ax
end

function Makie.plot(layout, p::StackRelativeScatterPanel, stack)
  ax = Axis(layout[1, 1]; title = p.title, aspect = DataAspect())
  hidedecorations!(ax; ticks = false, ticklabels = false)
  return plot!(ax, p, stack)
end

"""
Plot several RelativeScatterPanels in a grid.
"""
@kwdef struct StacksRelativeScatterPanel <: Panel
  xticklabels::Obs = nothing
  yticklabels::Obs = nothing
  xlabel::Obs = ""
  ylabel::Obs = ""

  photons::Obs = nothing
  count_bg::Obs = false
  title::Obs = ""
  color::Obs = :darkgreen
  outliercolor::Obs = :brown
  paramcolor::Obs = :gray20
  paramsize::Obs = 9
  scattersize::Obs = 2
  linewidth::Obs = 1.5
  show_covellipse::Obs = true
  show_mean::Obs = true
  show_outliers::Obs = true
  ceiling::Obs = Inf
  extension::Obs = 0.1
end

function Makie.plot(layout, p::StacksRelativeScatterPanel, entries...)
  nx, ny = maximum(entry -> entry.position, entries)

  xticklabels = lift(p.xticklabels) do labels
    if isnothing(labels)
      string.(1:nx)
    else
      @assert length(labels) == nx """
      Expected $nx xticklabels for StacksRelativeScatterPanel  
      """
      string.(labels)
    end
  end

  yticklabels = lift(p.xticklabels) do labels
    if isnothing(labels)
      string.(1:nx)
    else
      @assert length(labels) == nx """
      Expected $nx xticklabels for StacksRelativeScatterPanel  
      """
      string.(labels)
    end
  end

  ax = Axis(
    layout[1, 1];
    title = p.title,
    aspect = DataAspect(),
    limits = ((1, 2nx + 1), (1, 2ny + 1)),
    xticks = @lift((2:2:(2nx), $xticklabels)),
    yticks = @lift((2:2:(2ny), $yticklabels)),
    xlabel = p.xlabel,
    ylabel = p.ylabel,
  )

  # hidedecorations!(ax, ticks = false, ticklabels = false, labels = false)
  return plot!(ax, p, entries...)
end

function Makie.plot!(ax, p::StacksRelativeScatterPanel, entries...)
  fields = [:xticklabels, :yticklabels, :xlabel, :ylabel]
  options = deletefields(p, fields)

  for entry in entries
    entry_options = (;
      options...,
      deletefields(entry, [:stack, :position])...,
      normalize = true,
      center = entry.position .* 2,
    )
    panel = StackRelativeScatterPanel(; entry_options...)
    plot!(ax, panel, entry.stack)
  end

  return ax
end
