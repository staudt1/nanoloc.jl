
"""
Panel that implements a scatter plot of all position estimates in a stack.
The scattering is grouped according to the true parameter position of the
localization trace.
"""
@kwdef struct StackScatterPanel <: Panel
  photons::Obs = nothing
  count_bg::Obs = true
  title::Obs = ""
  colors::Obs = [:brown, :darkgreen]
  paramcolor::Obs = :gray20
  paramsize::Obs = 9
  scattersize::Obs = 1
  linewidth::Obs = 2
  limits::Obs = (nothing, nothing)
  show_paramgrid::Obs = true
  show_covellipse::Obs = true
  show_mean::Obs = true
end

function _preparedata(p::StackScatterPanel, stack)
  params = unique(map(t -> t.param, stack.traces))
  photons = photoncount(p, [stack])
  return map(params) do param
    multilift(photons, p.count_bg) do photons, count_bg
      s = filter(t -> t.param == param, stack)
      estimates = photonmap(s, photons; count_bg) do trace, step
        return trace[step].est.x
      end
      mean = Statistics.mean(estimates)
      return (
        pos = [param.x],
        mean = [mean],
        tomean = [param.x, mean],
        covellipse = ellipsepoints(mean, cov(estimates)),
        estimates = estimates,
      )
    end
  end
end

function Makie.plot(layout, p::StackScatterPanel, stack::Stack)
  ax = Axis(
    layout[1, 1];
    title = p.title,
    aspect = DataAspect(),
    limits = p.limits,
  )
  hidedecorations!(ax; ticks = false, ticklabels = false)
  return plot!(ax, p, stack)
end

function Makie.plot!(ax::Axis, p::StackScatterPanel, stack::Stack)
  data = _preparedata(p, stack)

  xs = map(t -> t.param.x[1], stack.traces) |> unique
  ys = map(t -> t.param.x[2], stack.traces) |> unique
  vlines!(ax, xs; color = :black, alpha = 0.15, visible = p.show_paramgrid)
  hlines!(ax, ys; color = :black, alpha = 0.15, visible = p.show_paramgrid)

  color_index = 1
  for d in data
    color = lift(p.colors) do colors
      index = mod1(color_index, length(colors))
      return colors[index]
    end

    lines!(
      ax,
      d.tomean;
      color = @lift(($color, 0.5)),
      linewidth = p.linewidth,
      visible = p.show_mean,
    )

    scatter!(
      ax,
      d.mean;
      color = color,
      markersize = @lift(0.75 * $(p.paramsize)),
      visible = p.show_mean,
    )

    scatter!(ax, d.pos; color = p.paramcolor, markersize = p.paramsize)

    poly!(
      ax,
      d.covellipse;
      color = @lift(($color, 0.05)),
      strokecolor = @lift(($color, 0.5)),
      strokewidth = p.linewidth,
      visible = p.show_covellipse,
    )

    scatter!(ax, d.estimates; markersize = p.scattersize, color = color)
  end

  on(p.photons) do _
    return reset_limits!(ax)
  end

  return ax
end
