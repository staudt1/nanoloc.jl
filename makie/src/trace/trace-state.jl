"""
Structure to hold state data objects (see [`statedata`](@ref)) of a trace.
These structures are meant to hold the information necessary to visualize the
inference states.

It is indexed by the step of the trace (i.e., starting at 0 with the initial
state).
"""
struct TraceData
  entries::Vector
end

function TraceData(trace::Trace{D}) where {D}
  entries = map(t -> statedata(t.method, t.state), trace)
  return TraceData(entries)
end

Base.getindex(m::TraceData, step) = m.entries[step + 1]

"""
Panel that visualizes the spatial distribution of the inference state
during the localization process of a trace.

Assumes that the inference state can be marginalized via `marginalize`.
"""
@kwdef struct TraceStatePositionPanel <: Panel
  data = nothing
  logscale::Obs = false
  colormap::Obs = ColorSchemes.algae
  paramcolor::Obs = :brown
  step::Obs = nothing
  limits::Obs = (nothing, nothing)
  show_obs::Obs = true
  show_param::Obs = true
  show_design::Obs = true
  show_support::Obs = true
  show_upgrade::Obs = true
end

decompose(d::Design) = [d]
decompose(d::TimedDesign) = decompose(d.base)
decompose(d::CompoundDesign) = mapreduce(decompose, vcat, d.designs)
decompose(d::MultinomialDesign) = mapreduce(decompose, vcat, d.designs)

function _obsattribs(design::Design, obs)
  designs = decompose(design)
  points = Point2f[p for p in NanoLoc.designpoints(design)]
  obs = collect(Iterators.flatten(obs))
  @assert length(points) == length(obs) == length(designs) """
  Observation inconsistent with design geometry.
  """
  return Dict{Symbol, Any}(
    :text_positions => points,
    :text => [pp.(d, o) for (o, d) in zip(obs, designs)],
  )
end

function _obsattribs(design::CircleDesign, obs::Float64)
  point = design.x .+ design.radius .* sincos(obs)
  return Dict{Symbol, Any}(
    :marker_positions => Point2f[point],
    :marker => :star5,
  )
end

function _obsattribs(design::TimedCircleDesign, obs::Tuple{Float64, Float64})
  point = design.base.x .+ design.base.radius .* sincos(obs[1])
  return Dict{Symbol, Any}(
    :marker_positions => Point2f[point],
    :marker => :star5,
    :text_positions => Point2f[point],
    :text => [@sprintf("%.2f", obs[2])],
  )
end

_obsattribs(design::TimedDesign, ::Nothing) = Dict{Symbol, Any}()

function _obsattribs(design::TimedDesign, obs::Tuple)
  return _obsattribs(design.base, obs[1])
end

function _plotobs!(axis, p::TraceStatePositionPanel, trace)
  attribs = lift(p.step, p.show_obs) do step, show
    show = show && !isnothing(step) && (0 <= step < trace.steps)
    if show
      m = trace.ms[step + 1]
      _obsattribs(m.design, m.obs)
    else
      Dict{Symbol, Any}()
    end
  end

  scatter!(
    axis,
    liftget(attribs, :marker_positions, Point2f[]);
    marker = liftget(attribs, :marker, :circle),
    color = :white,
    strokecolor = :black,
    strokewidth = 1,
  )
  text!(
    axis,
    liftget(attribs, :text_positions, Point2f[]);
    text = liftget(attribs, :text, []),
    align = liftget(attribs, :text_align, (:right, :top)),
    color = :white,
    strokecolor = :black,
    strokewidth = 1.0,
  )

  return nothing
end

function _plotdesign!(ax, p::TraceStatePositionPanel, trace)
  points = map(p.step, p.show_design) do step, show
    show = show && !isnothing(step) && (0 <= step < trace.steps)
    if show
      subdesigns = decompose(trace.ms[step + 1].design)
      Point2f[NanoLoc.designcenter(d) for d in subdesigns]
    else
      Point2f[]
    end
  end

  circles = map(p.step, p.show_design) do step, show
    show = show && !isnothing(step) && (0 <= step < trace.steps)
    if show
      design = trace.ms[step + 1].design
      c = NanoLoc.designcenter(design)
      r = NanoLoc.designextent(design) / 2
      circlepoints(c, r)
    else
      Point2f[]
    end
  end

  lines!(ax, circles; color = :black, linewidth = 2.0)
  lines!(ax, circles; color = :white, linewidth = 1.0)

  scatter!(ax, points; color = :white, strokecolor = :black, strokewidth = 1.5)

  return nothing
end

function _plotparam!(axes, p::TraceStatePositionPanel, trace)
  color = lift(p.paramcolor, p.show_param) do color, show
    return show ? color : (:transparent)
  end
  strokecolor = lift(show -> show ? (:white) : (:transparent), p.show_param)
  scatter!(axes.x, [trace.param.x]; color, strokecolor, strokewidth = 1)
  vlines!(axes.x1, [trace.param.x[1]]; color, linewidth = 2.0)
  return hlines!(axes.x2, [trace.param.x[2]]; color, linewidth = 2.0)
end

function _supportlines(d::MarginalData)
  x1 = extrema(d.x[1])
  x2 = extrema(d.x[2])
  return Point2f.(
    [x1[1], x1[1], x1[2], x1[2], x1[1]],
    [x2[1], x2[2], x2[2], x2[1], x2[1]],
  )
end

function _plotsupport!(ax, p::TraceStatePositionPanel, trace, data)
  lines = map(p.step, p.show_support) do step, show
    step = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    if show
      _supportlines(data[step])
    else
      Point2f[]
    end
  end
  lines!(ax, lines; color = :black, linewidth = 0.5)
  return nothing
end

function _plotupgrade!(ax, p::TraceStatePositionPanel, trace, data)
  lines = map(p.step, p.show_upgrade) do step, show
    step = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    if show && step < trace.steps && trace.updates[step + 1].upgrade
      _supportlines(data[step + 1])
    else
      Point2f[]
    end
  end
  lines!(ax, lines; color = :black, linewidth = 0.5)
  return nothing
end

function _addmeasurementinfo(layout, p::TraceStatePositionPanel, trace)
  layout_obs = GridLayout(; default_rowgap = 0, tellheight = false)
  layout[1, 2] = layout_obs

  info = lift(p.step) do step
    step = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    if 0 <= step < trace.steps
      m = trace.ms[step + 1]
      (design = pp(m.design), obs = pp(m.design, m.obs))
    else
      (design = "", obs = "")
    end
  end

  Label(
    layout_obs[1, 1],
    @lift($info.design);
    halign = :center,
    valign = :center,
    color = :gray50,
    fontsize = 8,
    tellheight = false,
  )

  Label(
    layout_obs[2, 1],
    @lift($info.obs);
    tellwidth = false,
    halign = :center,
    valign = :top,
    color = :green,
    fontsize = 10,
    tellheight = false,
  )

  return nothing
end

function Makie.plot(layout, p::TraceStatePositionPanel, trace::SimulatedTrace)
  data = isnothing(p.data) ? TraceData(trace) : p.data
  state_data = lift(p.step) do step
    s = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    return data[s]
  end

  position_panel =
    StatePositionPanel(; data = state_data, p.logscale, p.colormap, p.limits)

  axes = plot(layout, position_panel)

  _plotdesign!(axes.x, p, trace)
  _plotobs!(axes.x, p, trace)
  _plotparam!(axes, p, trace)
  _plotsupport!(axes.x, p, trace, data)
  _plotupgrade!(axes.x, p, trace, data)
  _addmeasurementinfo(layout, p, trace)

  return axes
end

"""
Plot the brightness distribution of the states of a trace.
"""
@kwdef struct TraceStateAlphaPanel <: Panel
  data = nothing
  step::Obs = nothing
  color::Obs = :darkgreen
  linewidth::Obs = 2.5
  paramcolor::Obs = :brown
  show_param::Obs = true
end

function Makie.plot(layout, p::TraceStateAlphaPanel, trace::SimulatedTrace)
  data = isnothing(p.data) ? TraceData(trace) : p.data
  state_data = lift(p.step) do step
    s = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    return data[s]
  end

  state_panel = StateAlphaPanel(; data = state_data, p.color, p.linewidth)

  ax = plot(layout, state_panel)

  vlines!(
    ax,
    trace.param.alpha;
    color = p.paramcolor,
    linewidth = 2.0,
    visible = p.show_param,
  )

  return ax
end

"""
Plot the noise distribution of the states of a trace.
"""
@kwdef struct TraceStateNoisePanel <: Panel
  data = nothing
  step::Obs = nothing
  color::Obs = :darkgreen
  linewidth::Obs = 2.5
  paramcolor::Obs = :brown
  show_param::Obs = true
end

function Makie.plot(layout, p::TraceStateNoisePanel, trace::SimulatedTrace)
  data = isnothing(p.data) ? TraceData(trace) : p.data
  state_data = lift(p.step) do step
    s = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    return data[s]
  end

  state_panel = StateNoisePanel(; data = state_data, p.color, p.linewidth)

  ax = plot(layout, state_panel)

  vlines!(
    ax,
    trace.param.noise;
    color = p.paramcolor,
    linewidth = 2.0,
    visible = p.show_param,
  )

  return ax
end
