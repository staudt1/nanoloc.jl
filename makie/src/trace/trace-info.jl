
"""
Text based panel that summarizes significant aspects of a trace.
"""
struct TraceInfoPanel <: Panel end

function Makie.plot(
  layout,
  p::TraceInfoPanel,
  trace::SimulatedTrace{D},
) where {D}
  est = trace.metrics[end].est
  std = trace.metrics[end].std
  err = trace.param .- est
  stdx = sqrt(sum(abs2, std.x))
  errx = sqrt(sum(abs2, err.x))
  ms = measurements(trace)
  sbr = mean(NanoLoc.sbratio(m.design, trace.param) for m in ms)
  healthy = trace.health.healthy

  layout_arguments =
    (tellwidth = true, default_rowgap = 5, default_colgap = 10, valign = :top)

  layout_summary = GridLayout(; layout_arguments...)
  layout_inference = GridLayout(; layout_arguments...)
  layout_policy = GridLayout(; layout_arguments...)
  layout_loc = GridLayout(; layout_arguments...)

  layout[1, 1] = layout_summary
  layout[1, 3] = layout_inference
  layout[1, 5] = layout_policy
  layout[1, 7] = layout_loc

  Box(layout[1, 2]; strokecolor = :gray70, width = 1)
  Box(layout[1, 4]; strokecolor = :gray70, width = 1)
  Box(layout[1, 6]; strokecolor = :gray70, width = 1)

  ## layout_summary
  steps_string = string(trace.steps)
  total = trace.photons
  bg = trace.bgphotons
  signal = total - bg
  photons_string = "$total / $signal / $bg"

  halign = :left
  Label(layout_summary[2, 1], "steps"; halign)
  Label(layout_summary[3, 1], "photons"; halign)
  Label(layout_summary[4, 1], "sbr"; halign)
  Label(layout_summary[5, 1], "err/std"; halign)
  Label(layout_summary[6, 1], "healthy"; halign)

  color = :gray30
  Label(layout_summary[2, 2], steps_string; halign, color)
  Label(layout_summary[3, 2], photons_string; halign, color)
  Label(layout_summary[4, 2], pp(sbr); halign, color)
  Label(layout_summary[5, 2], pp(errx) * " / " * pp(stdx); halign, color)

  color = healthy ? (:darkgreen) : (:darkred)
  Label(layout_summary[6, 2], string(healthy); halign, color = :darkgreen)

  Label(layout_summary[1, 1:2], "Trace"; font = :bold)

  ## layout_inference
  name = NanoLoc.methodname(trace.method)
  options = NanoLoc.methodoptions(trace.method)

  halign = :left
  Label(layout_inference[2, 1], "method"; halign)
  Label(layout_inference[2, 2], name; halign, color = :gray30)

  index = 3
  for (key, val) in options
    Label(layout_inference[index, 1], string(key); halign)
    Label(layout_inference[index, 2], string(val); halign, color = :gray30)
    index += 1
  end

  Label(layout_inference[1, 1:2], "Inference"; font = :bold)

  ## layout_policy
  name = NanoLoc.policyname(trace.policy)
  options = NanoLoc.policyoptions(trace.policy)

  halign = :left
  Label(layout_policy[2, 1], "name"; halign)
  Label(layout_policy[2, 2], name; halign, color = :gray30)

  index = 3
  for (key, val) in options
    Label(layout_policy[index, 1], string(key); halign)
    Label(layout_policy[index, 2], string(val); halign, color = :gray30)
    index += 1
  end

  Label(layout_policy[1, 1:2], "Policy"; font = :bold)

  ## layout_localization
  if trace.param isa Param
    param = trace.param
  else
    param = (x = fill("-", D), alpha = "-", noise = "-")
  end

  halign = :left
  Label(layout_loc[3, 1], "truth"; halign)
  Label(layout_loc[4, 1], "est"; halign)
  Label(layout_loc[5, 1], "std"; halign)
  Label(layout_loc[6, 1], "err"; halign)

  halign = :center
  color = :gray30

  index = 2
  for dim in 1:D
    Label(layout_loc[2, index], "x$dim"; halign, font = :italic)
    Label(layout_loc[3, index], pp(param.x[dim]); halign, color)
    Label(layout_loc[4, index], pp(est.x[dim]); halign, color)
    Label(layout_loc[5, index], pp(std.x[dim]); halign, color)
    Label(layout_loc[6, index], pp(err.x[dim]); halign, color)
    index += 1
  end

  Label(layout_loc[2, index], "alpha"; halign, font = :italic)
  Label(layout_loc[3, index], pp(param.alpha); halign, color)
  Label(layout_loc[4, index], pp(est.alpha); halign, color)
  Label(layout_loc[5, index], pp(std.alpha); halign, color)
  Label(layout_loc[6, index], pp(err.alpha); halign, color)

  Label(layout_loc[2, index + 1], "noise"; halign, font = :italic)
  Label(layout_loc[3, index + 1], pp(param.noise); halign, color)
  Label(layout_loc[4, index + 1], pp(est.noise); halign, color)
  Label(layout_loc[5, index + 1], pp(std.noise); halign, color)
  Label(layout_loc[6, index + 1], pp(err.noise); halign, color)

  Label(
    layout_loc[1, 1:(index + 1)],
    "Localization";
    font = :bold,
    halign = :center,
  )

  return nothing
end
