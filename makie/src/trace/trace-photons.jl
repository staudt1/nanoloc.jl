
"""
Panel that plots the photon number against the step count of a trace.
"""
@kwdef struct TracePhotonPanel <: Panel
  linewidth::Obs = 2.5
  markersize::Obs = 6
  totalcolor::Obs = :black
  signalcolor::Obs = :skyblue4
  bgcolor::Obs = :gray60
  activecolor::Obs = :darkorange
  sbrcolor::Obs = :darkcyan
  step::Obs = nothing
  show_sbr::Obs = true
  show_upgrade::Obs = true
end

function _preparedata(p::TracePhotonPanel, trace)
  total = [m.photons for m in trace.metrics]
  bg = [m.bgphotons for m in trace.metrics]
  signal = total .- bg
  sbr = [trace[step].sbr for step in 1:(trace.steps)]
  scaled_sbr = sbr ./ maximum(sbr) .* trace.photons / 2
  upgrades = [s for s in 1:(trace.steps) if trace.updates[s].upgrade]

  return multilift(p.step) do step
    s = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    return (;
      total,
      bg,
      signal,
      scaled_sbr,
      steps = 0:(trace.steps),
      upgrades,
      step = [s],
      ltotal = total[s + 1],
      lsignal = signal[s + 1],
      lbg = bg[s + 1],
      lvisible = !isnothing(step),
    )
  end
end

function Makie.plot(layout, p::TracePhotonPanel, trace::SimulatedTrace)
  ax = Axis(
    layout[1, 1];
    ylabel = "photons",
    xlabel = "steps",
    xtickalign = 1,
    ytickalign = 1,
    limits = Observable{Any}(((0, trace.steps), (0, nothing))),
  )

  return plot!(ax, p, trace)
end

function Makie.plot!(ax, p::TracePhotonPanel, trace::SimulatedTrace)
  data = _preparedata(p, trace)

  scatterlines!(
    ax,
    data.steps,
    data.total;
    color = p.totalcolor,
    p.linewidth,
    p.markersize,
    label = "total",
  )

  scatterlines!(
    ax,
    data.steps,
    data.signal;
    color = p.signalcolor,
    p.linewidth,
    p.markersize,
    label = "signal",
  )

  scatterlines!(
    ax,
    data.steps,
    data.bg;
    color = p.bgcolor,
    p.linewidth,
    p.markersize,
    label = "bg",
  )

  lines!(
    ax,
    1:(trace.steps),
    data.scaled_sbr;
    linewidth = 1,
    color = p.sbrcolor,
    alpha = 0.25,
    visible = p.show_sbr,
  )

  fill_between!(
    ax,
    1:(trace.steps),
    0,
    data.scaled_sbr;
    color = p.sbrcolor,
    alpha = 0.04,
    visible = p.show_sbr,
  )

  vlines!(
    ax,
    data.step;
    color = p.activecolor,
    linewidth = 2,
    visible = data.lvisible,
  )

  legend = axislegend(ax; position = :lt)
  legendvaluebox!(
    ax,
    legend,
    [data.ltotal, data.lsignal, data.lbg];
    color = p.activecolor,
    format = string,
    visible = data.lvisible,
  )

  vlines!(ax, data.upgrades; color = :gray60, visible = p.show_upgrade)

  return ax
end
