
"""
Panel that plots the localization error against the trace steps.
"""
@kwdef struct TraceAccuracyPanel <: Panel
  linewidth::Obs = 2.5
  markersize::Obs = 6
  stdcolor::Obs = :darkcyan
  errcolor::Obs = :skyblue4
  activecolor::Obs = :darkorange
  xaxis::Obs = :steps
  step::Obs = nothing
  show_upgrade::Obs = true
end

function Makie.plot(layout, p::TraceAccuracyPanel, trace::SimulatedTrace)
  ax = Axis(
    layout[1, 1];
    ylabel = "localization error",
    xlabel = lift(string, p.xaxis),
    xtickalign = 1,
    ytickalign = 1,
    limits = Observable{Any}(((0, trace.steps), (0, nothing))),
  )

  return plot!(ax, p, trace)
end

function _preparedata(p::TraceAccuracyPanel, trace)
  std = map(step -> trace[step].stdx, 0:(trace.steps))
  err = map(step -> trace[step].errx, 0:(trace.steps))
  upgrades = [s for s in 1:(trace.steps) if trace.updates[s].upgrade]

  return multilift(p.xaxis, p.step) do xaxis, step
    st = isnothing(step) ? trace.steps : clamp(step, 0, trace.steps)
    if xaxis == :photons
      xticks = 0:(trace.photons)
      xsteps = map(0:(trace.photons)) do n
        return findlast(mc -> mc.photons <= n, trace.metrics) - 1
      end
      xindex = findfirst(s -> s >= st, xsteps)
      xactive = [xticks[xindex]]
      stdvals = std[xsteps .+ 1]
      errvals = err[xsteps .+ 1]
      upvals = map(upgrades) do step
        index = findfirst(s -> s >= step, xsteps)
        return xticks[index]
      end
    else
      xticks = 0:(trace.steps)
      xactive = [st]
      stdvals = std
      errvals = err
      upvals = upgrades
    end

    return (;
      xticks,
      xactive,
      stdvals,
      errvals,
      upgrades = upvals,
      lstd = std[st + 1],
      lerr = err[st + 1],
      lvisible = !isnothing(step),
    )
  end
end

function Makie.plot!(ax, p::TraceAccuracyPanel, trace::SimulatedTrace)
  data = _preparedata(p, trace)

  scatterlines!(
    ax,
    data.xticks,
    data.stdvals;
    color = p.stdcolor,
    linewidth = p.linewidth,
    label = "estimated error",
  )

  scatterlines!(
    ax,
    data.xticks,
    data.errvals;
    color = p.errcolor,
    linewidth = p.linewidth,
    label = "actual error",
  )

  legend = axislegend(ax; merge = true)
  legendvaluebox!(
    ax,
    legend,
    [data.lstd, data.lerr];
    color = p.activecolor,
    format = NanoLoc.pp,
    visible = data.lvisible,
  )

  vlines!(
    ax,
    data.xactive;
    color = p.activecolor,
    linewidth = 2,
    visible = data.lvisible,
  )

  vlines!(ax, data.upgrades; color = :gray60)

  return ax
end

# step = lift(p.step) do step
#   step == nothing ? trace.steps : clamp(step, 0, trace.steps)
# end
# activecolor = lift(p.step, p.activecolor) do step, color
#   isnothing(step) ? (:transparent) : color
# end
# xval = lift(step, steps, xvals) do step, steps, xvals
#   if length(steps) == length(xvals) # update complete
#     index = findfirst(s -> s >= step, steps)
#     Float64[xvals[index]]
#   else
#     Float64[]
#   end
# end

# values = [
#   @lift(stdvals[$step + 1]),
#   @lift(errvals[$step + 1]),
# ]

# legendvaluebox!(
#   layout[1, 1],
#   legend,
#   values;
#   color = p.activecolor,
#   format = NanoLoc.pp,
#   visible = @lift(!isnothing($(p.step))),
# )

# upgrades = map(p.show_upgrade, steps, xvals) do show, steps, xvals
#   if show && length(steps) == length(xvals)
#     ss = [s for s in 1:trace.steps if trace.updates[s].upgrade]
#     map(ss) do step
#       index = findfirst(s -> s >= step, steps)
#       xvals[index]
#     end
#   else
#     Int[]
#   end
# end

# xvals = lift(p.xaxis) do xaxis
#   xaxis == :photons ? (0:trace.photons) : (0:trace.steps)
# end

# steps = lift(p.xaxis) do xaxis
#   if xaxis == :photons
#     map(0:trace.photons) do n
#       findlast(mc -> mc.photons <= n, trace.metrics) - 1
#     end
#   else
#     collect(0:trace.steps)
#   end
# end

# stdvals = map(step -> trace[step].stdx, 0:trace.steps)
# errvals = map(step -> trace[step].errx, 0:trace.steps)

# x, y = synced(xvals, @lift(stdvals[$steps .+ 1])) do x, y
#   length(x) == length(y)
# end

# x, y = synced(xvals, @lift(errvals[$steps .+ 1])) do x, y
#   length(x) == length(y)
# end
