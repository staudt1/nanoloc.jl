
"""
Interactive overview panel of a full trace.
"""
@kwdef struct TraceOverviewPanel <: Panel
  step::Obs = nothing
  linewidth::Obs = 2.5
  markersize::Obs = 6
  error_xaxis::Obs = :steps
  logscale::Obs = false
  colormap::Obs = ColorSchemes.algae
  paramcolor::Obs = :brown
  show_obs::Obs = true
  show_param::Obs = true
  show_design::Obs = true
  show_support::Obs = true
  show_upgrade::Obs = true
  autozoom::Obs = true
  limits::Obs = (nothing, nothing)
end

function Makie.plot(layout, p::TraceOverviewPanel, trace::SimulatedTrace)
  data = TraceData(trace)

  layout_left = layout[1, 1] = GridLayout()
  layout_right = layout[1, 2] = GridLayout()
  layout_interaction = layout[2, 1:2] = GridLayout(; tellheight = true)

  info_panel = TraceInfoPanel()
  photon_panel = TracePhotonPanel(;
    linewidth = p.linewidth,
    markersize = p.markersize,
    step = p.step,
  )
  error_panel = TraceAccuracyPanel(;
    linewidth = p.linewidth,
    markersize = p.markersize,
    xaxis = p.error_xaxis,
    step = p.step,
  )
  statex_panel = TraceStatePositionPanel(;
    data = data,
    step = p.step,
    logscale = p.logscale,
    colormap = p.colormap,
    limits = p.limits,
    show_obs = p.show_obs,
    show_param = p.show_param,
    show_design = p.show_design,
    show_support = p.show_support,
    show_upgrade = p.show_upgrade,
  )
  statea_panel = TraceStateAlphaPanel(;
    data = data,
    step = p.step,
    linewidth = p.linewidth,
    show_param = p.show_param,
  )
  staten_panel = TraceStateNoisePanel(;
    data = data,
    step = p.step,
    linewidth = p.linewidth,
    show_param = p.show_param,
    paramcolor = p.paramcolor,
  )

  # Hack to make the statex-plot of fixed aspect work well with interactive resizing
  proxy_layout = GridLayout(layout_right[1, 1:2])
  bbox = lift(proxy_layout.layoutobservables.suggestedbbox) do bb
    w = minimum(widths(bb))
    diff = widths(bb) .- w
    return Rect2f(bb.origin .+ diff ./ 2, (w, w))
  end

  layout_statex = GridLayout(; bbox = bbox, parent = layout_right.parent)

  axes = (
    info = plot(layout_left[1, 1], info_panel, trace),
    photon = plot(layout_left[2, 1], photon_panel, trace),
    error = plot(layout_left[3, 1], error_panel, trace),
    statex = plot(layout_statex[1, 1], statex_panel, trace),
    statea = plot(layout_right[2, 1], statea_panel, trace),
    staten = plot(layout_right[2, 2], staten_panel, trace),
  )

  onany(p.step, p.autozoom) do _, autozoom
    if autozoom
      p.limits[] = (nothing, nothing)
    end
  end

  on(p.autozoom) do autozoom
    if !autozoom
      x1lims = extrema(data[0].x[1])
      x2lims = extrema(data[0].x[2])
      p.limits[] = (x1lims, x2lims)
    end
  end

  rowsize!(layout_right, 2, Auto(0.25))
  rowgap!(layout_right, 1, 40)

  # axis does not matter, scene only depends on figure
  scene = Makie.get_scene(axes.photon)
  _enable_interactions(scene, p, trace)
  _document_interactions(layout_interaction)

  return axes
end

function _document_interactions(layout)
  fontsize = 12
  Label(layout[1, 1], "(shift+)space / left / right:"; fontsize = 12)
  Label(layout[1, 2], "step backward / forward"; font = :italic, fontsize)
  Label(layout[1, 3], "num+space:"; fontsize)
  Label(layout[1, 4], "jump to step num"; font = :italic, fontsize)
  Label(layout[1, 5], "page-up / down:"; fontsize)
  Label(layout[1, 6], "jump to first / last step"; font = :italic, fontsize)
  Label(layout[1, 7], "enter:"; fontsize)
  Label(layout[1, 8], "start / stop animation"; font = :italic, fontsize)
  Label(layout[1, 9], "z, l, t, d, o, x:"; fontsize)
  Label(
    layout[1, 10],
    "toggle zoom, logscale, truth, design, observation, xaxis";
    font = :italic,
    fontsize,
  )

  colgap!(layout, 1, 4)
  colgap!(layout, 3, 4)
  colgap!(layout, 5, 4)
  colgap!(layout, 7, 4)
  return colgap!(layout, 9, 4)
end

function _enable_interactions(scene, p::TraceOverviewPanel, trace)
  steps = trace.steps
  events = Makie.events(scene)
  toggle! = option -> option[] = !option[]

  step = map(p.step) do step
    return isnothing(step) ? steps : clamp(step, 0, steps)
  end

  stop_signal = Ref(false)
  task = nothing

  # control keys to increment / decrement step counter
  on(events.keyboardbutton) do event
    if ispressed(scene, Keyboard.left)
      step[] = p.step[] = max(0, step[] - 1)
    elseif ispressed(scene, Keyboard.right)
      step[] = p.step[] = min(steps, step[] + 1)
    elseif ispressed(scene, Keyboard.page_up)
      step[] = p.step[] = 0
    elseif ispressed(scene, Keyboard.page_down)
      step[] = p.step[] = steps
    elseif ispressed(scene, Keyboard.enter)
      if isnothing(task) || istaskdone(task)
        stop_signal[] = false
        start_step = step[]
        task = @async _animate_panel(p, trace; stop_signal, start_step)
      else
        stop_signal[] = true
        wait(task)
      end
    end
  end

  buffer = Ref{String}("")
  shift = Keyboard.left_shift | Keyboard.right_shift

  on(events.unicode_input) do char
    # Toggle heatmap overlay options
    if char == 'o'
      toggle!(p.show_obs)
    elseif char == 't'
      toggle!(p.show_param)
    elseif char == 'd'
      toggle!(p.show_design)
    elseif char == 'u'
      toggle!(p.show_upgrade)

      # Toggle logscale option
    elseif char == 'l'
      toggle!(p.logscale)

      # Toggle autozoom
    elseif char == 'z'
      p.autozoom[] = !p.autozoom[]

      # Toggle xaxis of error plot
    elseif char == 'x'
      p.error_xaxis[] = p.error_xaxis[] == :steps ? (:photons) : (:steps)

      # Populate the buffer with digits
    elseif char in '0':'9'
      buffer[] = buffer[] * char

      # Jump to the buffered step on space
    elseif char == ' ' && !isempty(buffer[])
      s = parse(Int, buffer[])
      s = clamp(s, 0, steps)
      step[] = p.step[] = s
      buffer[] = ""

      # Jump one step backward on space (empty buffer + shift)
    elseif char == ' ' && ispressed(scene, shift)
      p.step[] = max(0, step[] - 1)

      # Jump one step forward on space (empty buffer)
    elseif char == ' '
      step[] = p.step[] = min(steps, step[] + 1)
    end
  end
end

function _animate_panel(
  p,
  trace;
  stop_signal = Ref(false),
  pause = 0.25,
  start_step = 0,
  stop_step = trace.steps,
)
  p.step[] = start_step
  p.show_obs[] = false
  p.show_upgrade[] = false

  pause = clamp(pause, 0.1, 1.0)
  stop_step = clamp(stop_step, 0, trace.steps)
  start_step = clamp(start_step, 0, stop_step)

  while p.step[] < stop_step

    # Show the observation
    p.show_obs[] = true
    sleep(pause)
    stop_signal[] && break

    # If the next distribution was adapted, forecast the adaptation_box
    if trace.updates[p.step[] + 1].upgrade
      p.show_upgrade[] = true
      sleep(min(0.75, 2 * pause))
      stop_signal[] && break
    end

    # Update the distribution
    p.step[] += 1

    # Hide the observation and the adaptation box again
    p.show_obs[] = false
    p.show_upgrade[] = false
    sleep(pause)
    stop_signal[] && break
  end
end
