
module NanoLocMakie

using Statistics
using Printf
using Random
using LinearAlgebra

using Makie
using ColorSchemes

using Reexport

@reexport using NanoLoc
import NanoLoc: pp, GenericParamGrid, GaussSamplingState

"""
Panels implement visualizations related to NanoLoc via Makie.

Panels typically aim to illustrate aspects of individual or collections of
traces and stacks. All panels have to extend the function `Makie.plot`, which is
used to fill a Makie layout with the panel visualization. Panels that operate on
a single axis may also extend the function `Makie.plot!`.
"""
abstract type Panel end

"""
    plot(panel :: Panel, args...; kwargs...)

Create and display a figure, and fill it with the content of panel.
"""
function Makie.plot(p::Panel, args...; kwargs...)
  fig = Figure()
  ret = plot(fig[1, 1], p, args...; kwargs...)
  display(fig)
  return fig, ret
end

"""
Auxiliary alias for an untyped `Observable`.
"""
const Obs = Observable{Any}

# utility methods
include("util.jl")

# helper functions for boxplots
include("groupclass.jl")

# panel implementations for states
include("state.jl")

# plotting ParamGrids as states
include("states/paramgrid.jl")

# plotting Gausssampling states
include("states/gausssampling.jl")

# panel implementations for traces
include("trace/trace-info.jl")
include("trace/trace-photons.jl")
include("trace/trace-accuracy.jl")
include("trace/trace-state.jl")
include("trace/trace-overview.jl")

# panel implementations for stacks
include("stack/stack-scatter.jl")
include("stack/stack-relative-scatter.jl")
include("stack/stack-accuracy.jl")

# (multi) stack boxplots
include("stack/stack-photon-demand.jl")
include("stack/stack-metric.jl")
# include("stack-overview.jl")

## TODO:
## StackInfoPanel
## StackPhotonPanel
## StacksPhotonPanel
## StackBoxPanel
## StacksBoxPanel
## StackOverviewPanel

# panel implementations for policies
## MinfluxOverviewPanel
## MinstedOverviewPanel
# include("minflux-overview.jl")
# include("minsted-overview.jl")
# include("simple-bed-overview.jl")

# Default plot function for traces

"""
    Makie.plot(trace :: Trace) 
    Makie.plot(layout, trace :: Trace)

Plot an interactive visualization of the localization trace `trace` (into the
layout `layout`).
"""
function Makie.plot(parent_layout, trace::SimulatedTrace)
  panel = TraceOverviewPanel()
  axes = plot(parent_layout, panel, trace)
  return axes, panel
end

function Makie.plot(trace::SimulatedTrace; kwargs...)
  fig = Figure()
  axes, panel = Makie.plot(fig[1, 1], trace; kwargs...)
  display(fig)
  return (fig, axes, panel)
end

export Panel

export StatePositionPanel, StateAlphaPanel, StateNoisePanel

export TraceInfoPanel,
  TracePhotonsPanel, TraceAccuracyPanel, TraceStatePanel, TraceOverviewPanel

export StackScatterPanel,
  StackRelativeScatterPanel,
  StackAccuracyPanel,
  StackPhotonDemandPanel,
  StackMetricPanel

export StacksRelativeScatterPanel, StacksAccuracyPanel

end # module NanoLocMakie
