
## Implementation of state plotting for ParamGrid

"""
Structure that stores all marginalized distributions of a `GenericParamGrid`.
"""
struct MarginalData{D, F}
  x::NTuple{D, Vector{F}}
  alpha::Vector{F}
  noise::Vector{F}
  weights_x::Array{F, 2}
  weights_xi::NTuple{D, Vector{F}}
  weights_alpha::Vector{F}
  weights_noise::Vector{F}
end

function MarginalData(grid::GenericParamGrid{D, F}) where {D, F}
  weights_x = marginalize(grid, :x)
  weights_xi = map(1:D) do dim
    dims = setdiff(1:D, dim)
    weights = sum(weights_x; dims = Tuple(dims))
    return reshape(weights, :)
  end

  return MarginalData(
    grid.x,
    grid.alpha,
    grid.noise,
    weights_x,
    Tuple(weights_xi),
    marginalize(grid, :alpha),
    marginalize(grid, :noise),
  )
end

function statedata(method::InferenceMethod, state::GenericParamGrid)
  return MarginalData(state)
end

function stateaspect(data::MarginalData)
  return aspectratio(data.x[2], data.x[1])
end

function stateplotx!(ax, data::Observable{<:MarginalData}, options)
  onany(data, options.limits) do data, limits
    x1limits = isnothing(limits[1]) ? extrema(data.x[1]) : limits[1]
    x2limits = isnothing(limits[2]) ? extrema(data.x[2]) : limits[2]
    return ax.limits[] = (x1limits, x2limits)
  end

  args = multilift(data, options.logscale) do data, logscale
    scalef = logscale ? log10safe : identity
    values = scalef.(data.weights_x)
    colorrange = logscale ? extrema(values) : (0, maximum(values))
    return (; x1 = data.x[1], x2 = data.x[2], values, colorrange)
  end

  return heatmap!(
    ax,
    args.x1,
    args.x2,
    args.values;
    args.colorrange,
    options.colormap,
  )
end

function stateplotx1!(ax, data::Observable{<:MarginalData}, options)
  color = lift(cm -> cm[0.75], options.colormap)
  bgcolor = lift(cm -> (cm[0.0], 0.5), options.colormap)

  args = multilift(data, options.logscale) do data, logscale
    scalef = logscale ? log10safe : identity
    values = scalef.(data.weights_xi[1])
    min, max = extrema(values)
    limit = logscale ? (min, max + 0.2) : (zero(min), 1.1max)
    ax.limits[] = (nothing, limit)
    return (; x1 = data.x[1], values, limit)
  end

  onany(args.limit) do limit
    return ax.limits[] = (nothing, limit)
  end

  return marginaldensity!(
    ax,
    args.x1,
    args.values;
    color = bgcolor,
    linecolor = color,
    markercolor = color,
    direction = :x,
  )
end

function stateplotx2!(ax, data::Observable{<:MarginalData}, options)
  color = lift(cm -> cm[0.75], options.colormap)
  bgcolor = lift(cm -> (cm[0.0], 0.5), options.colormap)

  args = multilift(data, options.logscale) do data, logscale
    scalef = logscale ? log10safe : identity
    values = scalef.(data.weights_xi[2])
    min, max = extrema(values)
    limit = logscale ? (min, max + 0.2) : (zero(min), 1.1max)
    return (; x2 = data.x[2], values, limit)
  end

  onany(args.limit) do limit
    return ax.limits[] = (limit, nothing)
  end

  return marginaldensity!(
    ax,
    args.values,
    args.x2;
    color = bgcolor,
    linecolor = color,
    markercolor = color,
    direction = :y,
  )
end

function stateplotalpha!(ax, data::Observable{<:MarginalData}, options)
  args = multilift(data) do data
    if length(data.alpha) == 1
      alpha = [data.alpha[1], data.alpha[1]]
      weights = [0, data.weights_alpha[1]]
    else
      alpha = data.alpha
      weights = data.weights_alpha
    end
    return (; alpha, weights)
  end

  return marginaldensity!(ax, args.alpha, args.weights)
end

function stateplotnoise!(ax, data::Observable{<:MarginalData}, options)
  args = multilift(data) do data
    if length(data.noise) == 1
      noise = [data.noise[1], data.noise[1]]
      weights = [0, data.weights_noise[1]]
    else
      noise = data.noise
      weights = data.weights_noise
    end
    return (; noise, weights)
  end

  return marginaldensity!(ax, args.noise, args.weights)
end
