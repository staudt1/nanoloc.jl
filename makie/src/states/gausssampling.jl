struct GaussSampleData
  last_samples::Any
end
function statedata(method::GaussSamplingMethod, state::GaussSamplingState)
  if length(state.sample_history) > 0
    return GaussSampleData(state.sample_history[end])
  else
    return GaussSampleData([])
  end
end
function _supportlines(data::GaussSampleData)
  return Point2f[]
end
function stateplotx!(ax, data::Observable{<:GaussSampleData}, options)
  args = multilift(data) do data
    positions = []
    if length(data.last_samples) > 0
      positions = [param.x for param in (data.last_samples.params)]
      weights = copy(data.last_samples.weights)
      weights /= sum(weights)
      weights *= 2500
      weights = map(sqrt, weights)
    else
      #m = mean(data.lastDistribution)
      m = [0, 0]
      positions = [(m[1], m[2])]
      weights = [1]
    end
    return (; positions, weights)
  end

  return scatter!(ax, args.positions; markersize = args.weights)
end