
"""
    filltail(x, value, n)

If `length(x) <= n`, return a copy of `x` containing the first `n` elements.
Otherwise, fill the difference `n - length(x)` with `value`.

If `x == nothing`, create an array with `n` values from scratch.
"""
function filltail(x, val, n)
  m = length(x)
  if m >= n
    return x[1:n]
  else
    return [x; fill(val, n - m)]
  end
end

function filltail(::Nothing, val, n)
  return fill(val, n)
end

_wrapobs(x::Observable) = x
_wrapobs(x) = Observable(x)

"""
    synced(condition, observables...; notify = observables)

Creates copies of `observables` that only trigger an update if `condition` is
satisfied.

The argument `notify` can be used to specify that only a subset of the
observables are notified of updates.
"""
function synced(condition, args...; notify = args)
  obs = _wrapobs.(args)
  if length(args) < 2
    throw(ArgumentError("Expected at least two observables"))
  end
  indices = map(notify) do x
    return x isa Integer ? x : findfirst(v -> v === x, args)
  end

  filter(!isnothing, indices)
  synced_args = map(x -> Observable(x[]), args)

  onany(args...) do args...
    if condition(args...)
      # silent value update
      for index in eachindex(args)
        synced_args[index].val = args[index]
      end
      # visible update
      for index in eachindex(args)
        if index in indices
          Base.notify(synced_args[index])
        end
      end
    end
  end

  return synced_args
end

"""
    multilift(f, observables...)

Return multiple observables lifted from `observables` via the function `f`.
"""
function multilift(f, obs...)
  obs = _wrapobs.(obs)
  values = f([o[] for o in obs]...)
  if length(values) < 1
    throw(ArgumentError("Expected at least one return value"))
  end

  lifted = map(Observable, values)
  onany(obs...) do args...
    values = f(args...)
    # silent value update
    for index in eachindex(values)
      lifted[index].val = values[index]
    end
    # visible update
    return foreach(notify, lifted)
  end

  return lifted
end

"""
  liftget(obs, key, default)

Convenience function for `lift(val -> get(val, key, default), obs)`.
"""
liftget(obs, args...) = lift(val -> get(val, args...), obs)

"""
    photoncount(panel, stacks)

Obtain a suitable photon count to be used when generating data from `stacks` for
plotting `panel`.

Expects the panel to have the fields `panel.count_bg` and `panel.photons`.
"""
function photoncount(p::Panel, stacks)
  return lift(p.photons, p.count_bg) do photons, count_bg
    maxphotons = minimum(stacks) do stack
      minimum(stack.traces) do trace
        return count_bg ? trace.photons : trace.photons - trace.bgphotons
      end
    end
    @assert maxphotons >= 1 """
    Not all traces have $(count_bg ? "" : "signal") photons.
    """
    if isnothing(photons)
      maxphotons
    else
      ph = clamp(photons, 1, maxphotons)
      if ph != photons
        @warn """
        Panel photons option $photons invalid. Will use $ph instead.
        """
      end
      ph
    end
  end
end

"""
    deletefields(obj, keys)

Return a named tuple with the same keys / fields as `obj` but with `keys` removed.
"""
function deletefields(a::NamedTuple, keys)
  names = setdiff(Base.keys(a), keys)
  pairs = map(names) do name
    return name => a[name]
  end
  return (; pairs...)
end

function deletefields(a, keys)
  names = setdiff(fieldnames(typeof(a)), keys)
  pairs = map(names) do name
    return name => getfield(a, name)
  end
  return (; pairs...)
end

"""
  aspectratio(x1, x2)

Return the aspect ratio of the intervals `x1` and `x2`.
"""
function aspectratio(x1, x2)
  a1, b1 = extrema(x1)
  a2, b2 = extrema(x2)
  return (b1 - a1) / (b2 - a2)
end

"""
    log10safe(x)

Save version of `log10` with a cutoff at `1e-10`.
"""
log10safe(x) = log10(clamp(x, 1e-10, Inf))

"""
    circlepoints(center, radius; points = 64)

Return `points` points on a circle with center `center` and radius `radius`.
"""
function circlepoints(c, r; points = 64)
  return map(Base.range(0, 2pi, points)) do angle
    return Point2f(c .+ r .* sincos(angle))
  end
end

"""
    ellipsepoints(center, covariance; points = 64)

Return `points` points on an ellipse with center `center` and covariance matrix
`covariance`.
"""
function ellipsepoints(c, cov; points = 64)
  cov = Symmetric(Array(cov))
  vecs = eigvecs(cov)
  vals = eigvals(sqrt(cov))
  vecs[:, 1] .*= vals[1]
  vecs[:, 2] .*= vals[2]
  map(circlepoints((0, 0), 1; points)) do x
    y = vecs * collect(x)
    return c .+ Tuple(y)
  end
end

"""
    legendvaluebox!(position, legend, values; kwargs...)

Method that adds value annotations to legends.
"""
function legendvaluebox!(
  ax,
  legend,
  values;
  color = :gray50,
  format = identity,
  visible = true,
)
  elements = fill(LineElement(), length(values))
  legend_bbox = legend.layoutobservables.computedbbox
  # TODO: make this work for center alignment
  margin = lift(legend.halign, legend_bbox, legend.margin) do ha, bbox, margin
    width = round(Int, bbox.widths[1])
    if ha == :right
      (margin[1], width, margin[3:4]...)
    else
      (width, margin[2:4]...)
    end
  end

  color = @lift $visible ? $color : (:transparent)

  return Legend(
    ax.parent,
    elements,
    map(v -> lift(format, v), values);
    tellheight = false,
    tellwidth = false,
    margin,
    halign = legend.halign,
    valign = legend.valign,
    patchsize = (0, 20),
    patchlabelgap = 0,
    labelcolor = color,
    labelfont = :bold,
    labelhalign = :center,
    framevisible = false,
    bbox = ax.scene.viewport,
  )
end

"""
    marginaldensity!(axis, xdata, ydata; direction = :x, kwargs...)

Plot a marginal density plot with data given by `xdata`, `ydata` in direction
`direction`.
"""
function marginaldensity!(
  ax,
  data_x,
  data_y;
  color = (:darkgreen, 0.2),
  linecolor = :darkgreen,
  markercolor = :black,
  direction = :x,
  kwargs...,
)
  data_x, data_y = synced(data_x, data_y) do dx, dy
    return length(dx) == length(dy)
  end

  points = map(data_x, data_y) do x, y
    if length(x) > 0 && length(x) == length(y)
      points = Point2f.(x, y)
      if direction == :x
        push!(points, Point2f(x[end], 0), Point2f(x[1], 0))
      elseif direction == :y
        push!(points, Point2f(0, y[end]), Point2f(0, y[1]))
      else
        error("Invalid direction $direction in myband!")
      end
      resize!(points, length(points))
      points
    else
      Point2f[(0, 0)]
    end
  end

  poly!(ax, points; color = color)
  lines!(ax, data_x, data_y; linewidth = 2.5, color = linecolor)
  scatter!(ax, data_x, data_y; color = markercolor, markersize = 5)

  return nothing
end

function _rect(x0, width)
  rect = Point2f.([0, width[1], width[1], 0, 0], [0, 0, width[2], width[2], 0])
  return [Point2f(x0)] .+ rect
end

function _switchxy(xy::Point2f)
  return Point2f(xy[2], xy[1])
end

"""
    rainbox!(ax, values, outliers = 0; kwargs...)
"""
function rainbox!(
  ax,
  values,
  outliers = [];
  width = 1,
  offset = 0,
  linewidth = 3.0,
  scattersize = 4.0,
  show_outliers = true,
  show_scatter = true,
  show_ceiling = true,
  ceiling = nothing,
  outliercolor = :darkred,
  color = :darkgreen,
  direction = :y,
)
  values = _wrapobs(values)
  outliers = _wrapobs(outliers)
  color = _wrapobs(color)
  outliercolor = _wrapobs(outliercolor)

  jitter = []

  data = multilift(values, offset, width) do values, offset, width
    @assert !isempty(values) """
    Cannot plot rainbox for empty vector of values
    """
    if length(jitter) != length(values)
      jitter = rand(length(values))
    end
    median = Statistics.median(values)
    lower = Statistics.quantile(values, 0.25)
    upper = Statistics.quantile(values, 0.75)
    bottom, top = extrema(values)

    box = _rect((-width / 2, lower), (width, upper - lower))
    line_median = Point2f[(-width / 2, median), (width / 2, median)]
    line_top = Point2f[(0, upper), (0, top)]
    line_bottom = Point2f[(0, lower), (0, bottom)]
    points = Point2f.(jitter .* 0.8width .- 0.8width / 2, values)
    off = Point2f((offset, 0))

    if direction in [:x, :X]
      box = _switchxy.(box)
      line_median = _switchxy.(line_median)
      line_top = _switchxy.(line_top)
      line_bottom = _switchxy.(line_bottom)
      points = _switchxy.(points)
      off = _switchxy(off)
    end

    return (;
      box = box .+ off,
      line_median = line_median .+ off,
      line_top = line_top .+ off,
      line_bottom = line_bottom .+ off,
      points = points .+ off,
    )
  end

  poly!(
    ax,
    data.box;
    strokecolor = color,
    color = @lift(($color, 0.25)),
    strokewidth = linewidth,
  )
  lines!(ax, data.line_median; color, linewidth = @lift(2 * $linewidth))
  lines!(ax, data.line_top; color, linewidth)
  lines!(ax, data.line_bottom; color, linewidth)
  scatter!(
    ax,
    data.points;
    color = @lift(($color, 0.50)),
    markersize = scattersize,
    visible = show_scatter,
  )

  ojitter = []

  args = (values, outliers, offset, ceiling, width)
  odata = multilift(args...) do values, outliers, offset, ceiling, width
    if iszero(outliers) || isempty(outliers)
      (line_frac_1 = Point2f[], line_frac_2 = Point2f[], points = Point2f[])
    else
      if isnothing(ceiling) && outliers isa Integer
        min, max = extrema(values)
        ceiling = max + (max - min) * 0.1
      elseif isnothing(ceiling)
        ceiling = (4minimum(outliers) + maximum(values)) / 5
      end

      if outliers isa Integer
        min, max = extrema(values)
        outliers =
          [ceiling + (max - min) * (0.02 + 0.05 * rand()) for _ in 1:outliers]
      end

      if length(ojitter) != length(outliers)
        ojitter = rand(length(outliers))
      end
      frac = length(outliers) / (length(outliers) + length(values))

      line_frac_1 = Point2f[
        (-width / 2, ceiling),
        (frac * width - width / 2 - 0.01 * width, ceiling),
      ]
      line_frac_2 = Point2f[
        (width / 2, ceiling),
        (frac * width - width / 2 + 0.01 * width, ceiling),
      ]

      points = Point2f.(ojitter .* 0.9width .- 0.9width / 2, outliers)
      off = Point2f((offset, 0))

      if direction in [:x, :X]
        line_frac_1 = _switchxy.(line_frac_1)
        line_frac_2 = _switchxy.(line_frac_2)
        points = _switchxy.(points)
        off = _switchxy(off)
      end

      (;
        line_frac_1 = line_frac_1 .+ off,
        line_frac_2 = line_frac_2 .+ off,
        points = points .+ off,
      )
    end
  end

  lines!(
    ax,
    odata.line_frac_1;
    color = outliercolor,
    linewidth = @lift(1.5 * $linewidth),
    visible = show_outliers,
  )

  lines!(
    ax,
    odata.line_frac_2;
    color = color,
    linewidth = @lift(1.5 * $linewidth),
    visible = show_outliers,
  )

  scatter!(
    ax,
    odata.points;
    markersize = scattersize,
    color = @lift(($outliercolor, 0.5)),
    visible = show_ceiling,
  )

  return ax
end
