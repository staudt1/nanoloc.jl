
function _groupclasspositions(ngroups, nclasses, groupgap, classgap)
  position = 1
  positions = Float64[]
  ticks = Float64[]
  for group in 1:ngroups
    for class in 1:nclasses
      push!(positions, position)
      position += 1
      if class < nclasses
        position += classgap
      end
    end
    tick = mean(positions[(end - nclasses + 1):end])
    push!(ticks, tick)
    position += 1
    position += groupgap
  end
  return (positions, ticks)
end

function _groupclassdata(p, entries)
  groups = map(entry -> entry.group, entries)
  groups = lift(groups...) do groups...
    return groups
  end
  classes = map(entry -> entry.class, entries)
  classes = lift(classes...) do classes...
    return classes
  end
  args = (
    p.grouplabels,
    p.groupgap,
    p.classlabels,
    p.classcolors,
    p.classgap,
    groups,
    classes,
  )
  return multilift(args...) do args...
    glabels, ggap, clabels, ccolors, cgap, groups, classes = args
    ngroups = maximum(groups)
    nclasses = maximum(classes)
    glabels = filltail(glabels, "", ngroups)
    clabels = filltail(clabels, "", nclasses)
    ccolors = filltail(ccolors, :black, nclasses)
    positions, ticks = _groupclasspositions(ngroups, nclasses, ggap, cgap)
    return (;
      ngroups,
      nclasses,
      glabels,
      clabels,
      ccolors,
      positions,
      ticks = (ticks, glabels),
    )
  end
end

function _groupclasslegend(ax::Axis, p, data)
  legend_elements = []
  legend_labels = []
  for class in 1:data.nclasses[]
    color = lift(data.ccolors) do ccolors
      return ccolors[class]
    end
    label = lift(data.clabels) do clabels
      return clabels[class]
    end
    push!(legend_labels, label)
    push!(legend_elements, PolyElement(; color))
  end

  return Legend(
    ax.parent,
    legend_elements,
    legend_labels;
    tellheight = false,
    tellwidth = false,
    halign = liftget(p.legend, :halign, :right),
    valign = liftget(p.legend, :valign, :top),
    margin = (10, 10, 10, 10),
    bbox = ax.scene.viewport,
  )
end

function _groupclasslegend(layout, p, data)
  legend_elements = []
  legend_labels = []
  for class in 1:data.nclasses[]
    color = lift(data.ccolors) do ccolors
      return ccolors[class]
    end
    label = lift(data.clabels) do clabels
      return clabels[class]
    end
    push!(legend_labels, label)
    push!(legend_elements, PolyElement(; color))
  end

  return Legend(
    layout,
    legend_elements,
    legend_labels;
    halign = liftget(p.legend, :halign, :right),
    valign = liftget(p.legend, :valign, :top),
    margin = (10, 10, 10, 10),
  )
end

function _groupclassaxis(layout, p, data)
  axis_args = (xticks = data.ticks, xlabel = p.xlabel, ylabel = p.ylabel)
  legend_position = liftget(p.legend, :position, :inside)[]

  if legend_position == :none
    ax = Axis(layout[1, 1]; axis_args...)

  elseif legend_position == :inside
    ax = Axis(layout[1, 1]; axis_args...)
    _groupclasslegend(ax, p, data)

  elseif legend_position == :right
    layout = GridLayout(layout[1, 1])
    ax = Axis(layout[1, 1]; axis_args...)
    _groupclasslegend(layout[1, 2], p, data)

  elseif legend_position == :left
    layout = GridLayout(layout[1, 1])
    ax = Axis(layout[1, 2]; axis_args...)
    _groupclasslegend(layout[1, 1], p, data)

  elseif legend_position == :top
    layout = GridLayout(layout[1, 1])
    ax = Axis(layout[2, 1]; axis_args...)
    _groupclasslegend(layout[1, 1], p, data)

  elseif legend_position == :bottom
    layout = GridLayout(layout[1, 1])
    ax = Axis(layout[1, 1]; axis_args...)
    _groupclasslegend(layout[2, 1], p, data)
  end

  return ax
end
