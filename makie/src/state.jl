
"""
    statedata(method :: InferenceMethod, state :: InferenceState)

Return a data object containing all information to plot various projections
of `state` (belonging to `method`).

The purpose is to reduce memory consumption and enable pre-computations when
visualizing traces.
"""
function statedata(method::InferenceMethod, state::InferenceState)
  return error("statedata not implemented for $(typeof(state))")
end

"""
    stateaspect(data)

Return the aspect ratio of the state data `data`. Defaults to 1.
"""
function stateaspect(data)
  return 1.0
end

"""
    stateplotx!(axis, data, options)

Plot the x-projection of `data` into `axis`.

The state data `data` is assumed to be derived from a state via [`statedata`]
(@ref). The argument `options` contains additional customizations, like axis
limits.
"""
function stateplotx!(ax, data, options)
  @warn "Plotting of x-projections not implemented for $(typeof(data))"
end

function stateplotx1!(ax, data, options)
  @warn "Plotting of x1-projections not implemented for $(typeof(data))"
end

function stateplotx2!(ax, data, options)
  @warn "Plotting of x2-projections not implemented for $(typeof(data))"
end

function stateplotalpha!(ax, data, options)
  @warn "Plotting of alpha-projections not implemented for $(typeof(data))"
end

function stateplotnoise!(ax, data, options)
  @warn "Plotting of noise-projections not implemented for $(typeof(data))"
end

"""
Visualize the spatial information of an inference state.
"""
@kwdef struct StatePositionPanel <: Panel
  data::Obs = nothing
  logscale::Obs = false
  colormap::Obs = ColorSchemes.algae
  limits::Obs = (nothing, nothing)
end

function Makie.plot(layout, p::StatePositionPanel, stateargs = nothing)
  layout = GridLayout(layout, 2, 2; tellwidth = false, tellheight = false)
  data = lift(p.data) do data
    return isnothing(data) ? statedata(stateargs...) : data
  end

  ax_x = Axis(
    layout[2, 1];
    xgridvisible = true,
    ygridvisible = true,
    aspect = DataAspect(),
    backgroundcolor = @lift($(p.colormap)[0.0]),
    tellwidth = true,
    tellheight = true,
  )
  deactivate_interaction!(ax_x, :rectanglezoom)

  ax_x1 = Axis(
    layout[1, 1];
    xgridvisible = false,
    ygridvisible = false,
    yticklabelsvisible = false,
    yticksvisible = false,
    xticklabelsvisible = false,
    xticksvisible = false,
    topspinevisible = false,
    leftspinevisible = false,
    rightspinevisible = false,
    xzoomlock = true,
    yzoomlock = true,
    ypanlock = true,
    tellwidth = false,
    tellheight = false,
  )
  deactivate_interaction!(ax_x1, :rectanglezoom)
  deactivate_interaction!(ax_x1, :scrollzoom)

  ax_x2 = Axis(
    layout[2, 2];
    xgridvisible = false,
    ygridvisible = false,
    yticklabelsvisible = false,
    yticksvisible = false,
    xticklabelsvisible = false,
    xticksvisible = false,
    topspinevisible = false,
    bottomspinevisible = false,
    rightspinevisible = false,
    xzoomlock = true,
    yzoomlock = true,
    xpanlock = true,
    tellwidth = false,
    tellheight = false,
  )
  deactivate_interaction!(ax_x2, :rectanglezoom)
  deactivate_interaction!(ax_x2, :scrollzoom)

  stateplotx1!(ax_x1, data, p)
  stateplotx2!(ax_x2, data, p)
  stateplotx!(ax_x, data, p)

  linkxaxes!(ax_x, ax_x1)
  linkyaxes!(ax_x, ax_x2)

  on(data; update = true) do data
    return colsize!(layout, 1, Aspect(2, stateaspect(data)))
  end

  rowsize!(layout, 1, Fixed(50))
  colsize!(layout, 2, Fixed(50))

  return (; x = ax_x, x1 = ax_x1, x2 = ax_x2)
end

"""
Plot the brightness information of an inference state.
"""
@kwdef struct StateAlphaPanel <: Panel
  data::Obs = nothing
  color::Obs = :darkgreen
  linewidth::Obs = 2.5
end

function Makie.plot(layout, p::StateAlphaPanel, stateargs = nothing)
  data = lift(p.data) do data
    return isnothing(data) ? statedata(stateargs...) : data
  end

  ax = Axis(layout[1, 1]; xlabel = "alpha", limits = (nothing, (0, nothing)))

  stateplotalpha!(ax, data, p)

  return ax
end

"""
Plot the noise information of an inference state.
"""
@kwdef struct StateNoisePanel <: Panel
  data::Obs = nothing
  color::Obs = :darkgreen
  linewidth::Obs = 2.5
end

function Makie.plot(layout, p::StateNoisePanel, stateargs = nothing)
  data = lift(p.data) do data
    return isnothing(data) ? statedata(stateargs...) : data
  end

  ax = Axis(layout[1, 1]; xlabel = "noise", limits = (nothing, (0, nothing)))

  stateplotnoise!(ax, data, p)

  return ax
end
