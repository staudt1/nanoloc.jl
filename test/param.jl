
@testset "Param" begin
  @test Param((0,); alpha = 10, noise = 7) isa Param{1, Float64}
  @test Param((0, 1); alpha = 10, noise = 2) isa Param{2, Float64}
  @test Param((0, 1, 2); alpha = 3, noise = 2) isa Param{3, Float64}

  @test Param(Float32, (0, 1, 2); alpha = 3, noise = 2) isa Param{3, Float32}
  @test Param((0, 1, 2)) == Param(Position{3, Float64}(0, 1, 2))
  @test Param(Float32, (0, 1, 2)) == Param(Float32, Position(0, 1, 2))

  @test Param((0, 1)) == convert(Param{2, Float64}, Param(Float32, (0, 1)))
  @test Param(Float32, (0, 1)) ==
        convert(Param{2, Float32}, Param(Float64, (0, 1)))
  @test Param((0, 1)) == NanoLoc.setprecision(
    NanoLoc.setprecision(Param((0, 1)), Float32),
    Float64,
  )

  p = Param((0, 0); alpha = 7, noise = 3)
  @test setx(p, (1, 1)) == Param((1, 1); alpha = 7, noise = 3)
  @test setalpha(p, 2.1) == Param((0, 0); alpha = 2.1, noise = 3)
  @test setnoise(p, 2.1) == Param((0, 0); alpha = 7, noise = 2.1)
  @test scalenoise(p, 2) == Param((0, 0); alpha = 7, noise = 6)
  @test scalealpha(p, 2) == Param((0, 0); alpha = 14, noise = 3)
end
