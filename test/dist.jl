
@testset "ParamList" begin
  params = [Param((0, 0); alpha = 10), Param((1, 1); alpha = 20)]
  weights = Float64[0.5, 0.5]
  list = ParamList(params; weights)

  @test rand(list) in params
  @test issubset(rand(list, 5), params)

  list32 = NanoLoc.setatype(list, Array{Float32})
  list64 = NanoLoc.setatype(list32, Array{Float64})
  @test list32 isa ParamList{2, Float32}
  @test list64 isa ParamList{2, Float64}
  @test all(list.weights .== list64.weights)
  @test all(list.params .== list64.params)

  list32 = NanoLoc.setprecision(list, Float32)
  list64 = NanoLoc.setprecision(list32, Float64)
  @test list32 isa ParamList{2, Float32}
  @test list64 isa ParamList{2, Float64}
  @test all(list.weights .== list64.weights)
  @test all(list.params .== list64.params)

  @test mean(list) == Param((0.5, 0.5); alpha = 15)
  @test var(list) == Param((0.25, 0.25); alpha = 25)
  @test cov(list) == [0.25 0.25 2.5 0; 0.25 0.25 2.5 0; 2.5 2.5 25 0; 0 0 0 0]

  list = NanoLoc.setweights(list, [1, 2])
  @test isapprox(sum(NanoLoc.weights(list)), 1)

  params = repeat(params; outer = 5)
  weights = rand(10)
  ref = rand(10)
  list = ParamList(params; weights, ref)
  @test sum(NanoLoc.weights(list)) >= sum(weights)
  @test isapprox(sum(NanoLoc.weightsc(list)), 1)
  ref2 = rand(10)
  @test isapprox(sum(NanoLoc.weights(list; ref = ref2) .* ref2), 1)
  @test all(isapprox.(NanoLoc.refweights(list), ref))
end

@testset "ParamGrid" begin
  grid = ParamGrid(
    0:10,
    0:10;
    alpha = 0.5:1.5,
    noise = 0.3,
    weights = GaussProfile(; fwhm = 10),
  )

  # Floating point type conversions
  grid32 = NanoLoc.setprecision(grid, Float32)
  grid64 = NanoLoc.setprecision(grid, Float64)
  @test grid isa ParamGrid{2, Float64}
  @test grid32 isa ParamGrid{2, Float32}
  @test all(grid.weights .== grid64.weights)
  @test all(grid.params .== grid64.params)

  grid32 = NanoLoc.setatype(grid, Array{Float32})
  grid64 = NanoLoc.setatype(grid, Array{Float64})
  @test grid isa ParamGrid{2, Float64}
  @test grid32 isa ParamGrid{2, Float32}
  @test all(grid.weights .== grid64.weights)
  @test all(grid.params .== grid64.params)

  # Moments
  m = mean(grid)
  s = std(grid)
  @test isapprox(m, Param((5, 5); alpha = 1.0, noise = 0.3))
  @test isapprox(m, mean(grid32))

  @test isapprox(s.x[1], s.x[2])
  @test abs(s.noise) <= 1e-15
  @test isapprox(s, std(grid32), atol = 1e-5)

  grid2 = NanoLoc.setweights(grid, GaussProfile(; fwhm = 20))
  @test std(grid).x[1] < std(grid2).x[1]

  # Sampling
  @test rand(grid) in NanoLoc.params(grid)
  @test issubset(rand(grid, 5), NanoLoc.params(grid))

  # Changes in reference measures
  @test NanoLoc.hasref(grid) == false
  @test all(NanoLoc.weights(grid; ref = grid.weights) .== 1)

  l = length(grid.weights)
  grid2 = NanoLoc.setweights(grid, ones(l); ref = grid.weights)
  @test all(isapprox.(grid2.weights, grid.weights))

  grid3 = NanoLoc.setrefweights(grid, grid.weights)
  @test NanoLoc.hasref(grid3) == true
  @test all(grid3.weights .== 1)
end
