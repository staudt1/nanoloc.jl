
@testset "Profiles" begin
  gauss = GaussProfile()
  gauss32 = NanoLoc.setprecision(gauss, Float32)
  gaussians = [
    GaussProfile(F; width, alpha) for F in [Float32, Float64],
    width in [0.5, 5], alpha in [1, 10]
  ]

  @test gauss isa GaussProfile{Float64}
  @test gauss32 isa GaussProfile{Float32}
  for gauss in gaussians
    @test isapprox(gauss(zero(gauss.width)), gauss.alpha)
    @test isapprox(gauss(gauss.width), gauss.alpha * exp(-1))
  end
  @test isapprox(NanoLoc.fwhm(gauss), gauss.width * 2sqrt(log(2)))

  donut = DonutProfile()
  donut32 = NanoLoc.setprecision(donut, Float32)
  donuts = [
    DonutProfile(F; width, alpha) for F in [Float32, Float64],
    width in [0.5, 5], alpha in [1, 10]
  ]

  @test donut isa DonutProfile{Float64}
  @test donut32 isa DonutProfile{Float32}
  for donut in donuts
    @test isapprox(donut(zero(donut.width)), 0)
    @test isapprox(donut(donut.width), donut.alpha)
  end
  @test isapprox(NanoLoc.fwhm(donut), donut.width * 2sqrt(log(2)))

  quad = QuadraticProfile()
  quad32 = NanoLoc.setprecision(quad, Float32)
  quads = [
    QuadraticProfile(F; width, alpha) for F in [Float32, Float64],
    width in [0.5, 5], alpha in [1, 10]
  ]

  @test quad isa QuadraticProfile{Float64}
  @test quad32 isa QuadraticProfile{Float32}
  for quad in quads
    @test isapprox(quad(zero(quad.width)), 0)
    @test isapprox(quad(quad.width), quad.alpha)
  end
  @test isapprox(NanoLoc.fwhm(quad), quad.width * 2sqrt(log(2)))

  for profile in (gauss, donut, quad)
    normal = Position(rand(2)...)
    len = sum(abs2, normal)
    ortho = Position(normal[2], -normal[1])

    p = DirectedProfile(profile, normal)
    @test isapprox(p(normal), profile(len), atol = 1e-4)
    @test isapprox(p(ortho), profile(0e0), atol = 1e-4)
  end
end

for D in [PoissonDesign, BernoulliDesign]
  @testset "$(string(D))" begin
    prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.1:1)
    prior32 = NanoLoc.setprecision(prior, Float32)
    param = rand(prior)
    profile = DonutProfile()
    design = D((10, 10), profile)

    @test NanoLoc.profiles(design) == [profile]
    @test NanoLoc.designextent(design) > 0
    @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

    @test D((10, 10)) isa Design{2, Float64}
    @test D(Float32, (10, 10)) isa Design{2, Float32}
    @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
    @test likelihood(prior.params[1], design, 1) isa Float64
    @test posterior(prior, design, 1) isa ParamDist{2, Float64}
    @test posteriorm(prior, design, 1) isa Tuple{ParamDist{2, Float64}, Float64}
    @test posteriorm(prior32, design, 1) isa
          Tuple{ParamDist{2, Float32}, Float32}
    @test isapprox(sum(obs -> likelihood(param, design, obs), 0:1000), 1)

    @test rand(design, param) isa Int
    @test NanoLoc.randbg(design, param) isa Tuple{Int, Int}

    for obs in 0:1
      post = posterior(prior, design, obs)
      @test isapprox(sum(post.weights), 1)
    end

    obs = rand(design, param, 10)
    @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
    @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
  end
end

@testset "ExponentialDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.1:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  param = rand(prior)
  profile = QuadraticProfile()
  design = ExponentialDesign((10, 10), profile)
  obs = rand(design, param)

  @test NanoLoc.profiles(design) == [profile]
  @test NanoLoc.designextent(design) > 0
  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test ExponentialDesign((10, 10)) isa Design{2, Float64}
  @test ExponentialDesign(Float32, (10, 10)) isa Design{2, Float32}
  @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
  @test likelihood(prior.params[1], design, 1.0) isa Float64
  @test posterior(prior, design, 1.0) isa ParamDist{2, Float64}
  @test posteriorm(prior, design, obs) isa Tuple{ParamDist{2, Float64}, Float64}
  @test posteriorm(prior32, design, obs) isa
        Tuple{ParamDist{2, Float32}, Float32}

  @test rand(design, param) isa Float64
  @test NanoLoc.randbg(design, param) isa Tuple{Float64, Int}

  for obs in [0.1, 1.0, 10.0]
    post = posterior(prior, design, obs)
    @test isapprox(sum(post.weights), 1)
  end

  obs = rand(design, param, 10)
  @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
  @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
end

@testset "CompoundDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.1:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  design1 = PoissonDesign((0, 0))
  design2 = PoissonDesign((10, 10))
  design = CompoundDesign(design1, design2)

  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test length(NanoLoc.profiles(design)) == 2
  @test NanoLoc.designextent(design) > 0
  @test design isa Design{2, Float64}
  @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
  @test likelihood(prior.params[1], design, (0, 1)) isa Float64
  @test posterior(prior, design, (1, 5)) isa ParamDist{2, Float64}
  @test posteriorm(prior, design, (5, 7)) isa
        Tuple{ParamDist{2, Float64}, Float64}

  design32 = NanoLoc.setprecision(design, Float32)
  @test design32 isa Design{2, Float32}
  @test posteriorm(prior, design32, (5, 7)) isa
        Tuple{ParamDist{2, Float64}, Float64}

  param = rand(prior)
  @test rand(design, param) isa NTuple{2, Int}
  @test NanoLoc.randbg(design, param) isa Tuple{NTuple{2, Int}, Int}
end

@testset "MultinomialDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.5:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  design1 = PoissonDesign((0, 0))
  design2 = PoissonDesign((10, 10))
  design = MultinomialDesign(10, design1, design2)
  obs = rand(design, prior)

  @test length(NanoLoc.profiles(design)) == 2
  @test NanoLoc.designextent(design) > 0
  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test design isa Design{2, Float64}
  @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
  @test likelihood(prior.params[1], design, (0, 2)) isa Float64
  @test likelihood(prior.params[1], design, obs) isa Float64
  @test posterior(prior, design, obs) isa ParamDist{2, Float64}
  @test posteriorm(prior, design, obs) isa Tuple{ParamDist{2, Float64}, Float64}

  design32 = NanoLoc.setprecision(design, Float32)
  @test design32 isa Design{2, Float32}
  @test posteriorm(prior, design32, obs) isa
        Tuple{ParamDist{2, Float64}, Float64}

  @test sum(rand(design, prior)) == 10
  @test sum(rand(design32, prior)) == 10

  param = rand(prior)
  @test sum(rand(design, param)) == 10
  @test sum(NanoLoc.randbg(design, param)[1]) == 10

  obs = rand(design, param, 10)
  @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
  @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
end

@testset "TimedMultinomialDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.5:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  design1 = PoissonDesign((0, 0))
  design2 = PoissonDesign((10, 10))

  design = TimedMultinomialDesign(10, design1, design2)
  @test design == settiming(MultinomialDesign(10, design1, design2), true)

  obs = rand(design, prior)

  @test length(NanoLoc.profiles(design)) == 2
  @test NanoLoc.designextent(design) > 0
  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test design isa Design{2, Float64}
  @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
  @test likelihood(prior.params[1], design, ((0, 2), 0.5)) isa Float64
  @test likelihood(prior.params[1], design, obs) isa Float64
  @test posterior(prior, design, obs) isa ParamDist{2, Float64}
  @test posteriorm(prior, design, obs) isa Tuple{ParamDist{2, Float64}, Float64}

  design32 = NanoLoc.setprecision(design, Float32)
  @test design32 isa Design{2, Float32}
  @test posteriorm(prior, design32, obs) isa
        Tuple{ParamDist{2, Float64}, Float64}

  @test sum(rand(design, prior)[1]) == 10
  @test sum(rand(design32, prior)[1]) == 10

  param = rand(prior)
  @test sum(rand(design, param)[1]) == 10
  @test sum(NanoLoc.randbg(design, param)[1][1]) == 10

  obs = rand(design, param, 10)
  @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
  @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
end

@testset "CircleDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.1:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  param = rand(prior)

  design = CircleDesign((10, 10), 10)

  @test NanoLoc.profiles(design) == [design.profile]
  @test NanoLoc.designextent(design) > 0
  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test CircleDesign((10, 10), 10) isa Design{2, Float64}
  @test CircleDesign(Float32, (10, 10), 10) isa Design{2, Float32}

  for profile in [GaussProfile(), QuadraticProfile(), DonutProfile()]
    design = CircleDesign((10, 10), 10 * rand(), profile)
    @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
    @test likelihood(prior.params[1], design, 1.0) isa Float64
    @test posterior(prior, design, 1.0) isa ParamDist{2, Float64}
    @test posteriorm(prior, design, 1.0) isa
          Tuple{ParamDist{2, Float64}, Float64}
    @test posteriorm(prior32, design, 1.0) isa
          Tuple{ParamDist{2, Float32}, Float32}
    h = 2pi / 10000
    # Poor person's numerical integration...
    @test isapprox(
      sum(obs -> likelihood(param, design, obs) * h, 0:h:(2pi)),
      1,
      atol = 1e-3,
    )

    for obs in (2pi .* rand(100))
      post = posterior(prior, design, obs)
      @test isapprox(sum(post.weights), 1)
    end
  end

  obs = rand(design, param, 10)
  @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
  @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
end

@testset "TimedCircleDesign" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.0:0.1:1)
  prior32 = NanoLoc.setprecision(prior, Float32)
  param = rand(prior)

  design = TimedCircleDesign((10, 10), 10)

  @test design == settiming(CircleDesign((10, 10), 10), true)

  @test NanoLoc.profiles(design) == [design.base.profile]
  @test NanoLoc.designextent(design) > 0
  @test all(designcenter(setcenter(design, (20, 15))) .== (20, 15))

  @test settiming(CircleDesign((10, 10), 10), true) isa Design{2, Float64}
  @test settiming(CircleDesign(Float32, (10, 10), 10), true) isa Design{2, Float32}

  for profile in [GaussProfile(), QuadraticProfile(), DonutProfile()]
    design = settiming(CircleDesign((10, 10), 10 * rand(), profile), true)
    obs = (1.0, 1.0)
    @test NanoLoc.setprecision(design, Float32) isa Design{2, Float32}
    @test likelihood(prior.params[1], design, obs) isa Float64
    @test posterior(prior, design, obs) isa ParamDist{2, Float64}
    @test posteriorm(prior, design, obs) isa
          Tuple{ParamDist{2, Float64}, Float64}
    @test posteriorm(prior32, design, obs) isa
          Tuple{ParamDist{2, Float32}, Float32}
    h = 2pi / 10000
    # Poor person's numerical integration...
    # @test isapprox(sum(obs -> likelihood(param, design, obs)*h, 0:h:2pi), 1, atol = 1e-3)

    for obs in zip(2pi .* rand(1), 5rand(1))
      post = posterior(prior, design, obs)
      @test isapprox(sum(post.weights), 1)
    end
  end

  obs = rand(design, param, 10)
  @test isapprox(utility(prior, design, obs), utility(prior32, design, obs))
  @test isapprox(utilitys(prior, design, obs), utilitys(prior32, design, obs))
end

@testset "Measurement" begin
  prior = ParamGrid(0:10, 0:10; alpha = 0.5:0.1:1.5)
  design = MinfluxDesign((5, 5), 5)
  observations = rand(design, Param((2, 2)), 10)
  param = prior.params[5]

  m = Measurement(design, observations[1])
  @test likelihood(param, m.design, m.obs) == likelihood(param, m)

  post = nothing
  for obs in observations
    post = posterior(prior, design, obs)
  end
  ms = Measurement.(design, observations)
  post2 = posterior(prior, ms)

  @test all(isapprox.(weights(post), weights(post2)))
end
