
using Test

using NanoLoc
using Statistics

import NanoLoc: designcenter, setcenter

@testset "Param" include("param.jl")
@testset "Dist" include("dist.jl")
@testset "Design" include("design.jl")
@testset "Trace" include("trace.jl")
