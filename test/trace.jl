
@testset "Trace" begin
  policy = Minsted()
  method = BayesGrid(1:100, 1:100)
  stop = t -> t.photons >= 50
  trace = localize(policy, method, Param((50, 50)); stop)
  trace2 = relocalize(trace, method)

  @test trace[end] == trace2[end]

  @test measurements(trace, 1) == trace.ms[1]
  @test_throws BoundsError measurements(trace, 51)
  @test_throws BoundsError measurements(trace, 0)

  @test length(trace) == 51 && length(trace) == trace.steps + 1
  @test trace[0] === metrics(trace, 0) === trace.metrics[1]
  @test trace[end] === metrics(trace, 50) === trace.metrics[51]

  @test firstmetric(m -> m.photons > 5, trace) === trace[6]
  @test lastmetric(m -> m.photons > 5, trace) === trace[end]
  @test trace[10] === photonmap(trace, 10) do trace, step
    return trace[step]
  end

  @test all(isapprox.(trace.state.weights, trace2.state.weights))
  NanoLoc.forgetstate!(trace)
  NanoLoc.recallstate!(trace)
  @test all(isapprox.(trace.state.weights, trace2.state.weights))
end
