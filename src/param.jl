
"""
Model parameter.

Consists of a `D`-dimensional marker position `x` in space, a marker brightness
value `alpha`, and a background noise value `noise`.
"""
struct Param{D, F}
  x::Position{D, F}
  alpha::F
  noise::F
end

"""
Alias for `Union{Param, NTuple{_, Param}}` that is currently mainly intended
for dispatching.
"""
const MultiParam{D, F} = Union{Param{D, F}, NTuple{N, Param{D, F}} where N}

"""
    Param([F], x; alpha = 1, noise = 0)

Create a model parameter with position `x`, brightness `alpha`, and background
noise level `noise`. The floating point precision can be specified via `F`.
"""
function Param(x::Position{D, F}; alpha::Real = 1, noise::Real = 0) where {D, F}
  return Param(x, F(alpha), F(noise))
end

function Param(F::Type{<:AbstractFloat}, x; alpha::Real = 1, noise::Real = 0)
  D = length(x)
  return Param(SVector{D}(F.(x)), F(alpha), F(noise))
end

function Param(x; alpha::Real = 1, noise::Real = 0)
  return Param(Float64, x; alpha, noise)
end

function Base.:+(a::Param{D}, b::Param{D}) where {D}
  return Param(a.x + b.x, a.alpha + b.alpha, a.noise + b.noise)
end

function Base.:-(a::Param{D}, b::Param{D}) where {D}
  return Param(a.x - b.x, a.alpha - b.alpha, a.noise - b.noise)
end

"""
    setx(param, alpha)

Return a copy of `param` with position parameter set to `x`.
"""
function setx(p::Param{D, F}, x) where {D, F}
  return Param{D, F}(SVector{D, F}(x...), p.alpha, p.noise)
end
"""
    setalpha(param, alpha)

Return a copy of `param` with brightness parameter set to `alpha`.
"""
function setalpha(p::Param{D, F}, alpha) where {D, F}
  return Param{D, F}(p.x, F(alpha), p.noise)
end
"""
    scalealpha(param, scale)

Return a copy of `param` with brightness parameter scaled by `scale`.
"""
function scalealpha(p::Param{D, F}, scale::Real) where {D, F}
  return Param{D, F}(p.x, F(scale) * p.alpha, p.noise)
end

"""
    setnoise(param, noise)

Return a copy of `param` with noise parameter set to `noise`.
"""
function setnoise(p::Param{D, F}, noise) where {D, F}
  return Param{D, F}(p.x, p.alpha, F(noise))
end
"""
    scalenoise(param, scale)

Return a copy of `param` with noise parameter scaled by `scale`.
"""
function scalenoise(p::Param{D, F}, scale::Real) where {D, F}
  return Param{D, F}(p.x, p.alpha, F(scale) * p.noise)
end

"""
    adapt(F, param)

Adapt the floating point type of `param` to `F`.
"""
setprecision(p::Param{D, F}, ::Type{F}) where {D, F} = p

function setprecision(p::Param{D}, ::Type{F}) where {D, F}
  return Param(Position{D, F}(p.x), F(p.alpha), F(p.noise))
end

# Disable broadcasting over the fields of a Param
Base.broadcastable(p::Param) = Ref(p)

function Base.convert(::Type{Param{D, F}}, p::Param{D}) where {D, F}
  return setprecision(p, F)
end

function Base.isapprox(a::Param, b::Param; kwargs...)
  return all([
    isapprox(a.x, b.x; kwargs...),
    isapprox(a.alpha, b.alpha; kwargs...),
    isapprox(a.noise, b.noise; kwargs...),
  ])
end

function Base.show(io::IO, p::Param)
  x = Tuple(round.(Float64.(p.x), digits = 2))
  alpha = round(p.alpha; digits = 2)
  noise = round(p.noise; digits = 2)
  if iszero(p.noise)
    print(io, "Param($x, alpha = $(alpha))")
  else
    print(io, "Param($x, alpha = $(alpha), noise = $(noise))")
  end
end

function Base.show(io::IO, ::MIME"text/plain", p::Param{D, F}) where {D, F}
  x = Tuple(round.(Float64.(p.x), digits = 2))
  alpha = round(p.alpha; digits = 2)
  noise = round(p.noise; digits = 2)
  println(io, "Param{$D, $F}")
  println(io, " x     ", x)
  println(io, " alpha ", alpha)
  return print(io, " noise ", noise)
end

function pp(p::Param)
  parts = [
    "P(",
    map(xi -> @sprintf("%.1f, ", xi), p.x)...,
    @sprintf("a:%.1f, ", p.alpha),
    @sprintf("n:%.1f)", p.noise)
  ]
  return join(parts)
end
