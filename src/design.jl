
"""
Supertype for measurement designs.

Besides several auxiliary functions, each concrete design subtype must implement
[`likelihood`](@ref).
"""
abstract type Design{D, F, O} end

Base.broadcastable(d::Design) = Ref(d)

"""
    obstype(design :: Design)
    obstype(:: Type{<: Design})

Returns the type of observations associated to `design`.
"""
obstype(::Design{D, F, O}) where {D, F, O} = O

obstype(::Type{<:Design{D, F, O} where {D, F}}) where {O} = O

"""
    photoncount(design :: Design, obs)

Returns the number of photons of `obs` under `design`.
"""
photoncount(::Design, _obs) = error("Not implemented")

"""
    setprecision(design :: Design, F)

Adapt the floating point type of `design` to `F`.
"""
setprecision(::Design, ::Type) = error("Not implemented")

function Base.convert(
  ::Type{<:Design{D, F, O}},
  d::Design{D, F_, O},
) where {D, F, F_, O}
  return setprecision(d, F)
end

"""
    profiles(design :: Design)

Returns a vector of profiles associated to `design`.
"""
profiles(::Design) = error("Not implemented")

"""
    designpoints(design :: Design)

Return a vector of all spatial positions associated to `design`.
"""
designpoints(::Design) = error("Not implemented")

"""
    designcenter(design :: Design)

Return the geometric center of `design`.
"""
designcenter(::Design) = error("Not implemented")

"""
    designradius(design :: Design)

Returns the radius of the geometry of a `design`. Returns zero if only a single
measurement locus is involved.
"""
designradius(::Design) = 0.0f0

"""
    designextent(design :: Design)

Loose notion of a diameter within which `design` will interact meaningfully with
the parameter. By default, it equals the sum of [`designradius`](@ref) times two
and the fwhm of the widest profile in `design`.
"""
function designextent(design::Design)
  r = designradius(design)
  return if isnothing(r)
    maximum(fwhm, profiles(design))
  else
    2r + maximum(fwhm, profiles(design))
  end
end

"""
    setcenter(design :: Design, x)

Return a translated copy of `design` with design center `x`.
"""
setcenter(::Design, x) = error("Not implemented")

"""
    intensity(design :: Design, param)

Return the absolute emission intensity (including noise) that `design`
causes under `param`. Can be an iterable if `design` combines several
measurements.

If `param` is a tuple of values of type `Param`, returns the intensity sum.
"""
intensity(::Design, param) = error("Not implemented")

function intensity(
  design::Design{D, F},
  params::NTuple{N, Param{D, F}},
) where {D, F, N}
  return mapreduce(p -> intensity(design, p), (.+), params)
end

"""
    signalintensity(design :: Design, param)

Return the signal intensity of `design` under `param`, i.e., the intensity if
`param.noise` were zero.

If `param` is a tuple of values of type `Param`, returns the intensity sum.
"""
function signalintensity(design::Design, param)
  return intensity(design, setnoise(param, 0))
end

"""
    noiseintensity(design :: Design, param)

Return the noise intensity of `design` under `param`, i.e., the intensity if
`param.alpha` were zero.

If `param` is a tuple of values of type `Param`, returns the intensity sum.
"""
function noiseintensity(design::Design, param)
  return intensity(design, setalpha(param, 0))
end

"""
    loglikelihood(param, design :: Design, obs, [intensity])

Returns the log-likelihood of a parameter `param` when `obs` has been observed
under the measurement design `design`.

Optionally, a pre-computed `intensity` value obtained by `intensity(design,
param)` may be passed to accelerate computation.

If `param` is a tuple of values of type `Param`, the intensity sum is used for
likelihood computations.

See also [`likelihood`](@ref).
"""
function loglikelihood(param, design::Design, obs, intensity = 0)
  return error("Not implemented")
end

"""
    likelihood(param, design :: Design, obs, [intensity])

Returns the likelihood of a parameter `param` when `obs` has been observed under
the measurement design `design`.

Optionally, a pre-computed `intensity` value obtained by `intensity(design,
param)` may be passed to accelerate computation.

If `param` is a tuple of values of type `Param`, the intensity sum is used for
likelihood computations.

See also [`loglikelihood`](@ref).
"""
function likelihood(args...)
  return exp(loglikelihood(args...))
end

"""
    sbratio(design :: Design, param)

Calculate the signal-to-background ratio (SBR, signal intensity / noise
intensity) for a given design `design` and parameter `param`.
"""
function sbratio(design::Design{D, F}, param) where {D, F}
  signal = sum(signalintensity(design, param))
  noise = sum(noiseintensity(design, param))
  return if noise == 0 && signal > 0
    typemax(F)
  else
    signal / noise
  end
end

"""
    normalsbratio(design, x = designcenter(design))

Calculate `sbratio(design, Param(x, alpha = 1, noise = 1))`.

Designs with similar values of `normalsbratio` possess a similar signal-to-
background ratio when confronted with the same absolute noise levels.
"""
function normalsbratio(
  design::Design{D, F},
  x::Position{D, F} = designcenter(design),
) where {D, F}
  param = Param(x; alpha = 1, noise = 1)
  return sbratio(design, param)
end

"""
    setalpha(design, alpha)

Create a new design where the brightness values of all profiles within `design`
are set to `alpha`.
"""
setalpha(::Design, ::Real) = error("To be implemented")

"""
    scalealpha(design, scale)
    scalealpha(design; normalsbr)

Create a new design where the brightness values of all profiles within `design`
are scaled by `scale`. Alternatively, scale the brightness values to realize a
targeted value `normalsbratio(design) == normalsbr`
"""
scalealpha(::Design, ::Real) = error("To be implemented")

function scalealpha(design::Design; normalsbr::Real = 1)
  scale = normalsbr / normalsbratio(design)
  return scalealpha(design, scale)
end

"""
    rand([rng], design :: Design, param, [n])

Sample `n` observations from `design` under the model parameter `param`.

Optionally, a random number generator `rng` can be specified. The argument
`param` can also be a tuple of parameters.
"""
function Base.rand(rng::AbstractRNG, design::Design, param)
  return _takenth(rand(rng, design, param, 1), 1)
end

function Base.rand(design::Design, param)
  return rand(Random.default_rng(), design, param)
end

function Base.rand(design::Design, param, n::Integer)
  return rand(Random.default_rng(), design, param, n)
end

"""
    randbg([rng], design, param)

Sample a single observation from `design` under the model parameter `param` and
also return the number of background photons of the observation.

Optionally, a random number generator `rng` can be specified. 
"""
function randbg(rng::AbstractRNG, design::Design, param)
  return error("Not implemented")
end

function randbg(design::Design, param)
  return randbg(Random.default_rng(), design, param)
end

"""
Specification of intensity options for a design.

The design intensity can be specified in three different ways: By setting
* the alpha value of all profiles of the design to a value `alpha`.
* the total intensity at a specific point to a value `intensity`
* the signal-to-background ratio for a specific parameter to a value `sbr`.

---
    IntensityOptions(; alpha :: Real)
    IntensityOptions(; intensity :: Real [, reference])
    IntensityOptions(; sbr :: Real [, reference])

Create an `IntensityOptions` object.

If an intensity `intensity` is specified without a location `reference`,
the designcenter is used. If a signal-to-noise ratio `sbr` is specified without
explicitly setting a reference parameter `reference`, the normal signal-to-noise
ratio (see [`normalsbratio`](@ref)) is used.
"""
struct IntensityOptions
  sbr::Float64 # set the normalized signal to noise ratio
  alpha::Float64 # set the alpha values of all profiles in the design
  intensity::Float64 # set the intensity at the design center
  reference::Union{Nothing, Param} # Reference parameter used for sbr / intensity specification
end

function IntensityOptions(;
  sbr = 1,
  alpha = -1,
  intensity = -1,
  reference = nothing,
)
  if alpha > 0
    return IntensityOptions(-1, alpha, -1, nothing)
  elseif intensity > 0
    if !(reference isa Union{Nothing, Param})
      reference = Param(reference; alpha = 1, noise = 1) # interpret as position
    end
    return IntensityOptions(-1, -1, intensity, reference)
  elseif sbr > 0
    @assert isnothing(reference) || reference.noise > 0 """
    Cannot specify signal-to-background ratio with given options.
    """
    return IntensityOptions(sbr, -1, -1, reference)
  else
    throw(ArgumentError("Invalid arguments for IntensityOption."))
  end
end

function Base.convert(::Type{IntensityOptions}, kwargs::NamedTuple)
  return IntensityOptions(; kwargs...)
end

function pp(intensity::IntensityOptions)
  if intensity.alpha > 0
    @sprintf "alpha %.2f" intensity.alpha
  elseif intensity.intensity > 0
    spot = isnothing(intensity.reference) ? "center" : intensity.reference.x
    @sprintf "%.2f (%s)" intensity.intensity spot
  else
    reference =
      isnothing(intensity.reference) ? "normal" : pp(intensity.reference)
    @sprintf "sbr %.2f (%s)" intensity.sbr reference
  end
end

"""
    setintensity(design :: Design, options :: IntensityOptions)

Scale the alpha values of all profiles in `design` to meet the intensity
specification declared by `options`.

See also [`setalpha`](@ref) and [`scalealpha`](@ref).
"""
function setintensity(design::Design, options::IntensityOptions)
  if isnothing(options.reference)
    param = Param(designcenter(design); alpha = 1, noise = 1)
  end
  if options.alpha > 0
    return setalpha(design, options.alpha)
  elseif options.intensity > 0
    scale = options.intensity / sum(signalintensity(design, param))
    return scalealpha(design, scale)
  elseif options.sbr > 0
    scale = options.sbr / sbratio(design, param)
    return scalealpha(design, scale)
  else
    throw(ArgumentError("Invalid intensity options."))
  end
end

function setintensity(design::Design, options)
  return setintensity(design, convert(IntensityOptions, options))
end

"""
Measurements are pairs of a design and a compatible observation.
"""
struct Measurement{O, T <: Design{D, F, O} where {D, F}}
  design::T
  obs::O
end

"""
    likelihood(param, measurement)

Calculate the likelihood of `param` given `measurement`.
"""
function likelihood(param, m::Measurement)
  return likelihood(param, m.design, m.obs)
end

"""
    loglikelihood(param, measurement)

Calculate the log-likelihood of `param` given `measurement`.
"""
function loglikelihood(param, m::Measurement)
  return loglikelihood(param, m.design, m.obs)
end

"""
    loglikelihood(param, measurements)

Calculate the log-likelihood of `param` given a vector of `measurements`.
"""
function loglikelihood(param, ms::AbstractVector{<:Measurement})
  @assert length(ms) > 0 """
  Cannot evaluate the likelihood for empty evidence.
  """
  return sum(ms) do m
    return loglikelihood(param, m)
  end
end

function loglikelihood(param, ms::NTuple{N, <:Measurement}) where {N}
  # @assert N > 0 """
  # Cannot evaluate the likelihood for empty evidence.
  # """
  # For some reason, this does not work inside of a CUDA kernel
  # But it should?
  # return sum(ms) do m
  #   return loglikelihood(param, m)
  # end
  l = 0
  for m in ms
    l += loglikelihood(param, m)
  end
  return l
end

function setprecision(m::Measurement, ::Type{F}) where {F}
  return Measurement(setprecision(m.design, F), m.obs)
end
