module NanoLoc

using Random, Statistics, LinearAlgebra
using StaticArrays
using Printf
using Serialization: Serialization

using StatsBase: StatsBase
import Distributions: UnivariateDistribution
import Distributions: Bernoulli, Binomial, Poisson, Multinomial, Exponential
import Distributions: MvNormal
import Distributions: pdf, logpdf

import Interpolations: BSpline, Linear, Cubic, NoInterp
import Interpolations: interpolate

using Bessels: Bessels
using GZip: GZip

"""
Position in `D`-dimensional space with floating point precision `F`.
"""
const Position{D, F} = SVector{D, F}

"""
    _takenth(a, n)

Returns the `n`-th element of an array `a`.
If `a` is a `CuArray`, `CUDA.@allowscalar` is used to suppress scalar indexing
warnings if CUDA is loaded.
"""
_takenth(a::AbstractArray, n) = a[n]

"""
    sample_indices([rng], weights, [n])

Sample `n` indices from the weight vector `weights`. Optionally, a random
number generator `rng` may be passed.
"""
function sample_indices(rng::AbstractRNG, weights::Array{<:Real}, n::Integer)
  weights = StatsBase.Weights(weights)
  return StatsBase.sample(rng, 1:length(weights), weights, n)
end

sample_indices(args...) = sample_indices(Random.default_rng(), args...)

"""
    @maythread usethreads for ... end

Dynamically dispatch between `Threads.@threads for ... end` if `usethreads ==
true` and `for ... end` if `usethreads == false`.
"""
macro maythread(threads, ex)
  return quote
    if $threads
      Threads.@threads $ex
    else
      $ex
    end
  end |> esc
end

"""
    pp(value)
    pp(io, value)

Return a short and descriptive string representation of `value`.
If the argument `io` is passed, the value is printed to `io`.
"""
pp(io::IO, val) = print(io, pp(val))

pp(_) = error("Not implemented")

pp(val::AbstractFloat; digits = 1) = string(round(val; digits = digits))

"""
Supported array backends.

Can be extended safely in the __init__ method of package extensions.
"""
const _atypes = Dict{Symbol, Type{<:AbstractArray}}(
  :default => Array{Float64},
  :default64 => Array{Float64},
  :default32 => Array{Float32},
)

function _atype(sym::Symbol)
  return if sym in keys(_atypes)
    _atypes[sym]
  else
    @warn """
    Requested array backend :$sym not supported. Falling back to :default.
    """
    _atypes[:default]
  end
end

function _atype(T::Type{<:AbstractArray{F}}) where {F <: AbstractFloat}
  return T
end

include("param.jl")
include("profile.jl")
include("design.jl")
include("method.jl")
include("policy.jl")
include("trace.jl")
include("stack.jl")

include("designs/poisson.jl")
include("designs/bernoulli.jl")
include("designs/exponential.jl")
include("designs/circle.jl")
include("designs/compound.jl")
include("designs/multinomial.jl")
include("designs/timed.jl")
include("designs/minflux.jl")
include("designs/rastmin.jl")

include("paramdist/dist.jl")
include("paramdist/discrete.jl")
include("paramdist/stats.jl")
include("paramdist/posterior.jl")
include("paramdist/utility.jl")
include("paramdist/adaptor.jl")

include("methods/dummy.jl")
include("methods/bayesgrid.jl")
include("methods/gausssampling.jl")

include("policies/minflux.jl")
include("policies/rastmin.jl")
include("policies/minsted.jl")
include("policies/simplebed.jl")
include("policies/minfluxbayes.jl")
include("policies/minfluxbed.jl")
include("policies/minstedbayes.jl")
include("policies/minstedbed.jl")

include("precompile.jl")

export Position, Param, Design, Measurement
export ParamDist, DiscreteParamDist, ParamList, ParamGrid
export Profile, RadialProfile, DirectedProfile
export GaussProfile, QuadraticProfile, DonutProfile

export Design,
  PoissonDesign,
  BernoulliDesign,
  ExponentialDesign,
  CircleDesign,
  CompoundDesign,
  MultinomialDesign,
  MinfluxDesign,
  RastminDesign,
  TimedDesign,
  TimedCircleDesign,
  TimedMultinomialDesign

export InferenceMethod, InferenceState

export BayesGrid, GaussSamplingMethod

export Criterion,
  TotalVar,
  PositionVar,
  BrightnessVar,
  NoiseVar,
  TotalEntropy,
  PositionEntropy,
  BrightnessEntropy,
  NoiseEntropy

export Policy,
  Minflux,
  Rastmin,
  Minsted,
  MinstedBayes,
  MinstedBED,
  MinfluxBayes,
  MinfluxBED,
  SimpleBED

export NoAdaptor, DyadicCropAdaptor
export Trace, SimulatedTrace, Stack

export setx, setalpha, setnoise, scalealpha, scalenoise
export setcenter, designcenter
export setintensity, settiming
export weights, params
export marginalize, marginalizec
export posterior, posteriorm, posterior!, posteriorm!
export photoncount, likelihood, utility, utilitys
export sbratio, normalsbratio
export localize, relocalize, metrics, measurements
export trim
export firstmetric, lastmetric, stepsneeded, photonsneeded, photonmap
export hpdset, hpdsetdiamlb, hpdsetdiamub

end # module NanoLoc
