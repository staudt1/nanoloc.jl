
"""
Abstract type representing localization traces.
"""
abstract type Trace{D, S <: InferenceState{D}, I <: InferenceMethod{S}} end

"""
    measurements(trace :: Trace)
    measurements(trace, steps)

Return the measurements of `trace`.

Optionally, a selection of `steps` between `1` (first measurement) and
`trace.steps` (final measurement) can be provided.
"""
measurements(t::Trace) = t.ms

function measurements(t::Trace, step::Integer)
  if !(1 <= step <= length(t))
    throw(BoundsError(t, step))
  end
  return t.ms[step]
end

function measurements(t::Trace, steps)
  if !(maximum(steps) <= length(t) && minimum(steps) >= 1)
    throw(BoundsError(t, steps))
  end
  return t.ms[steps]
end

"""
    metrics(trace :: Trace)
    metrics(trace, steps)

Return the metric recordings of `trace`. Optionally, a selection of `steps`
between `0` (before first measurement) and `length(trace)` (after final
measurement) can be provided.

Note: For convenience, indexing into a trace via `trace[steps]` returns
`metrics(trace, steps)`.
"""
metrics(t::Trace) = t.metrics

function metrics(t::Trace, step::Integer)
  if !(0 <= step <= length(t))
    throw(BoundsError(t, step))
  end
  return t.metrics[step + 1]
end

function metrics(t::Trace, steps)
  if !(maximum(steps) <= length(t) && minimum(steps) >= 0)
    throw(BoundsError(t, steps))
  end
  return t.metrics[steps .+ 1]
end

function Base.getindex(t::Trace, s)
  return metrics(t, s)
end

Base.firstindex(t::Trace) = 0
Base.lastindex(t::Trace) = t.steps

"""
    firstmetric(condition, trace :: Trace)

Returns the first entry in `trace.metrics` that satisfies `condition`. Returns
`nothing` if no entry does.

See also [`lastmetric`](@ref).
"""
function firstmetric(cond, trace::Trace)
  index = findfirst(cond, trace.metrics)
  return isnothing(index) ? nothing : trace.metrics[index]
end

"""
    lastmetric(condition, trace :: Trace)

Returns the last entry in `trace.metrics` that satisfies `condition`. Returns
`nothing` if no value satisfies `condition`.

See also [`firstmetric`](@ref).
"""
function lastmetric(cond, trace::Trace)
  index = findlast(cond, trace.metrics)
  return isnothing(index) ? nothing : trace.metrics[index]
end

"""
    photonmap(f, trace :: Trace, n; count_bg = true)

Return `f(trace, step)`, where `step` is the latest step such that `<=
n` photons were observed.

If `count_bg = false`, only signal photons are counted.
"""
function photonmap(f, trace::Trace, n; count_bg = true)
  return if count_bg
    _photonmap(f, trace, n)
  else
    _photonmap_nobg(f, trace, n)
  end
end

function _photonmap(f, trace::Trace, n::Integer)
  @assert 0 <= n "Expected non-negative photon number"
  @assert n <= trace.photons "Trace has fewer than $n photons"
  step = findlast(mc -> mc.photons <= n, trace.metrics) - 1
  return f(trace, step)
end

function _photonmap_nobg(f, trace::Trace, n::Integer)
  @assert 0 <= n "Expected non-negative photon number"
  @assert n <= trace.photons - trace.bgphotons """
  Trace has fewer than $n signal photons
  """
  step = findlast(mc -> mc.photons - mc.bgphotons <= n, trace.metrics) - 1
  return f(trace, step)
end

"""
    photonmap(f, trace :: Trace; count_bg = true)

Calculate `photonmap(f, trace, n)` for each valid photon count `n`, starting at `n = 1`.

If `count_bg = false`, background photons are not counted and `n` corresponds to
the number of signal photons.
"""
function photonmap(f, trace::Trace; count_bg::Bool = true)
  # TODO: make this more efficient!
  photons = count_bg ? trace.photons : trace.photons - trace.bgphotons
  return map(n -> photonmap(f, trace, n; count_bg), 0:photons)
end

"""
    stepsneeded(f, trace :: Trace)

Compute the number `s` of steps such that `f(trace, steps)` is true for all
`steps >= s`. Returns `nothing` if `f(trace, trace.steps)` is false.

See also [`photonsneeded`](@ref).
"""
function stepsneeded(f, trace::Trace)
  s = nothing
  for step in (trace.steps):-1:0
    if f(trace, step)
      s = step
    else
      return s
    end
  end
  return nothing
end

"""
    photonsneeded(f, trace :: Trace; count_bg = true)

Compute the number `n` of photons that are needed such that `f(trace, steps)`
is true for all steps with `trace[steps].photons >= n`. Returns
`nothing` if `f(trace, trace.steps)` is `false`.

If `count_bg = false`, background photons are subtracted from the returned
counts.

See also [`stepsneeded`](@ref).
"""
function photonsneeded(f, trace::Trace; count_bg::Bool = true)
  n = nothing
  for step in (trace.steps):-1:1
    if f(trace, step)
      n = metrics(trace, step).photons
      if !count_bg
        n -= metrics(trace, step).bgphotons
      end
    else
      return n
    end
  end
  return 1
end

"""
    forgetstate!(trace :: Trace)

Remove the initial and final inference states from `trace`.

Useful to save memory when many traces are generated. Can be undone via
[`recallstate!`](@ref).
"""
function forgetstate!(trace::Trace)
  trace.initial_state = nothing
  return trace.state = nothing
end

"""
    recallstate!(trace :: Trace)

Recompute the initial and final inference state of `trace`.

Undos [`forgetstate!`](@ref).
"""
function recallstate!(trace::Trace)
  if isnothing(trace.state)
    for t in trace
      trace.initial_state = t.initial_state
      trace.state = t.state
    end
  end
  return nothing
end

# TODO: make this function more generic.
# In particular, implement arbitrary stop criteria for trimming.
"""
    trim(trace, steps)

Return a version of trace that is reduced to the first `steps` steps. 

For performance reasons, the state of the returned trace is forgotten (see [forgetstate!](@ref) and [recallstate!](@ref)).
"""
function trim(trace::Trace, steps::Integer)
  steps = clamp(steps, 0, trace.steps)
  trace = copy(trace)
  trace.steps = steps
  trace.photons = trace.metrics[steps + 1].photons

  trace.ms = trace.ms[1:steps]
  trace.updates = trace.updates[1:steps]
  trace.metrics = trace.metrics[1:(steps + 1)]
  forgetstate!(trace)

  return trace
end

"""
    diagnose(trace :: Trace)

Return the health status of `trace`.
"""
diagnose(trace::Trace) = trace.health

"""
    ishealthy(trace :: Trace) 

Return whether `trace` is healthy or not.
"""
ishealthy(trace::Trace) = trace.health.healthy

Base.length(trace::Trace) = trace.steps + 1 # length when iterating over trace

"""
    Base.copy(trace :: Trace)

Shallow copy of the localization trace `trace`.
"""
function Base.copy(trace::T) where {T <: Trace}
  fields = Base.fieldnames(T)
  values = (getfield(trace, field) for field in fields)
  return T(values...)
end

"""
Observed localization trace.
"""
struct ObservedTrace{D, S, I <: InferenceMethod{S}} <: Trace{D, S, I}
  # TODO: implement suitable methods  for this type!
  steps::Int
  photons::Int

  initial_state::Union{Nothing, S}
  state::Union{Nothing, S}

  method::I
  ms::Vector{Measurement}
  metrics::Vector{NamedTuple}
  updates::Vector{NamedTuple}
end

"""
Simulated localization trace.

In addition to measurements and recorded metrics, this trace also stores the
design policy and the true parameter for the simulation.
"""
mutable struct SimulatedTrace{D, S, I <: InferenceMethod{S}} <: Trace{D, S, I}
  steps::Int
  photons::Int
  bgphotons::Int

  initial_state::Union{Nothing, S}
  state::Union{Nothing, S}

  method::I
  ms::Vector{Measurement}
  metrics::Vector{NamedTuple}
  updates::Vector{NamedTuple}
  health::NamedTuple

  param::Param{D}
  policy::Policy
end

function SimulatedTrace(policy::Policy, method::InferenceMethod, param)
  ms = Vector{Measurement}()
  metrics = Vector{NamedTuple}()
  updates = Vector{NamedTuple}()
  initial_state = init(method, param)
  return SimulatedTrace(
    0,
    0,
    0,
    initial_state,
    initial_state,
    method,
    ms,
    metrics,
    updates,
    (;),
    param,
    policy,
  )
end

function _computemetrics(trace::SimulatedTrace, metrics::NamedTuple)
  est = estimate(Float32, trace.state)
  std = estimatestd(Float32, trace.state)
  err = setprecision(trace.param - est, Float32)
  if !isempty(trace.ms)
    design = trace.ms[end].design
    sbr = sbratio(design, trace.param)
    int = sum(intensity(design, trace.param))
    signal = sum(intensity(design, setnoise(trace.param, 0)))
    noise = sum(intensity(design, setalpha(trace.param, 0)))
  else
    sbr = int = signal = noise = NaN
  end
  return (;
    trace.steps,
    trace.photons,
    trace.bgphotons,
    signalphotons = trace.photons - trace.bgphotons,
    est,
    std,
    err,
    stdx = sqrt(sum(abs2, std.x)),
    errx = sqrt(sum(abs2, err.x)),
    sbr = Float32(sbr),
    intensity = Float32(int),
    signalintensity = Float32(signal),
    noiseintensity = Float32(noise),
    map(f -> f(trace), metrics)...,
  )
end

function _computemetrics(trace::SimulatedTrace, ::Nothing)
  return (; trace.steps, trace.photons, trace.bgphotons)
end

function _printprogress(trace::SimulatedTrace)
  println("-- step $(trace.steps) --")
  println("photons $(trace.photons) / $(trace.bgphotons)")
  println("design  $(pp(trace.ms[end].design))")
  println("obs     $(trace.ms[end].obs)")
  @printf "sbr     %.2f\n" trace.metrics[end].sbr
  @printf "stdx    %.2f\n" trace.metrics[end].stdx
  @printf "errx    %.2f\n" trace.metrics[end].errx
end

"""
    localize(policy :: Policy, method :: InferenceMethod, param; kwargs...])

Simulate a localization process for the parameter `param`, returning a
[`SimulatedTrace`](@ref).

The argument `policy` determines the designs used for the measurements in each step. 
The argument `method` determines the inference method used to incrementally
update the current state of knowledge about the parameter.

## Keyword arguments
- `stop = t -> t.photons >= 100`: Stopping criterion that ends the localization.
- `forget_state = false`: Whether to remove the initial and final states from \
  the returned trace (in order to save memory).
- `verbose = false`: Whether to print progress info during the localization.
- `rng`: Random number generate to be used.
- `metrics`: Named tuple of metrics to be recorded during the localization.
- `callback`: Optional callback function called after each step.
"""
function localize(
  policy::Policy,
  method::InferenceMethod,
  param::Param;
  stop = t -> t.photons >= 100,
  callback = _ -> nothing,
  metrics = (;),
  forget_state = false,
  verbose::Bool = false,
  rng::AbstractRNG = Random.default_rng(),
)

  # Initialize the trace and inference state
  trace = SimulatedTrace(policy, method, param)
  trace.metrics = [_computemetrics(trace, metrics)]

  # Conduct inference steps as long as the stop criterion is not reached
  while !stop(trace)
    design = policy(trace)
    obs, bg = randbg(rng, design, param)

    trace.steps += 1
    trace.photons += photoncount(design, obs)
    trace.bgphotons += bg

    m = Measurement(design, obs)

    push!(trace.ms, m)
    trace.state, info = update(method, trace.state, trace.ms)

    push!(trace.updates, info)
    push!(trace.metrics, _computemetrics(trace, metrics))

    if verbose
      _printprogress(trace)
    end

    callback(trace)
  end

  @assert trace.steps > 0 """
  Stop criterion caused empty trace.
  """

  if verbose
    println("-- finished --")
  end

  trace.health = diagnose(method, trace.state)

  if forget_state
    trace.initial_state = nothing
    trace.state = nothing
  end

  return trace
end

function localize(policy::Policy, param::Param{D}; kwargs...) where {D}
  xs = fill(0:3:300, D)
  method = BayesGrid(xs...; alpha = :known, noise = :known)
  @info """
  The method `localize` was called without an explicit inference method.
  Assuming a uniform $method that spans a spatial window from 0 to 300.
  """
  return localize(policy, method, param; kwargs...)
end

"""
    relocalize(trace, method :: InferenceMethod; kwargs...)

Re-localize `trace` with the alternative inference method `method`.

Accepts the keyword arguments `metrics`, `forget_state`, and `callback` (see
[`localize`](@ref)).
"""
function relocalize(
  t::SimulatedTrace,
  method::InferenceMethod;
  metrics = (;),
  forget_state::Bool = false,
  callback = _ -> nothing,
)
  trace = SimulatedTrace(t.policy, method, t.param)
  trace.metrics = [_computemetrics(trace, metrics)]

  for step in 1:(t.steps)
    m = NanoLoc.measurements(t, step)
    r = NanoLoc.metrics(t, step)

    trace.steps += 1
    trace.photons = r.photons
    trace.bgphotons = r.bgphotons

    push!(trace.ms, m)
    trace.state, info = update(method, trace.state, trace.ms)

    push!(trace.updates, info)
    push!(trace.metrics, _computemetrics(trace, metrics))

    callback(trace)
  end

  trace.health = diagnose(method, trace.state)

  if forget_state
    trace.initial_state = nothing
    trace.state = nothing
  end

  return trace
end

function Base.iterate(t::SimulatedTrace)
  trace = SimulatedTrace(t.policy, t.method, t.param)
  trace.metrics = t.metrics[1:1]
  return trace, (; trace, step = 1)
end

function Base.iterate(t::SimulatedTrace, state)
  if state.step > t.steps
    return nothing
  end
  trace = state.trace
  step = state.step

  mc = metrics(t, step)
  trace.steps = mc.steps
  trace.photons = mc.photons
  trace.bgphotons = mc.bgphotons

  trace.ms = t.ms[1:step]
  trace.state, _ = update(t.method, trace.state, trace.ms)

  trace.metrics = t.metrics[1:(step + 1)]
  trace.updates = t.updates[1:step]

  if state.step == length(t)
    trace.health = diagnose(t.method, trace.state)
  end

  return trace, (; trace, step = state.step + 1)
end

function Base.show(io::IO, trace::SimulatedTrace)
  return print(
    io,
    "SimulatedTrace(",
    trace.steps,
    "steps, ",
    trace.photons,
    "photons)",
  )
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  trace::SimulatedTrace{D, S, I},
) where {D, S, I}
  stdx = trace.metrics[end].stdx
  errx = trace.metrics[end].errx
  health = trace.health.healthy ? "" : "(unhealthy)"
  println(io, "SimulatedTrace $health")
  println(io, " policy   ", policyname(trace.policy))
  println(io, " method   ", trace.method)
  println(io, " upgrades ", count(u -> u.upgrade, trace.updates))
  println(io, " steps    ", trace.steps)
  println(io, " photons  ", trace.photons, " / ", trace.bgphotons, " bg")
  println(io, " param    ", pp(trace.param))
  println(io, " estimate ", pp(trace.metrics[end].est))
  return print(io, " err/std  ", @sprintf("%.2f / %.2f", errx, stdx))
end

"""
    policy(trace :: SimulatedTrace)

Return the policy that `trace` was simulated with.
"""
policy(t::SimulatedTrace) = t.policy

"""
    policyname(trace :: SimulatedTrace)

Return the name of the policy that trace was simulated with.

See also [`policyoptions`](@ref).
"""
policyname(trace::SimulatedTrace) = policyname(policy(trace))

"""
    policyoptions(trace :: SimulatedTrace)

Return the options of the policy that trace was simulated with.

See also [`policyname`](@ref).
"""
policyoptions(trace::SimulatedTrace) = policyoptions(policy(trace))
