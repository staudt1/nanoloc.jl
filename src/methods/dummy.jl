
"""
Dummy inference state serving [`DummyInferenceMethod`](@ref) for testing
purposes.
"""
struct DummyInferenceState{D} <: InferenceState{D}
  param::Param{D}
  std::Float64
  steps::Int
end

"""
Dummy inference method for testing purposes.

If informed by the true parameter (i.e., in simulations where the parameter
is known), this method produces states that randomly sample estimates with a
variance that decreases linearly in the number of measurement steps.
"""
struct DummyInferenceMethod{D} <: InferenceMethod{DummyInferenceState{D}}
  std::Float64
end

DummyInferenceMethod() = DummyInferenceMethod{2}(100)

function init(m::DummyInferenceMethod{D}) where {D}
  # observed traces
  param = Param(zeros(D); alpha = 1, noise = 0)
  return DummyInferenceState{D}(param, m.std, 1)
end

function init(m::DummyInferenceMethod{D}, param) where {D}
  # simulated traces
  return DummyInferenceState{D}(param, m.std, 1)
end

function update(::DummyInferenceMethod, s::DummyInferenceState, ms)
  meta = (; upgrade = false)
  s = DummyInferenceState(s.param, s.std, s.steps + 1)
  return (s, meta)
end

function estimate(s::DummyInferenceState{D}) where {D}
  x = s.std / sqrt(s.steps) * randn(D) .+ s.param.x
  return Param(x; alpha = 1, noise = 0)
end

function estimatecov(s::DummyInferenceState{D}) where {D}
  return s.std^2 ./ s.steps .* diagm(ones(D + 2))
end
