
"""
Inference state for the [`GaussSamplingMethod`](@ref).
"""
struct GaussSamplingState{D} <: InferenceState{D}
  rng::AbstractRNG
  dist::MvNormal
  sample_history::Array{ParamList{D, Float64}}
  alpha::Float64 # TODO: make the state store a distribution for alpha
  noise::Float64 # TODO: make the state store a distribution for noise
end

"""
Method that approximates the spatial distribution of the parameter by a Gaussian
distribution. Updates are implemented by calculating the prior-posterior
transition on a number of sample points.

!!! note

    Currently this method always assumes the brightness and noise parameters to
    be known!
"""
struct GaussSamplingMethod{D} <: InferenceMethod{GaussSamplingState{D}}
  rng::Random.AbstractRNG
  samples::Int
  cov_factor::Float64
  init_mean::Position{D, Float64}
  init_cov::SMatrix{D, D, Float64}
  # TODO: add prior distributions for alpha and noise!
end

function GaussSamplingMethod(
  init_mean,
  init_std = nothing;
  alpha = :known,
  noise = :known,
  rng = Xoshiro(),
  samples::Integer = 100,
  cov_factor::Real = 2,
  init_cov = nothing,
)
  @assert samples > 0
  @assert cov_factor > 0

  @assert alpha == :known """
  Currently, only the option alpha == :known is implemented.
  """
  @assert noise == :known """
  Currently, only the option noise == :known is implemented.
  """

  @assert !isnothing(init_std) || !isnothing(init_cov) """
  The keyword argument init_cov must be passed since no initial standard
  deviation has been specified.
  """

  if !isnothing(init_cov) && !isnothing(init_std)
    @warn """
    Ignoring the specified initial standard deviation $init_std since an
    explicit covariance matrix has been passed via init_cov.
    """
  end

  D = length(init_mean)

  if isnothing(init_cov)
    init_cov = init_std^2 * SMatrix{D, D, Float64}(I)
  end

  return GaussSamplingMethod{D}(rng, samples, cov_factor, init_mean, init_cov)
end

function init(method::GaussSamplingMethod{D}, param::Param{D}) where {D}
  rng = copy(method.rng)
  dist = MvNormal(method.init_mean, method.init_cov)
  return GaussSamplingState{D}(rng, dist, [], param.alpha, param.noise)
end

function init(method::GaussSamplingMethod{D}) where {D}
  @warn """
  The method GaussSamplingMethod currently only works as intended when
  brightness and noise values are known. Assuming alpha = 1, noise = 0.
  """
  x = zeros(D)
  return init(method, Param(x; alpha = 1, noise = 0))
end

function update(
  method::GaussSamplingMethod{D},
  state::GaussSamplingState{D},
  ms::Vector{<:Measurement},
) where {D}
  measurement = ms[end]
  prior_dist = state.dist

  proposal_mean = mean(prior_dist)
  proposal_cov = method.cov_factor * cov(prior_dist)
  proposal_dist = MvNormal(proposal_mean, proposal_cov)

  # approximate prior distribution by a paramlist via Gauss sampling
  rng = copy(state.rng)
  points = rand(rng, proposal_dist, method.samples)

  params = mapslices(points; dims = 1) do x
    return Param(x; state.alpha, state.noise)
  end
  params = reshape(params, :)

  # correction for cov_factor
  weights = map(params) do param
    return pdf(prior_dist, param.x) / pdf(proposal_dist, param.x)
  end
  paramlist = ParamList(params; weights)

  # obtain posterior
  paramlist = posterior(paramlist, measurement)

  new_history = [state.sample_history; paramlist]
  # extract new normal approximation
  new_mean = mean(paramlist).x
  new_cov = Symmetric(cov(paramlist)[1:D, 1:D]) # Better ways to estimate cov?
  new_dist = MvNormal(new_mean, new_cov)

  info = (; upgrade = false)

  return GaussSamplingState{D}(
    rng,
    new_dist,
    new_history,
    state.alpha,
    state.noise,
  ),
  info
end

function estimate(state::GaussSamplingState)
  x = mean(state.dist)
  return Param(x; state.alpha, state.noise)
end

# TODO: This is nonsense for alpha and noise covariances
function estimatecov(state::GaussSamplingState{D}) where {D}
  c = Matrix{Float64}(I, D + 2, D + 2)
  c[1:D, 1:D] .= cov(state.dist)
  return c
end

function NanoLoc.diagnose(
  method::GaussSamplingMethod,
  state::GaussSamplingState,
)
  POINTHRESHOLD = 0.01 #Todo: find out how to do magic numbers properly
  WEIGHTTHRESHOLD = 0.99
  lastPoint = max(1, ceil(Integer, POINTHRESHOLD * method.samples))

  lastWeights = state.sample_history[end].weights

  maxWeight = sum(lastWeights) * WEIGHTTHRESHOLD
  topWeights = sort(lastWeights; rev = true)[1:lastPoint]

  healthy = sum(topWeights) <= maxWeight

  if !healthy
    @warn """
    The top $(POINTHRESHOLD * 100)% of points contain more than $(WEIGHTTHRESHOLD * 100)% of the total mass. This is a sign that the proposal density is a bad choice
    """
  end
  return (; healthy)
end
function Base.show(
  io::IO,
  ::MIME"text/plain",
  method::GaussSamplingMethod{D},
) where {D}
  println(io, "GaussSamplingMethod{$D}")
  return println(io, " samples $(method.samples)")
end

function Base.show(io::IO, method::GaussSamplingMethod{D}) where {D}
  samples = method.samples
  return print(io, "GaussSamplingMethod{$D}($samples)")
end
