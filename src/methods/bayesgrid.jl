
"""
Inference method that applies Bayesian updates to a [`GenericParamGrid`](@ref).

---

    BayesGrid(args...; adaptor = DyadicCropAdaptor(), kwargs...)

Create a `BayesGrid` inference method. It accepts the same arguments and
keyword arguments as [`ParamGrid`](@ref), an instance of which it constructs on
initialization of the method.

It additionally accepts a keyword argument `adaptor` of type
[`GridAdaptor`](@ref), which determines how the parameter grid is adapted if the
posterior distribution cannot be captured well by the original grid geometry.
"""
struct BayesGrid{D} <: InferenceMethod{GenericParamGrid{D}}
  x::NTuple{D, Vector{Float32}} # Float32 used for memory efficiency
  alpha::Vector{Float32}
  noise::Vector{Float32}
  prior_weights::Any # callable that maps parameters to prior weights
  adaptor::GridAdaptor
  atype::Symbol
end

function BayesGrid(
  xs::Vararg{Any, D};
  alpha = :known,
  noise = :known,
  weights = :constant,
  adaptor = DyadicCropAdaptor(),
  atype = :default,
) where {D}
  if alpha == :known
    alpha = Float32[]
  elseif alpha isa Real
    alpha = Float32[alpha]
  else
    alpha = convert(Vector{Float32}, alpha)
  end

  if noise == :known
    noise = Float32[]
  elseif noise isa Real
    noise = Float32[noise]
  else
    noise = convert(Vector{Float32}, noise)
  end

  xs = map(x -> convert(Array{Float32}, x), xs)
  return BayesGrid{D}(xs, alpha, noise, weights, adaptor, atype)
end

function init(b::BayesGrid{D}) where {D}
  @assert !isempty(b.alpha) """
  Cannot initialize BayesGrid without parameter since alpha is unknown.
  """
  @assert !isempty(b.noise) """
  Cannot initialize BayesGrid without parameter since noise is unknown.
  """
  return ParamGrid(b.x...; b.alpha, b.noise, b.atype, weights = b.prior_weights)
end

methodname(b::BayesGrid) = "BayesGrid"

function methodoptions(b::BayesGrid)
  xrange = map(b.x) do xi
    min, max = round.(Int, extrema(xi))
    return "$min-$max"
  end
  if length(b.alpha) == 0
    arange = "auto"
  elseif length(b.alpha) == 1
    arange = string(round(b.alpha; digits = 2))
  else
    min, max = round.(extrema(b.alpha), digits = 2)
    arange = "$min-$max"
  end
  if length(b.noise) == 0
    nrange = "auto"
  elseif length(b.noise) == 1
    nrange = string(round(b.noise; digits = 2))
  else
    min, max = round.(extrema(b.noise), digits = 2)
    nrange = "$min-$max"
  end

  return [
    "gridsize" => join(size(b), "×"),
    "xrange" => "(" * join(xrange, ", ") * ")",
    "alpha" => arange,
    "noise" => nrange,
  ]
end

function init(b::BayesGrid{D}, param::Param{D}) where {D}
  alpha = isempty(b.alpha) ? param.alpha : b.alpha
  noise = isempty(b.noise) ? param.noise : b.noise
  return ParamGrid(b.x...; alpha, noise, b.atype, weights = b.prior_weights)
end

function update(b::BayesGrid, dist::GenericParamGrid, ms)
  post = posterior(dist, ms[end])
  post, info = b.adaptor(post)
  return post, info
end

function diagnose(::BayesGrid, dist::GenericParamGrid)
  healthy = true

  # check if mass is concentrated on a small fraction of grid points
  wsorted = sort(weightsc(dist); rev = true) # largest values first
  l = ceil(Int, length(wsorted) / 100)
  concentration = sum(wsorted[1:l])
  if concentration > 0.95
    @warn """
    Parameter grid concentrates more than 95% of mass on less than 1% of \
    support points.
    """
    healthy = false
  end

  # check if the posterior mean is very close to the border of the grid.
  x = estimatex(dist)
  xmin = minimum.(dist.x)
  xmax = maximum.(dist.x)
  xdiam = xmax .- xmin
  xdist = min.(min.(abs.(x .- xmin)), min.(abs.(x .- xmax)))
  offset = 1 - minimum(xdist ./ xdiam)
  if offset > 0.95
    @warn """
    Parameter grid concentrates mass near the grid border (offset $offset)"
    """
    healthy = false
  end

  return (; healthy, concentration, offset)
end

function Base.size(b::BayesGrid)
  xsize = map(length, b.x)
  asize = length(b.alpha)
  nsize = length(b.noise)
  return (xsize..., max(1, asize), max(1, nsize))
end

function Base.show(io::IO, b::BayesGrid)
  if b.atype == :default
    print(io, "BayesGrid(", join(size(b), "×"), ")")
  else
    print(io, "BayesGrid(:", b.atype, ", ", join(size(b), "×"), ")")
  end
end

function Base.show(io::IO, ::MIME"text/plain", b::BayesGrid)
  xrange = map(b.x) do x
    min, max = extrema(x)
    @sprintf "(%.1f, %.1f)" min max
  end

  alpha = if isempty(b.alpha)
    "known"
  elseif length(b.alpha) == 1
    @sprintf "%.1f" b.alpha[1]
  else
    min, max = extrema(b.alpha)
    @sprintf "(%.1f, %.1f)" min max
  end

  noise = if isempty(b.noise)
    "known"
  elseif length(b.noise) == 1
    @sprintf "%.1f" b.noise[1]
  else
    min, max = extrema(b.noise)
    @sprintf "(%.1f, %.1f)" min max
  end

  println(io, "BayesGrid inference method")
  println(io, " atype   ", b.atype)
  println(io, " size    ", join(size(b), " × "))
  println(io, " x       ", join(xrange, " × "))
  println(io, " alpha   ", alpha)
  println(io, " noise   ", noise)
  println(io, " weights ", b.prior_weights)
  return print(io, " adaptor ", b.adaptor)
end
