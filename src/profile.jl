
"""
Supertype for excitation profiles.
"""
abstract type Profile{F} end

"""
Supertype for radially symmetric excitation profiles.
"""
abstract type RadialProfile{F} <: Profile{F} end

function (p::RadialProfile{F})(dx::Position{D, F}) where {D, F}
  r = sqrt(sum(abs2, dx))
  return p(r)
end

"""
    fwhm(profile :: Profile)

Return the full-width-at-half-maximum (fwhm) of `profile`.

!!! Note: Even though the notion of fwhm does not make sense for some profiles, like the quadratic profile, `fwhm = width * 2sqrt(log(2))` is still used as a rough proxy for the interaction extent of the profile.
"""
fwhm(p::Profile) = p.width * 2sqrt(log(2))

"""
    adapt(F, profile)

Adapt the floating point type of `profile` to `F`.
"""
function setprecision(gp::Profile, ::Type)
  return error("To be implemented")
end

"""
    setalpha(profile, alpha)

Return a copy of `profile` with brightness value set to `alpha`.
"""
setalpha(::Profile, ::Real) = error("To be implemented")

"""
    scalealpha(profile, scale)

Return a copy of `profile` with brightness value scaled by `scale`.
"""
scalealpha(::Profile, scale::Real) = error("To be implemented")

"""
Gaussian excitation profile.

The intensity function is defined by ``f(r) = α \\exp(-(r/s)^2)``, where ``α`` is
the brightess `alpha` and ``s`` the `width` of the profile.
"""
struct GaussProfile{F} <: RadialProfile{F}
  alpha::F
  width::F
  inv_width::F
end

"""
    GaussProfile([F]; alpha = 1, width = 200, fwhm = nothing)

Returns a `GaussProfile` with brightness `alpha` and width `width`. 
Alternatively, the full width at half maximum `fwhm` can be passed to adapt
the width. The floating point precision `F` defaults to `Float64`.
"""
function GaussProfile(
  F::Type{<:AbstractFloat};
  alpha = 1.0,
  width = 200,
  fwhm = nothing,
)
  if !isnothing(fwhm)
    width = fwhm / (2sqrt(log(2)))
  end
  inv_width = 1 / width
  return GaussProfile(F(alpha), F(width), F(inv_width))
end

GaussProfile(; kwargs...) = GaussProfile(Float64; kwargs...)

(p::GaussProfile{F})(r::F) where {F} = p.alpha * exp(-(p.inv_width * r)^2)

function setprecision(p::GaussProfile, ::Type{F}) where {F}
  return GaussProfile(F; p.alpha, p.width)
end

function setalpha(p::GaussProfile{F}, alpha::Real) where {F}
  return GaussProfile(F; alpha, p.width)
end

function scalealpha(p::GaussProfile{F}, scale::Real) where {F}
  return GaussProfile(F; alpha = F(scale) * p.alpha, p.width)
end

function Base.show(io::IO, p::GaussProfile{F}) where {F}
  fwhm = p.width * 2sqrt(log(2))
  return print(io, "GaussProfile(alpha = $(p.alpha), fwhm = $(pp(fwhm)))")
end

function Base.show(io::IO, ::MIME"text/plain", p::GaussProfile{F}) where {F}
  fwhm = p.width * 2sqrt(log(2))
  return print(io, "GaussProfile{$F}(alpha = $(p.alpha), fwhm = $fwhm)")
end

function pp(p::GaussProfile; alpha = true)
  fwhm = p.width * 2sqrt(log(2))
  if alpha
    @sprintf "G(%.1f, %.1f)" p.alpha fwhm
  else
    @sprintf "G%.1f" fwhm
  end
end

"""
Quadratic excitation profile.

The intensity function is defined by ``f(r) = α (r/s)^2``, where ``α`` is the
brightness `alpha` and ``s`` is the `width` of the profile.
"""
struct QuadraticProfile{F} <: RadialProfile{F}
  alpha::F
  width::F
  inv_width::F
end

"""
    QuadraticProfile([F]; alpha = 1, width = 200)

Returns a `QuadraticProfile` with brightness `alpha` and width `width`. The
floating point precision `F` defaults to `Float64`.
"""
function QuadraticProfile(F::Type{<:AbstractFloat}; alpha = 1, width = 200)
  inv_width = -1 / width
  return QuadraticProfile(F(alpha), F(width), F(inv_width))
end

QuadraticProfile(; kwargs...) = QuadraticProfile(Float64; kwargs...)

(p::QuadraticProfile{F})(r::F) where {F} = p.alpha * (p.inv_width * r)^2

function setprecision(p::QuadraticProfile, ::Type{F}) where {F}
  return QuadraticProfile(F; p.alpha, p.width)
end

function setalpha(p::QuadraticProfile{F}, alpha) where {F}
  return QuadraticProfile(F; alpha, p.width)
end

function scalealpha(p::QuadraticProfile{F}, scale::Real) where {F}
  return QuadraticProfile(F; alpha = F(scale) * p.alpha, p.width)
end

function Base.show(io::IO, p::QuadraticProfile{F}) where {F}
  return print(io, "QuadraticProfile(alpha = $(p.alpha), width = $(p.width))")
end

function Base.show(io::IO, ::MIME"text/plain", p::QuadraticProfile{F}) where {F}
  return print(
    io,
    "QuadraticProfile{$F}(alpha = $(p.alpha), width = $(p.width))",
  )
end

function pp(profile::QuadraticProfile; alpha = true)
  if alpha
    @sprintf "Q(%.1f, %.1f)" profile.alpha profile.width
  else
    @sprintf "Q%.1f" profile.width
  end
end

"""
Donut excitation profile.

The intensity function is defined by ``f(r) = α * (r/s)^2 \\exp(1 - (r/s)^2)``,
where ``s`` is the `width` of the profile.
"""
struct DonutProfile{F} <: RadialProfile{F}
  alpha::F
  width::F
  inv_width::F
end

"""
    DonutProfile([F]; alpha = 1, width = 200, fwhm = nothing)

Returns a `DonutProfile` with brightness `alpha` and width `width`. The
floating point precision `F` defaults to `Float64`.
"""
function DonutProfile(
  F::Type{<:AbstractFloat};
  alpha = 1,
  width = 200,
  fwhm = nothing,
)
  if !isnothing(fwhm)
    width = fwhm / (2sqrt(log(2)))
  end
  inv_width = -1 / width
  return DonutProfile(F(alpha), F(width), F(inv_width))
end

DonutProfile(; kwargs...) = DonutProfile(Float64; kwargs...)

function (p::DonutProfile{F})(r::F) where {F}
  x2 = (p.inv_width * r)^2
  return p.alpha * x2 * exp(one(F) - x2)
end

function setprecision(p::DonutProfile, ::Type{F}) where {F}
  return DonutProfile(F; p.alpha, p.width)
end

function setalpha(p::DonutProfile{F}, alpha) where {F}
  return DonutProfile(F; alpha, p.width)
end

function scalealpha(p::DonutProfile{F}, scale::Real) where {F}
  return DonutProfile(F; alpha = F(scale) * p.alpha, p.width)
end

function Base.show(io::IO, p::DonutProfile{F}) where {F}
  return print(io, "DonutProfile(alpha = $(p.alpha), width = $(p.width))")
end

function Base.show(io::IO, ::MIME"text/plain", p::DonutProfile{F}) where {F}
  return print(io, "DonutProfile{$F}(alpha = $(p.alpha), width = $(p.width))")
end

function pp(profile::DonutProfile; alpha = true)
  if alpha
    @sprintf "D(%.1f, %.1f)" profile.alpha profile.width
  else
    @sprintf "D%.1f" profile.width
  end
end

"""
Directed excitation profile.

This wrapper turns a [`RadialProfile`](@ref) into a profile evaluated with
radius `r = dot(dx, n)`, where `dx` is the offset and `n` a specified normal
vector. In particular, this profile remains constant perpendicular to the normal
vector `n`.

---
    DirectedProfile(profile::RadialProfile, normal)

Turn `profile` into a directed profile with normal direction `normal`, which is
standardized to unit Euclidean length automatically.
"""
struct DirectedProfile{D, F, P <: RadialProfile{F}} <: Profile{F}
  profile::P
  normal::SVector{D, F}

  function DirectedProfile(profile::P, normal) where {F, P <: RadialProfile{F}}
    D = length(normal)
    normal = SVector{D, F}(normal...)
    len = sqrt(sum(abs2, normal))
    @assert len > 0 """
    The specified normal vector must have a positive length.
    """
    return new{D, F, P}(profile, normal ./ len)
  end
end

function (p::DirectedProfile)(dx::Position)
  r = abs(dot(p.normal, dx))
  return p.profile(r)
end

function setprecision(p::DirectedProfile{D}, ::Type{F}) where {D, F}
  return DirectedProfile(setprecision(p.profile, F), p.normal)
end

function setalpha(p::DirectedProfile{F}, alpha) where {F}
  return DirectedProfile(setalpha(p, alpha), p.normal)
end

function scalealpha(p::DirectedProfile{F}, scale::Real) where {F}
  return DirectedProfile(scalealpha(p, scale), p.normal)
end

function Base.show(io::IO, p::DirectedProfile{2, F}) where {F}
  angle = round(acos(p.normal[1]) / pi * 180)
  print(io, "Directed(")
  show(io, p.profile)
  print(io, ", $(angle)°)")
  return
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  p::DirectedProfile{2, F},
) where {F}
  angle = round(acos(p.normal[1]) / pi * 180)
  print(io, "Directed{2, $F}(")
  show(io, p.profile)
  print(io, ", $(angle)°)")
  return
end

function pp(p::DirectedProfile; kwargs...)
  angle = round(Int, acos(p.normal[1]) / pi * 180)
  return "d$angle" * pp(p.profile; kwargs...)
end
