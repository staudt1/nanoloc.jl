
using PrecompileTools: @setup_workload, @compile_workload

function _precompile(policies, methods)
  params = Param((5, 5); alpha = 1, noise = 0.1)
  for p in policies, m in methods
    stack = localize(p, m, params; stop = t -> t.photons >= 5, verbose = false)
  end
end

@setup_workload begin
  _policies_precompile = [
    Minflux(50),
    Minflux(50; timing = true),
    MinfluxBayes(),
    MinfluxBayes(; timing = true),
    MinfluxBED(; gridsize = 3, samples = 10),
    MinfluxBED(; gridsize = 3, timing = true, samples = 10),
    Minsted(),
    Minsted(; timing = true),
    MinstedBayes(),
    MinstedBayes(; timing = true),
    MinstedBED(; gridsize = 3, samples = 10),
    MinstedBED(; gridsize = 3, timing = true, samples = 10),
    SimpleBED(
      BernoulliDesign,
      [GaussProfile(; fwhm = 10)];
      gridsize = 3,
      samples = 10,
    ),
  ]

  _methods_precompile = [
    BayesGrid(0:10, 0:10),
    BayesGrid(0:10, 0:10; atype = :default32),
    BayesGrid(0:10, 0:10; alpha = 1.0:1.1),
    BayesGrid(0:10, 0:10; alpha = 1.0:1.1, atype = :default32),
  ]

  @compile_workload _precompile(_policies_precompile, _methods_precompile)
end
