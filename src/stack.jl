
"""
Collection of localization traces that share the same policy and inference
method.
"""
struct Stack{T <: Trace}
  policy::Policy
  method::InferenceMethod
  traces::Vector{T}

  function Stack(
    policy::Policy,
    method::InferenceMethod,
    traces::Vector{T},
  ) where {T}
    @assert !isempty(traces) "Cannot create a stack without traces"
    @assert all(t -> iscompatible(policy, t.policy), traces) """
    All stack traces must share the same policy
    """
    @assert all(t -> t.method == method, traces) """
    All stack traces must share the same inference method.
    """
    return new{T}(policy, method, traces)
  end
end

"""
    localize(policy, method, params; kwargs...)

Create a localization stack that contains one simulated trace per parameter
in `params`.

## Keyword Arguments

- `seeds`: A vector of seed values with equal length as `params`. \
  May be provided instead of passing a random number generator `rng` \
  (which is used to derive one seed per trace).
- `threads = true`: Whether the localizations are conducted in parallel.
- `catch_exceptions = true`: If `false`, errors in individual localizations \
  abort the simulation.

The remaining keyword arguments are passed to the localization of a single trace
via [`localize`](@ref).
"""
function localize(
  policy::Policy,
  method::InferenceMethod,
  params::Vector{<:Param{D}};
  stop = t -> t.photons >= 100,
  metrics = (;),
  forget_state = true,
  rng::AbstractRNG = Random.default_rng(),
  seeds = [rand(rng, UInt) for _ in 1:length(params)],
  kwargs...,
) where {D}
  n = length(params)
  @assert length(seeds) == n """
  Number of seeds ($(length(seeds))) must coincide with number of traces ($n).
  """

  traces = _localize(n; kwargs...) do index, callback
    return localize(
      policy,
      method,
      params[index];
      stop,
      metrics,
      callback,
      forget_state,
      rng = Random.Xoshiro(seeds[index]),
      verbose = false,
    )
  end
  return Stack(policy, method, traces)
end

"""
    relocalize(stack, method; [metrics, forget_state, threads])

Re-localize the traces in `stack` with the alternative inference method `method`
"""
function relocalize(
  stack::Stack,
  method::InferenceMethod;
  metrics = (;),
  forget_state::Bool = false,
  kwargs...,
)
  n = length(stack.traces)
  traces = _localize(n; kwargs...) do index, callback
    return relocalize(
      stack.traces[index],
      method;
      metrics,
      forget_state,
      callback,
    )
  end
  # TODO: Maybe we should conceptually distinguish between localized and
  # relocalized traces / stacks?
  # The latter are not really reproducible since policy and method do not work
  # together.
  return Stack(stack.policy, method, traces)
end

"""
    _localize(f :: Function, policy, method, n; kwargs...)

Internal function for batch trace localization by applying the function `f` to
each index in `1:n`. Expects that `f` returns compatible traces.

Do not use this function directly. Prefer to use [`localize`](@ref) for stack
localization instead.
"""
function _localize(
  f::Function,
  n::Integer;
  verbose = true,
  threads = false,
  catch_exceptions = true,
)

  # Async logging of the progress
  if verbose
    stepcount = Threads.Atomic{Int}(0)
    tracecount = Threads.Atomic{Int}(0)
    update = Channel{Bool}(10)
    time_start = time()
    time_update = time_start

    printlog =
      () -> begin
        dt = time() - time_start
        log = join([
          "\r",
          @sprintf("%.1f steps / s ", stepcount[] / dt),
          @sprintf("| %d × %.0f steps ", n, stepcount[] / n),
          @sprintf("| %d / %d traces", tracecount[], n),
        ])
        print(log)
      end

    println("Running stack simulation with $n traces")
    task = @async while take!(update)
      now = time()
      if now - time_update > 0.25
        printlog()
        time_update = now
      end
    end
  end

  # React to exceptions to inform the async logging loop
  try

    # Vector to save the traces
    traces = Vector{Trace}(undef, n)

    # If verbose == true, increase the step counter after each step
    callback = _ -> if verbose
      Threads.atomic_add!(stepcount, 1)
      put!(update, true)
      yield()
    end

    @maythread threads for index in 1:n
      try
        traces[index] = f(index, callback)
      catch err
        if err isa InterruptException
          close(update)
        elseif !(err isa InvalidStateException)
          if catch_exceptions
            @warn "Encountered error at index $index: $err"
            @warn """
            Generated Stack will have fewer traces than expected.
            Continuing localization due to catch_exceptions = true.
            """
          else
            @error """
            Encountered error at index $index: $err.
            Aborting localization due to catch_exceptions = false.
            """
            throw(err)
          end
        end
      end

      if verbose
        Threads.atomic_add!(tracecount, 1)
        put!(update, true)
      end
    end

    traces = [traces[index] for index in 1:n if isassigned(traces, index)]
    return traces

  finally
    if verbose && isopen(update)
      put!(update, false)
      wait(task)
      printlog()
      @printf "\nFinished after %.1f seconds\n" time() - time_start
    end
  end
end

"""
    localize!(stack, truths; kwargs...)

Extend `stack` by the traces obtained via `localize(stack.policy,
stack.method, params; kwargs...)`.
"""
function localize!(
  stack::Stack{<:Trace{D}},
  params::Vector{Param{D}};
  kwargs...,
) where {D}
  s = localize(stack.policy, stack.method, params; kwargs...)
  push!(stack.traces, s.traces)
  return stack
end

"""
    recallstate!(stack)

Recall the posterior state for all localization traces in `stack`.

!!! Note: this may take a considerable amount of time and memory, since it
!!! retraces all state computations.
"""
recallstate!(stack::Stack) = foreach(recallstate!, stack.traces)

"""
    save(io, stack)
    save(path, stack; gzip = true)

Save the localization stack `stack` to `path` or `io`. If `gzip = true`, the
stack file is saved with gzip compression and the extension `".gz"` is appended to `path` if not already present.

In oder to reduce memory consumption, the initial and final states of the
saved traces are always forgotten in the saved stack (`stack` itself
is not affected by this). After loading, they can be recovered via
[`NanoLoc.recallstate!`](@ref).

See also [`load`](@ref)
"""
function save(io::IO, stack::Stack)
  traces = map(copy, stack.traces)
  forgetstate!.(traces)
  stack = Stack(stack.policy, stack.method, traces)
  Serialization.serialize(io, stack)
  return nothing
end

function save(path::AbstractString, stack::Stack; gzip = true)
  if gzip
    path = splitext(path)[2] == ".gz" ? path : path * ".gz"
    GZip.open(path, "w") do io
      return save(io, stack)
    end
  else
    open(path; write = true) do io
      return save(io, stack)
    end
  end
end

"""
    load(path, :: Type{Stack})
    load(io, :: Type{Stack})

Load a localization stack from `path` or `io`. If `path` has extension `".gz"`,
or if the file `path` does not exist but `path * ".gz"` does, a gzipped stack
file is assumed.

The traces of the loaded stack miss the initial and final states.
To recover them, use [`NanoLoc.recallstate!`](@ref).

See also [`save`](@ref).
"""
function load(io::IO, ::Type{Stack})
  stack = Serialization.deserialize(io)
  @assert stack isa Stack "Wrong type when loading localization stack"
  return stack
end

function load(path::AbstractString, ::Type{Stack})
  # Check whether we load a gzipped stack file
  if splitext(path)[2] == ".gz"
    gzip = true
  elseif !isfile(path) && isfile(path * ".gz")
    gzip = true
    path = path * ".gz"
  else
    gzip = false
  end

  if gzip
    GZip.open(path) do io
      return load(io, Stack)
    end
  else
    open(path) do io
      return load(io, Stack)
    end
  end
end

Base.length(stack::Stack) = length(stack.traces)

function Base.getindex(stack::Stack, index::Int)
  return stack.traces[index]
end

function Base.getindex(stack::Stack, index::Int, step::Int)
  return stack.traces[index][step]
end

Base.firstindex(stack::Stack) = 1
Base.lastindex(stack::Stack) = length(stack)

Base.eltype(::Stack{T}) where {T} = T

function Base.iterate(stack::Stack, state = nothing)
  return if isnothing(state)
    stack[1], 1
  elseif state > length(stack)
    nothing
  else
    stack[state], state + 1
  end
end

Base.IndexStyle(::Type{<:Stack}) = IndexLinear()
Base.keys(stack::Stack) = LinearIndices(1:length(stack))

"""
    merge(a :: Stack, bs :: Stack...)

Merge localization stacks `a` and `b`. Requires `a` and each of the `bs` to
have the same policies and inference methods.
"""
function Base.merge(a::Stack, bs::Stack...)
  @assert all(iscompatible(a.policy, b.derive_noise) for b in bs)
  @assert all(a.method == b.method for b in bs)
  traces = copy(a.traces)
  for b in bs
    append!(traces, b.traces)
  end
  return Stack(a.policy, a.method, traces)
end

"""
    filter(f, stack)

Return a new stack that only contains the traces in `stack.traces` for which
`f(trace) == true`.
"""
function Base.filter(f, stack::Stack)
  traces = filter(f, stack.traces)
  return Stack(stack.policy, stack.method, traces)
end

function Base.show(io::IO, stack::Stack{D}) where {D}
  policy = policyname(stack)
  method = stack.method
  return print(io, "Stack($policy, $method, $(length(stack)))")
end

function Base.show(io::IO, ::MIME"text/plain", stack::Stack{T}) where {T}
  println(io, "Stack{", nameof(T), "} with ", length(stack), " traces")

  meanstdi = vals -> @sprintf "%.0f ± %.0f" mean(vals) std(vals)
  meanstd = vals -> @sprintf "%.1f ± %.1f" mean(vals) std(vals)

  traces = stack.traces
  steps = map(t -> t.steps, traces)
  photons = map(t -> t.photons, traces)
  bgphotons = map(t -> t.bgphotons, traces)
  errs = map(t -> t[end].errx, traces)
  stds = map(t -> t[end].stdx, traces)
  upgrades = map(t -> sum(u -> u.upgrade, t.updates), traces)
  healthy = map(t -> t.health.healthy, traces)

  println(io, " policy   ", policyname(stack))
  println(io, " method   ", stack.method)
  println(io, " upgrades ", meanstd(upgrades))
  println(io, " steps    ", meanstd(steps))
  print(io, " photons  ", meanstd(photons))
  println(io, " / ", meanstd(bgphotons), " bg")
  print(io, " err/std  ", meanstd(errs))
  println(io, " / ", meanstd(stds))
  print(io, " healthy  ", floor(Int, 100 * mean(healthy)), "%")
  if any(.!healthy)
    print(io, " (", count(.!healthy), " unhealthy)")
  end
end

"""
    firstmetric(f, stack, [indices])

Compute `firstmetric(f, trace)` for all traces in `stack.traces[indices]`.
"""
function firstmetric(cond, stack::Stack, indices = 1:length(stack.traces))
  return map(t -> firstmetric(cond, t), @view(stack.traces[indices]))
end

"""
    lastmetric(f, stack, [indices])

Compute `lastmetric(f, trace)` for all traces in `stack.traces[indices]`.
"""
function lastmetric(cond, stack::Stack, indices = 1:length(stack.traces))
  return map(t -> lastmetric(cond, t), @view(stack.traces[indices]))
end

"""
    photonmap(f, stack, [n]; kwargs...)

Compute `photonmap(f, trace, [n]; kwargs...)` for each trace in `stack`.
"""
function photonmap(f, stack::Stack, args...; kwargs...)
  return map(trace -> photonmap(f, trace, args...; kwargs...), stack.traces)
end

"""
    stepsneeded(f, stack)

Calculate `stepsneeded(f, trace)` for each `trace` in `stack.traces` and
return the resulting vector.
"""
function stepsneeded(f, stack::Stack)
  return map(trace -> stepsneeded(f, trace), stack.traces)
end

"""
    photonsneeded(f, stack; kwargs...)

Calculate `photonsneeded(f, trace; kwargs...)` for each `trace` in `stack.traces` and
return the resulting vector.
"""
function photonsneeded(f, stack::Stack; kwargs...)
  return map(trace -> photonsneeded(f, trace; kwargs...), stack.traces)
end

"""
    stackmap(f, stack :: Stack)

Return a new stack with its traces replaced by `f.(stack.traces)`.
The function `f` must map traces to traces.
"""
function stackmap(f, stack::Stack)
  traces = map(f, stack.traces)
  return Stack(stack.policy, stack.method, traces)
end

"""
    trim(stack, args...; kwargs...)

Reduce the length of the traces in stack by setting a maximal number of steps
or photons.
"""
function trim(stack::Stack, args...; kwargs...)
  return stackmap(t -> trim(t, args...; kwargs...), stack)
end

"""
    policy(stack)

Return the policy of `stack`. Is `nothing` if the policy is unknown.
"""
policy(stack::Stack) = stack.policy

"""
    policyname(stack)

Return the name of the policy that stack was generated with. Returns
the string `"<unknown>"` if the policy is unknown.

See also [`policyoptions`](@ref).
"""
policyname(stack::Stack) = policyname(policy(stack))

"""
    policyoptions(stack)

Return the options of the policy that stack was created with. Returns an
empty vector if the policy is unknown.

See also [`policyname`](@ref).
"""
policyoptions(stack::Stack) = policyoptions(policy(stack))
