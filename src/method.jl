
"""
Abstract type that represents the current state of inference.

Inference states are used by [`InferenceMethod`](@ref)s to keep track and
incrementally update the available parameter information. For example, in case
of Bayesian inference, parameter distributions (see [`ParamDist`](@ref)) are
used as inference states.
"""
abstract type InferenceState{D} end

"""
Abstract type that represents an inference algorithm or method.

Inference methods determine how to track the current state of inference (see
[`InferenceState`](@ref)) and how to update this state on the occasion of new
observations.
"""
abstract type InferenceMethod{S <: InferenceState} end

"""
    init(method :: InferenceMethod)
    init(method :: InferenceMethod, param :: Param)

Return the initial [`InferenceState`](@ref) of the inference method `method`.

In simulations with known true parameter, this parameter `param` will be passed
as well.
"""
function init(::InferenceMethod, param = nothing)
  return error("Not implemented")
end

"""
    update(method :: InferenceMethod, state :: InferenceState, ms)

Return the updated inference state that incorporates a new measurement into
`state` as determined by `method`. The argument `ms` contains the history of
all measurements during the localization process. Therefore, the final entry
`ms[end]` refers to the most recent measurement that has not been available in
previous calls to [`update`](@ref).

A named tuple with meta information regarding the update process, with at least
the boolean field `upgrade`, is returned as well.

!!! note

    This function should be free of side effects. In particular, it should not
    modify its arguments.
"""
function update(
  ::InferenceMethod{S},
  state::S,
  ms, # history of previous measurements
) where {S <: InferenceState}
  return error("Not implemented")
end

"""
    methodname(method :: InferenceMethod)

Get a descriptive name of the inference method `method`.
"""
methodname(method::InferenceMethod) = string(nameof(typeof(method)))

"""
    methodoptions(method :: InferenceMethod)

Returns the options that parameterize the inference method `method`.
"""
methodoptions(::InferenceMethod) = []

"""
    diagnose(method :: InferenceMethod, state)

Diagnose `state` under the inference method `method` and return health
information.

Besides returning a named tuple with at least the boolean field `healthy`,
this method should print warnings if pathological tendencies in `state`
are discerned, like all mass focusing on few support points.
"""
function diagnose(::InferenceMethod{S}, state::S) where {S}
  return (; healthy = true)
end

"""
    estimate(state; kwargs...)

Obtain a point estimate for the model parameter.

See also [`estimatex`](@ref), [`estimatealpha`](@ref), [`estimatenoise`](@ref),
[`estimatestd`](@ref), [`estimatevar`](@ref), and [`estimatecov`](@ref).
"""
function estimate(state; kwargs...)
  return error("Not implemented")
end

function estimate(F::Type{<:AbstractFloat}, state; kwargs...)
  return setprecision(estimate(state; kwargs...), F)
end

"""
    estimatecov(state; kwargs...)

Obtain a point estimate for the covariance matrix of `estimate(state;
kwargs...)`.
"""
function estimatecov(state; kwargs...)
  return error("Not implemented")
end

function estimatecov(F::Type{<:AbstractFloat}, state; kwargs...)
  return convert(Array{F}, estimatecov(state; kwargs...))
end

"""
    estimatex(state; kwargs...)

Obtain a point estimate for the position parameter x.
"""
function estimatex(state; kwargs...)
  return estimate(state; kwargs...).x
end

function estimatex(F::Type{<:AbstractFloat}, state; kwargs...)
  return setprecision(estimatex(state; kwargs...), F)
end

"""
    estimatealpha(state; kwargs...)

Obtain a point estimate for the brightness parameter alpha.
"""
function estimatealpha(state; kwargs...)
  return estimate(state; kwargs...).alpha
end

function estimatealpha(F::Type{<:AbstractFloat}, state; kwargs...)
  return F(estimatealpha(state; kwargs...))
end

"""
    estimatealpha(state; kwargs...)

Obtain a point estimate for the noise parameter.
"""
function estimatenoise(state; kwargs...)
  return estimate(state; kwargs...).noise
end

function estimatenoise(F::Type{<:AbstractFloat}, state; kwargs...)
  return F(estimatenoise(state; kwargs...))
end

"""
    estimatevar(state; kwargs...)

Obtain a point estimate for the variance of `estimate(state;
kwargs...)`.
"""
function estimatevar(state::InferenceState{D}; kwargs...) where {D}
  c = estimatecov(state; kwargs...)
  c = convert(Array, c)
  x = diag(c[1:D, 1:D])
  return Param(x; alpha = c[D + 1, D + 1], noise = c[D + 2, D + 2])
end

function estimatevar(F::Type{<:AbstractFloat}, state; kwargs...)
  return setprecision(estimatevar(state, kwargs...), F)
end

"""
    estimatestd(state; kwargs...)

Obtain a point estimate for the standard deviation of `estimate(state;
kwargs...)`.
"""
function estimatestd(state::InferenceState{D}; kwargs...) where {D}
  v = estimatevar(state; kwargs...)
  return Param(sqrt.(v.x); alpha = sqrt(v.alpha), noise = sqrt(v.noise))
end

function estimatestd(F::Type{<:AbstractFloat}, state; kwargs...)
  return setprecision(estimatestd(state, kwargs...), F)
end

"""
    estimateextent(state; kwargs...)

Obtain a point estimate for the spatial extent of the uncertainty of
`estimate(state; kwargs...)`.

Defaults to two times the square root of the largest eigenvalue of
[`estimatecov`](@ref).
"""
function estimateextent(state::InferenceState{D}; kwargs...) where {D}
  c = estimatecov(state; kwargs...)
  covmat = convert(Array, c)[1:D, 1:D]
  return 2sqrt(eigmax(Symmetric(covmat)))
end

function estimateextent(F::Type{<:AbstractFloat}, state; kwargs...)
  return F(estimateextent(state; kwargs...))
end
