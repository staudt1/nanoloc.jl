
"""
Supertype for discrete parameter distributions.

Each concrete subtype must at least implement the following methods:
* [`NanoLoc.params`](@ref), which returns the support points of the \
  distribution.
* [`NanoLoc.weights`](@ref), which returns the weights of the \
  distribution at the support points with respect to a reference measure.
* [`NanoLoc.refweights`](@ref), which returns the weights of the \
  default reference measure with respect to the counting measure. Returns \
  `nothing` if no default reference measure exists (see also [`hasref`](@ref)).\
  In this case, the counting measure is assumed as implicit default \
  reference.

In most situations, reference measures can be left unspecified (i.e., weights
are understood relative to the counting measure). They are mainly intended
for situations where a Lebesgue density is approximated on a non-uniform
structure. For example, in case of a [`GenericParamGrid`](@ref), the reference
measure can capture the relation between the Lebesgue probability density values
at the grid points and the actual probability within the given grid cell by
accounting for the Lebesgue cell volume.

Weights are assumed to be normalized with respect to their default reference
measure. That is, for any discrete distribution `dist` with `hasref(dist) ==
true`,

    sum(weights(dist) .* refweights(dist)) ≊ 1

If `hasref(dist) == false`, we assume `sum(weights(dist)) ≊ 1`.
"""
abstract type DiscreteParamDist{D, F} <: ParamDist{D, F} end

# InferenceState interface. The methods mean and std are defined in dist/
# stats.jl
estimate(d::DiscreteParamDist) = mean(d)
estimatestd(d::DiscreteParamDist) = std(d)
estimatecov(d::DiscreteParamDist) = cov(d)

"""
    params(dist :: DiscreteParamDist)

Return a vector of [`Param`](@ref) values which defines the support points of
the distribution `dist`.
"""
params(::DiscreteParamDist) = error("Not implemented")

"""
    weights(dist :: DiscreteParamDist [; ref])

Return the probability weights of `dist` with respect to the reference measure
`ref`.

If an explicit reference measure `ref` is passed, it will hold that

    sum(weights(dist; ref) .* ref) ≊ 1

If no explicit reference measure is passed, the default reference measure of
`grid` is used. The special values `ref = 1` or `ref = :counting` yield weights
relative to the counting measure.

!! Note: `weights(dist)` must return a reference to the weight storage of `dist`.

See also [`weightsc`](@ref), which is an alias for
`weights(dist; ref = :counting)`.
"""
weights(::DiscreteParamDist; ref = nothing) = error("Not implemented")

"""
    logweights(dist :: DiscreteParamDist [; ref])

Return the log-weights of `dist` with respect to the reference measure.

See also [`logweightsc`](@ref), which is an alias for
`logweights(dist, ref = :counting)`.
"""
logweights(dist::DiscreteParamDist; kwargs...) = log.(weights(dist; kwargs...))

"""
    setweights(dist :: DiscreteParamDist, weights [; ref])

Return a new parameter distribution with the same support points and reference
measure as `dist` but updated weights `weights`.

Note that the argument `ref` only affects the reference measure with respect
to which `weights` is understood. The reference measure of `dist` is explicitly
not affected by specification of `ref`.

See also [`setweightsc`](@ref), which is an alias for
`weights(dist, weights; ref = :counting)`.
"""
function setweights(::DiscreteParamDist, ::Any; ref = nothing)
  return error("Not implemented")
end

"""
      setweights!(dist :: DiscreteParamDist, weights)
      setweights!(dist :: DiscreteParamDist, dist_b :: DiscreteParamDist)

Overwrite the weights of `dist` by the vector `weights` or `weights(dist_b)`.
"""
function setweights!(dist::ParamDist{D, F}, ws::AbstractArray{F}) where {D, F}
  array = weights(dist)
  array .= ws
  return
end

function setweights!(
  dista::ParamDist{D, F},
  distb::ParamDist{D, F},
) where {D, F}
  return setweights!(dista, weights(distb))
end

"""
    setrefweights(dist, ref)

Return a version of the distribution `dist` where the reference measures is
defined by `ref`.

In particular, the weights with respect to the counting measure (see
[`weightsc`](@ref)) between `dist` and the returned distribution will match.
"""
function setrefweights(::DiscreteParamDist, refweights)
  return error("Not implemented")
end

"""
    weightsc(dist :: DiscreteParamDist)

Returns a vector with the probability weights of `dist` with respect to the
counting measure.

Equivalent to [`weights`](@ref) with the argument `ref = :counting`.
"""
weightsc(dist::DiscreteParamDist) = weights(dist; ref = :counting)

"""
    logweightsc(dist :: DiscreteParamDist)

Returns the log-weights of `dist` with respect to the counting measure.

Equivalent to [`logweights`](@ref) with the argument `ref = :counting`.
"""
logweightsc(dist::DiscreteParamDist) = log.(weightsc(dist))

"""
    setweightsc(dist :: DiscreteParamDist, weights)

Return a new parameter distribution with the same support points and reference
measure as `dist` but updated weights `weights` with respect to the
counting measure.

Equivalent to [`setweights`](@ref) with the argument `ref = :counting`.
"""
function setweightsc(dist::DiscreteParamDist, weights::AbstractVector)
  return setweights(dist, weights ./ refweights(weights))
end

"""
    refweights(dist :: DiscreteParamDist)

Returns a vector with the probability weights of the reference distribution of
`dist` at the parameters `params(dist)`.

If `hasrefereence(dist) == false`, then `refweights(dist) == nothing`.
"""
refweights(::DiscreteParamDist) = error("Not implemented")

"""
    hasreference(dist :: DiscreteParamDist)

Returns whether dist possesses an explicit reference distribution or not.
"""
function hasref(dist::DiscreteParamDist)
  ref = refweights(dist)
  if isnothing(ref) || isempty(ref)
    return false
  else
    return true
  end
end

"""
    setatype(dist :: DiscreteParamDist, T)

Return a new discrete parameter distribution with underlying base array type set
to `T`.
"""
function setatype(dist::DiscreteParamDist, ::Type)
  return error("Not implemented")
end

function setatype(dist::DiscreteParamDist, sym::Symbol)
  return setatype(_atype(sym), dist)
end

Base.convert(::Type{ParamDist{D, F}}, p::ParamDist{D, F}) where {D, F} = p

function Base.convert(::Type{ParamDist{D, F}}, p::ParamDist{D}) where {D, F}
  return setprecision(p, F)
end

Base.length(d::ParamDist) = length(params(d))

function Base.rand(rng::AbstractRNG, dist::DiscreteParamDist, n::Integer)
  indices = sample_indices(rng, weightsc(dist), n)
  return params(dist)[indices]
end

"""
    findmax(dist :: ParamDist [; ref])

Returns the largest weight value (relative to `ref`) and the corresponding
parameter in `dist`.
"""
function Base.findmax(dist::DiscreteParamDist; kwargs...)
  weight, index = findmax(weights(dist; kwargs...))
  return weight, _takenth(params(dist), index)
end

function Base.copy(::DiscreteParamDist)
  return error("Not implemented")
end

"""
    sbratio(design :: Design, dist :: DiscreteParamDist)

Calculate the average signal-to-background ratio for a given design `design` and
parameter distribution `dist`.
  
"""
function sbratio(
  design::Design{D, F},
  dist::DiscreteParamDist{D, F},
) where {D, F}
  sbrs = sbratio.(design, params(dist))
  ws = weightsc(dist)
  return dot(sbrs, ws) / sum(ws)
end

"""
    _prepareweights(V, params :: Vector{<: Param}, weights)
    _prepareweights(V, params :: Vector{<: Param}, profile)
    _prepareweights(V, params :: Vector{<: Param}, f)

Derive weight values for the parameter `params` in type `V`. The weight values
are either taken from an array `weights`, a profile `profile`, or an function
`f` that is applied to each parameter in `params`.
"""
function _prepareweights(
  V,
  params::AbstractVector{<:Param},
  weights::AbstractArray,
)
  @assert length(weights) == length(params) """
  Weight vector has incorrect length $(length(weights)).
  """
  return convert(V, reshape(weights, :))
end

function _prepareweights(V, params::AbstractVector{<:Param}, f::Function)
  return convert(V, f.(params))
end

function _prepareweights(V, params::AbstractVector{<:Param}, sym::Symbol)
  @assert sym in [:const, :constant] """
  Weight symbol :$sym is invalid.
  """
  return _prepareweights(V, params, p -> 1)
end

function _prepareweights(V, params::AbstractVector{<:Param}, profile)
  @assert profile isa Profile """
  Expected a weight array, a function, or a profile when preparing parameter
  distribution weights.
  """
  center = mean(map(p -> p.x, params))
  weights = map(params) do param
    r = sqrt(sum(abs2, center .- param.x))
    return profile(r)
  end
  return convert(V, weights)
end

function _prepareweights(dist::DiscreteParamDist, arg)
  V = typeof(weights(dist))
  return _prepareweights(V, params(dist), arg)
end

"""
    _usecounting(ref)

Determine whether the argument `ref` implies the counting measure.
"""
_usecounting(ref) = ref == :counting || ref == 1 || isempty(ref)

"""
    _preparerefweights(params :: Vector{<: Param}, ref)

Derive reference weight values for the argument `ref`.
"""
function _preparerefweights(V, params::AbstractVector{<:Param}, ref)
  if _usecounting(ref)
    return V()
  else
    return _prepareweights(V, params, ref)
  end
end

"""
    _striparrayparams(T)

Return the supertype of `T` that results from stripping all type parameters.
For example, `_striparrayparams(Array{Float32, 1})` returns `Array`
"""
_striparrayparams(::Type{<:Array}) = Array

"""
Weighted parameter list.

Default implementation of a discrete parameter distribution. No underlying
geometric structure is assumed for the parameters.
"""
struct GenericParamList{
  D,
  F,
  P <: AbstractVector{Param{D, F}},
  V <: AbstractVector{F},
} <: DiscreteParamDist{D, F}
  params::P
  weights::V
  ref::V
end

params(dist::GenericParamList) = dist.params

function weights(dist::GenericParamList; ref = dist.ref)
  if ref === dist.ref || _usecounting(ref) && !hasref(dist)
    # default reference measure is used
    return dist.weights
  elseif _usecounting(ref)
    # counting measure is used when it is not the default reference
    return dist.weights .* dist.ref
  else
    # custom reference measure is used
    ref = convert(typeof(dist.weights), ref)
    @assert length(ref) == length(dist) """
    Reference weight vector has incorrect length $(length(ref)).
    """
    dref = hasref(dist) ? dist.ref : 1
    return dist.weights .* dref ./ ref
  end
end

refweights(dist::GenericParamList) = isempty(dist.ref) ? nothing : dist.ref

function setweights(
  dist::GenericParamList{D, F, P, V},
  weights;
  ref = nothing,
) where {D, F, P, V}
  weights = _prepareweights(V, dist.params, weights)
  ref = isnothing(ref) ? dist.ref : _preparerefweights(V, dist.params, ref)

  if ref === dist.ref || _usecounting(ref) && !hasref(dist)
    # default reference measure is used
    if _usecounting(ref)
      scale = one(F) ./ sum(weights)
    else
      scale = one(F) ./ sum(weights .* dist.ref)
    end
    weights .= scale .* weights
  elseif _usecounting(ref)
    # counting measure is used when it is not the default reference
    scale = one(F) ./ sum(weights)
    weights .= scale .* weights ./ dist.ref
  else
    # custom reference measure is used
    weights .= weights .* ref
    scale = one(F) ./ sum(weights)
    dref = hasref(dist) ? dist.ref : 1
    weights .= scale .* weights ./ dref
  end

  return GenericParamList(dist.params, weights, dist.ref)
end

function setrefweights(
  dist::GenericParamList{D, F, P, V},
  ref,
) where {D, F, P, V}
  ref = _preparerefweights(V, dist.params, ref)
  ws = weights(dist; ref)
  return GenericParamList(dist.params, ws, ref)
end

function setprecision(
  dist::GenericParamList{D, F},
  ::Type{F},
) where {D, F <: AbstractFloat}
  return dist
end

function setprecision(
  dist::GenericParamList{D, <:Any, P, V},
  F::Type{<:AbstractFloat},
) where {D, P, V}
  T = _striparrayparams(P)
  S = _striparrayparams(V)
  params = convert(T{Param{D, F}, 1}, dist.params)
  weights = convert(S{F, 1}, dist.weights)
  ref = convert(S{F, 1}, dist.ref)
  return GenericParamList(params, weights, ref)
end

function setatype(
  dist::GenericParamList{D},
  ::Type{T},
) where {F <: AbstractFloat, T <: AbstractArray{F}, D}
  S = _striparrayparams(T)
  params = convert(S{Param{D, F}, 1}, dist.params)
  weights = convert(S{F, 1}, dist.weights)
  ref = convert(S{F, 1}, dist.ref)
  return GenericParamList(params, weights, ref)
end

function setatype(dist::GenericParamList{D, F}, ::Type{T}) where {T, D, F}
  return setatype(dist, T{F})
end

function Base.copy(dist::GenericParamList)
  return GenericParamList(copy(dist.weights), copy(dist.params), copy(dist.ref))
end

Base.size(dist::GenericParamList) = (length(dist.weights),)

"""
Weighted parameter list on the basis of the in-built `Array` type.
"""
const ParamList{D, F} = GenericParamList{D, F, Vector{Param{D, F}}, Vector{F}}

"""
    ParamList(params :: Vector{Param{D, F}}, weights[, ref; atype])

Create a [`GenericParamList`](@ref) based on the arguments `params`, `weights`,
and (optionally) `ref`. Alternatively, the array type that the param list should
use internally can be specified via `atype`.
"""
function ParamList(
  params::AbstractVector{Param{D, F}};
  weights = :constant,
  ref = :counting,
  atype = nothing,
) where {D, F}
  atype = isnothing(atype) ? nothing : _atype(atype)

  if !isnothing(atype) && eltype(atype) != Any
    R = eltype(atype)
    setprecision(params, R)
  else
    R = F
  end

  if !isnothing(atype)
    T = _striparrayparams(atype)
    params = convert(T, params)
  else
    T = _striparrayparams(typeof(params))
  end

  weights = _prepareweights(T{R, 1}, params, weights)
  ref = _preparerefweights(T{R, 1}, params, ref)

  if isempty(ref)
    scale = one(F) / sum(weights)
    weights .= scale .* weights
  else
    scale = one(F) / sum(weights .* ref)
    weights .= scale .* weights
  end

  return GenericParamList(params, weights, ref)
end

function ParamList(dist::DiscreteParamDist; kwargs...)
  return ParamList(params(dist); dist.weights, dist.ref, kwargs...)
end

function Base.show(io::IO, ::MIME"text/plain", p::ParamList{D, F}) where {D, F}
  l = length(p)
  return print(io, "ParamList{$D, $F} with $l support point(s)")
end

"""
Weighted parameter grid.

The parameters realize a grid-structure over the positions, brightness values,
and noise levels.
"""
struct GenericParamGrid{
  D,
  F,
  P <: AbstractVector{Param{D, F}},
  V <: AbstractVector{F},
} <: DiscreteParamDist{D, F}
  # Grid
  x::NTuple{D, V}
  alpha::V
  noise::V
  # Data
  params::P
  weights::V
  ref::V
end

params(dist::GenericParamGrid) = dist.params

function weights(dist::GenericParamGrid; kwargs...)
  return weights(ParamList(dist); kwargs...)
end

refweights(dist::GenericParamGrid) = isempty(dist.ref) ? nothing : dist.ref

function setprecision(
  dist::GenericParamGrid{D, F},
  ::Type{F},
) where {D, F <: AbstractFloat}
  return dist
end

function setprecision(
  dist::GenericParamGrid{D, <:Any, P, V},
  F::Type{<:AbstractFloat},
) where {D, P, V}
  T = _striparrayparams(P)
  S = _striparrayparams(V)
  x = convert.(S{F, 1}, dist.x)
  alpha = convert(S{F, 1}, dist.alpha)
  noise = convert(S{F, 1}, dist.noise)
  params = convert(T{Param{D, F}, 1}, dist.params)
  weights = convert(S{F, 1}, dist.weights)
  ref = convert(S{F, 1}, dist.ref)
  return GenericParamGrid(x, alpha, noise, params, weights, ref)
end

function setatype(
  dist::GenericParamGrid{D},
  ::Type{T},
) where {F <: AbstractFloat, T <: AbstractArray{F}, D}
  S = _striparrayparams(T)
  x = convert.(S{F, 1}, dist.x)
  alpha = convert(S{F, 1}, dist.alpha)
  noise = convert(S{F, 1}, dist.noise)

  params = convert(S{Param{D, F}, 1}, dist.params)
  weights = convert(S{F, 1}, dist.weights)
  ref = convert(S{F, 1}, dist.ref)

  return GenericParamGrid(x, alpha, noise, params, weights, ref)
end

function setatype(dist::GenericParamGrid{D, F}, ::Type{T}) where {T, D, F}
  return setatype(dist, T{F})
end

function setweights(dist::GenericParamGrid, weights; kwargs...)
  list = setweights(ParamList(dist), weights; kwargs...)
  return GenericParamGrid(
    dist.x,
    dist.alpha,
    dist.noise,
    list.params,
    list.weights,
    list.ref,
  )
end

function setrefweights(dist::GenericParamGrid, ref)
  list = setrefweights(ParamList(dist), ref)
  return GenericParamGrid(
    dist.x,
    dist.alpha,
    dist.noise,
    list.params,
    list.weights,
    list.ref,
  )
end

"""
    setnoise(grid :: GenericParamGrid, noise)

Returns a new parameter grid with noise support changed to `noise`.
"""
function setnoise(p::GenericParamGrid{D, F, P, V}, noise) where {D, F, P, V}
  @assert length(p.noise) == length(noise) "Inconsistent support for noise"
  noise = isa(noise, Real) ? F[noise] : convert(V, noise)
  return GenericParamGrid(F, p.x...; p.alpha, noise, p.weights, p.ref)
end

"""
    setalpha(grid :: ParamGrid, alpha)

Returns a new parameter grid with brightness support changed to `alpha`.
"""
function setalpha(p::GenericParamGrid{D, F, P, V}, alpha) where {D, F, P, V}
  @assert length(p.alpha) == length(alpha) "Inconsistent support for alpha"
  alpha = isa(alpha, Real) ? F[alpha] : convert(V, alpha)
  return GenericParamGrid(F, p.x...; alpha, p.noise, p.weights, p.ref)
end

"""
    scalealpha(grid :: ParamGrid, scale)

Returns a new parameter grid with brightness support scaled by `scale`.
"""
function scalealpha(p::GenericParamGrid{D, F}, scale::Real) where {D, F}
  @assert scale > 0 "Brightness can only be scaled by positive number"
  alpha = F(scale) .* p.alpha
  return ParamGrid(F, p.x...; alpha, p.noise, p.weights, p.ref)
end

function Base.size(grid::GenericParamGrid)
  return (length.(grid.x)..., length(grid.alpha), length(grid.noise))
end

Base.size(grid::GenericParamGrid, sel) = size(grid)[sel]

function Base.copy(dist::GenericParamGrid)
  return GenericParamGrid(
    copy.(dist.x),
    copy(dist.alpha),
    copy(dist.noise),
    copy(dist.params),
    copy(dist.weights),
    copy(dist.ref),
  )
end

"""
Type alias for parameter grids based on the base julia `Vector` type.
"""
const ParamGrid{D, F} = GenericParamGrid{D, F, Vector{Param{D, F}}, Vector{F}}

"""
    ParamGrid(xs...; alpha = [1], noise = [0], weights = p -> 1 [, ref, atype])

Construct a `ParamGrid` with discretized spatial positions `xs`, brightness
values `alpha`, and background levels `noise`. The argument `weights` can
either be a function that maps params to weight values or an array of weight
values whose length matches the number of grid points. The floating point
precision `F` defaults to `Float64`.

Explicit weight values for the reference measure can be provided via the
argument `ref`. If no reference weights are provided, the counting measure is
implicitly used.

The julia array type underlying the grid can be specified via the argument `atype`.

## Example

Construct a grid with `x1 = 0, ..., 100`, `x2 = 25, 25.5, ..., 75`,
`alpha = 1, ..., 20` and `noise = 0, 0.5, 1`:
```
ParamGrid(0:100, 25:0.5:75, alpha = 1:20, noise = [0, 0.5, 1])
```
"""
function ParamGrid(
  xs::Vararg{Any, D};
  alpha = 1,
  noise = 0,
  weights = :constant,
  ref = :counting,
  atype = :default,
) where {D}
  atype = _atype(atype) # resolve atype
  F = eltype(atype) == Any ? Float64 : eltype(atype)
  T = _striparrayparams(atype)
  # Adapt floating point type
  xs = map(xi -> convert(Array{F}, xi), xs)
  alpha = isa(alpha, Real) ? F[alpha] : convert(Array{F}, alpha)
  noise = isa(noise, Real) ? F[noise] : convert(Array{F}, noise)

  # Collect all parameters that constitute the grid.
  # The following way to do so was much more efficient than a dedicated loop
  product = Iterators.product(xs..., alpha, noise)
  data = reshape(collect(product), :)
  params = reinterpret(Param{D, F}, data)
  params = convert(T, params)
  list = ParamList(params; weights, ref)

  return GenericParamGrid(
    Tuple(convert.(T, xs)),
    convert(T, alpha),
    convert(T, noise),
    list.params,
    list.weights,
    list.ref,
  )
end

function Base.show(io::IO, grid::ParamGrid)
  dims = join(size(grid), "×")
  return print(io, "ParamGrid($dims)")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  grid::ParamGrid{D, F},
) where {D, F}
  dims = join(size(grid), "×")
  return print(io, "ParamGrid{$D, $F}($dims)")
end

"""
    marginalize(grid :: GenericParamGrid{D}; dims, ref = nothing)
    marginalize(grid :: GenericParamGrid{D}, sym; ref = nothing)

Marginalize `grid` by summing over all dimensions `dims`.The first `D`
dimensions are the spatial ones while the final two dimensions correspond to
brightness and noise.

Alternatively, a marginalization target can be specified via a symbol `sym` that
is one of `:x, :x1, :x2, :alpha, :noise`. If `sym = :x`, for example, then `dims
= (D+1, D+2)` corresponds to marginalization over noise and brightness.

If `grid` does not possess a reference measure (see [`hasref`](@ref)),
weights with respect to the counting measure are returned by default. Otherwise,
weights with respect to the corresponding marginal reference measure are
returned.

The marginal reference weights with respect to which the result is returned can
also explicitly be set via the argument `ref`.

If marginal weights with respect to the counting measure are desired even if
`grid` has a reference measure, use [`marginalizec`](@ref).
"""
function marginalize(grid::GenericParamGrid; dims, ref = nothing)
  dims = Tuple(dims)
  # Obtain weights with respect to counting measure
  if hasref(grid)
    weights = reshape(grid.weights .* grid.ref, size(grid))
  else
    weights = reshape(grid.weights, size(grid))
  end

  # Calculate marginals
  if !isnothing(ref)
    return dropdims(sum(weights; dims); dims) ./ ref
  elseif hasref(grid)
    ref = marginalizeref(grid; dims)
    return dropdims(sum(weights; dims); dims) ./ ref
  else
    return dropdims(sum(weights; dims); dims)
  end
end

function _marginalizedims(sym::Symbol, D)
  return if sym in [:x, :X, :pos, :position]
    (D + 1, D + 2) # alpha and noise
  elseif sym in [:x1, :X1, :pos1, :position1]
    ((2:D)..., D + 1, D + 2)
  elseif sym in [:x2, :X2, :pos2, :position2]
    @assert D >= 2 "cannot marginalize on second spatial coordinate when D < 2"
    (1, (3:D)..., D + 1, D + 2)
  elseif sym in [:a, :A, :alpha]
    ((1:D)..., D + 2) # position and noise
  elseif sym in [:n, :N, :noise]
    ((1:D)..., D + 1) # position and alpha
  else
    error("Unsupported marginalization symbol :$sym")
  end
end

function marginalize(
  grid::GenericParamGrid{D},
  sym::Symbol;
  ref = nothing,
) where {D}
  dims = _marginalizedims(sym, D)
  return marginalize(grid; dims, ref)
end

"""
    marginalizec(grid :: GenericParamGrid{D}; dims)
    marginalizec(grid :: GenericParamGrid{D}, sym)

Like [`marginalize`](@ref) but always returns the marginalized weights with
respect to the counting measure.
"""
function marginalizec(grid::GenericParamGrid; dims)
  dims = Tuple(dims)
  if isempty(refweights(grid))
    weights = reshape(grid.weights, size(grid))
  else
    weights = reshape(grid.weights .* grid.ref, size(grid))
  end
  marginal = sum(weights; dims)
  return dropdims(marginal; dims)
end

function marginalizec(grid::GenericParamGrid{D}, sym) where {D}
  dims = _marginalizedims(sym, D)
  return marginalizec(grid; dims)
end

"""
    marginalizeref(grid :: GenericParamGrid; dims)
    marginalizeref(grid :: GenericParamGrid, sym)

Same as [`marginalize`](@ref), but returns the marginalization of the reference
weights `refweights(grid)`.

If `grid` has no reference measure (see [`hasref`](@ref)), the counting measure is returned.
"""
function marginalizeref(grid::GenericParamGrid; dims)
  @assert hasref(grid)
  marginal = sum(grid.ref; dims = Tuple(dims))
  return dropdims(marginal; dims)
end

function marginalizeref(grid::GenericParamGrid{D}, sym::Symbol) where {D}
  dims = _marginalizedims(sym, D)
  return marginalizeref(grid; dims)
end
