
"""
Abstract criterion type.

Criteria are meant to quantify the 'utility' of a measurement, typically by
evaluating some aspect of posterior concentration (see
[`PosteriorCriterion`](@ref)). The default utility criterion is the total
entropy of the posterior ([`TotalEntropy`](@ref)).

General criteria are very flexible and can also incorporate aspects of the
observation, like the number of measured photons, to assess the utility of
a measurement.
"""
abstract type Criterion end

"""
Generic criterion wrapper around criterion functions.

    GenericCriterion(fun)

Wrap the function `fun`, which takes the arguments `(design, prior, post, obs)`.
"""
struct GenericCriterion <: Criterion
  criterion::Any
end

function (c::GenericCriterion)(
  prior::ParamDist{D, F},
  post::ParamDist{D, F},
  design::Design{D, F, O},
  obs::O,
) where {D, F, O}
  return c.criterion(prior, post, design, obs)
end

"""
Criterion that is a function of the observation only.
"""
abstract type ObsCriterion <: Criterion end

(c::ObsCriterion)(_, _, _, obs) = c(obs)

struct PhotonCount <: ObsCriterion end

(c::PhotonCount)(obs) = sum(obs)

"""
Criterion that is a function of the posterior distribution only.
"""
abstract type PosteriorCriterion <: Criterion end

(c::PosteriorCriterion)(_, post, _, _) = c(post)

"""
Total variance utility criterion.
"""
struct TotalVar <: PosteriorCriterion end

function (c::TotalVar)(post::DiscreteParamDist)
  v = var(post)
  return -(v.x[1] + v.x[2] + v.alpha + v.noise)
end

"""
Positional variance utility criterion.
"""
struct PositionVar <: PosteriorCriterion end

function (c::PositionVar)(post::DiscreteParamDist)
  v = var(post)
  return -(v.x[1] + v.x[2])
end

"""
Brightness variance utility criterion.
"""
struct BrightnessVar <: PosteriorCriterion end

function (c::BrightnessVar)(post::DiscreteParamDist)
  v = var(post)
  return -v.alpha
end

"""
Noise variance utility criterion.
"""
struct NoiseVar <: PosteriorCriterion end

function (c::NoiseVar)(post::DiscreteParamDist)
  v = var(post)
  return -v.noise
end

"""
Total entropy utility criterion.
"""
struct TotalEntropy <: PosteriorCriterion end

function (c::TotalEntropy)(post::DiscreteParamDist{D, F}) where {D, F}
  offset = nextfloat(zero(F))
  return sum(weights(post)) do weight
    return weight .* log2(weight + offset)
  end
end

"""
Positional entropy utility criterion.
"""
struct PositionEntropy <: PosteriorCriterion end

function (c::PositionEntropy)(post::GenericParamGrid{D, F}) where {D, F}
  weights = NanoLoc.weights(post)
  weights = reshape(weights, :, length(post.alpha), length(post.noise))
  weights = sum(weights; dims = (2, 3))
  offset = nextfloat(zero(F))
  return sum(weights) do weight
    return weight .* log2(weight + offset)
  end
end

"""
Brightness entropy utility criterion.
"""
struct BrightnessEntropy <: PosteriorCriterion end

function (c::BrightnessEntropy)(post::GenericParamGrid{D, F}) where {D, F}
  weights = NanoLoc.weights(post)
  weights = reshape(weights, :, length(post.alpha), length(post.noise))
  weights = sum(weights; dims = (1, 3))
  offset = nextfloat(zero(F))
  return sum(weights) do weight
    return weight .* log2(weight + offset)
  end
end

"""
Noise entropy utility criterion.
"""
struct NoiseEntropy <: PosteriorCriterion end

function (c::NoiseEntropy)(post::GenericParamGrid{D, F}) where {D, F}
  weights = NanoLoc.weights(post)
  weights = reshape(weights, :, length(post.alpha), length(post.noise))
  weights = sum(weights; dims = (1, 2))
  offset = nextfloat(zero(F))
  return sum(weights) do weight
    return weight .* log2(weight + offset)
  end
end

"""
    utility([crit,] prior, design, observations [; masscap])

Calculate the average utility of `design` over `observations` for a given
`prior`. The argument `criterion` should be of type [`Criterion`](@ref)
and is meant to map posterior distributions to utility scalars. It defaults to
[`TotalEntropy`](@ref).

If the argument `masscap` is provided, the calculation is stopped if the
summed marginal mass of the observations is larger than `masscap`. This may
lead to efficiency gains at low accuracy costs.

In contrast to the sampling method [`utilitys`](@ref), the utilities are
weighted by the marginal mass / density of the respective observations.
Note that this is not the right approach if continuous observations are randomly
sampled (since the sampling already is proportional to the marginal density).
Rather use [`utilitys`](@ref) in this case.
"""
function utility(
  criterion::Criterion,
  prior::DiscreteParamDist{D, F},
  design::Design{D},
  observations;
  masscap = Inf,
) where {D, F}
  @assert !isempty(observations) """
  Cannot calculate utility without observations.
  """
  design = setprecision(design, F)
  buffer = setweights(prior, copy(weights(prior)))
  total_mass = zero(F)
  value = zero(F)

  for obs in unique(observations)
    post, mass = posteriorm!(buffer, prior, design, obs)
    crit = criterion(prior, post, design, obs)
    value += mass * crit
    total_mass += mass
    if total_mass >= masscap
      break
    end
  end
  return value / total_mass
end

"""
    utility([crit], prior, designs :: Array{Design}, observations; kwargs...)

Calculate [`utility`](@ref) for each design in the array `designs`.
The observations are shared for each design.
"""
function utility(
  criterion::Criterion,
  prior::DiscreteParamDist{D, F},
  designs::Array{<:Design{D}},
  args...;
  kwargs...,
) where {D, F}
  return map(designs) do design
    return utility(prior, design, args...; kwargs...)
  end
end

# TODO: Utility function for several designs / observations (product evaluation) + GPU-accelerated versions thereof.

"""
    utilitys(prior, design, observations [, criterion])

Calculate the empirically averaged utility.

In contrast to the method [`utility`](@ref), each observation has the same
weight when calculating the average.
"""
function utilitys(
  criterion::Criterion,
  prior::DiscreteParamDist{D, F},
  design::Design{D},
  samples,
) where {D, F}
  @assert !isempty(samples) """
  Cannot calculate sample utility without observation samples.
  """
  # TODO: count frequencies of unique sample values and use this as weight.
  design = setprecision(design, F)
  samples = collect(samples)
  buffer = setweights(prior, copy(weights(prior)))
  return mean(samples) do obs
    post = posterior!(buffer, prior, design, obs)
    return criterion(prior, post, design, obs)
  end
end

"""
    utilitys(prior, designs :: Array{Design}, observations, args...)

Calculate [`utilitys`](@ref) for each design in the array `designs`.
The observations are shared for each design.
"""
function utilitys(
  criterion::Criterion,
  prior::DiscreteParamDist{D, F},
  designs::Array{<:Design{D}},
  args...;
  kwargs...,
) where {D, F}
  return map(designs) do design
    return utilitys(criterion, prior, design, args...; kwargs...)
  end
end

# Batch utility calculation via broadcasting

function _prepareutilityb(
  prior::DiscreteParamDist{D, F},
  designs,
  observations,
) where {D, F}
  @assert !isempty(observations) """

  """
  ps = NanoLoc.params(prior)
  ws = NanoLoc.weights(prior)

  T = NanoLoc._striparrayparams(typeof(ps))

  ws = reshape(ws, :, 1)
  ps = reshape(ps, :, 1)
  designs = NanoLoc.setprecision.(designs, F)
  designs = reshape(designs, 1, 1, size(designs)...)
  observations = reshape(observations, 1, :)

  observations = convert(T, observations)
  designs = convert(T, designs)

  return ps, ws, designs, observations
end

"""
    utilityb(prior, designs, observations)

Broadcasting-based parallel evaluation of [`utility`](@ref) for the fixed
criterion [`TotalEntropy`](@ref). Should be used by default for GPU arrays.
"""
function utilityb(
  prior::DiscreteParamDist{D, F},
  designs,
  observations,
) where {D, F}
  observations = unique(observations)
  ps, ws, designs, observations = _prepareutilityb(prior, designs, observations)
  offset = nextfloat(zero(F))

  post = NanoLoc.likelihood.(ps, designs, observations) .* ws
  masses = sum(post; dims = 1)
  post ./= masses

  # Negative entropy calculation
  post .= post .* log2.(post .+ offset)
  buf = sum(post; dims = 1)
  buf = sum(buf .* masses; dims = 2) ./ sum(masses; dims = 2)

  return dropdims(buf; dims = (1, 2))
end

"""
    utilitysb(prior, designs, observations)

Broadcasting-based parallel evaluation of [`utilitys`](@ref) for the fixed
criterion [`TotalEntropy`](@ref). Should be used by default for GPU arrays.
"""
function utilitysb(
  prior::DiscreteParamDist{D, F},
  designs,
  samples,
) where {D, F}
  ps, ws, designs, samples = _prepareutilityb(prior, designs, samples)
  offset = nextfloat(zero(F))

  post = likelihood.(ps, designs, samples) .* ws
  masses = sum(post; dims = 1)
  post ./= masses

  # Negative entropy calculation
  post .= post .* log2.(post .+ offset)
  buf = sum(post; dims = (1, 2)) ./ length(samples)

  return dropdims(buf; dims = (1, 2))
end

# Convenience definitions

function utility(prior::DiscreteParamDist, args...; kwargs...)
  return utility(TotalEntropy(), prior, args...; kwargs...)
end

function utilitys(prior::DiscreteParamDist, args...; kwargs...)
  return utilitys(TotalEntropy(), prior, args...; kwargs...)
end

function _getcriterion(f::Function, dist, obs)
  if applicable(f, dist)
    crit = (_, post, _, _) -> f(post)
  elseif applicable(f, obs)
    crit = (_, _, _, obs) -> f(obs)
  else
    crit = f
  end
  return GenericCriterion(crit)
end

function utility(f::Function, prior, design, observations, args...; kwargs...)
  @assert !isempty(observations) """
  Cannot calculate expected utility without observations.
  """
  crit = _getcriterion(f, prior, observations[1])
  return utility(crit, prior, design, observations, args...; kwargs...)
end

function utilitys(f::Function, prior, design, samples, args...; kwargs...)
  @assert !isempty(samples) """
  Cannot calculate sample utility without observed samples.
  """
  crit = _getcriterion(f, prior, samples[1])
  return utility(crit, prior, design, samples, args...; kwargs...)
end
