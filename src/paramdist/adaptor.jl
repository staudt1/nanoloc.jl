
"""
Abstract type for strategies to adapt parameter distributions to
better represent the underlying distribution.
"""
abstract type GridAdaptor end

"""
    adapt(adaptor :: GridAdaptor, grid :: GenericParamGrid)

Adapt the parameter grid `grid` to its distribution via `adaptor`.
Returns the adapted parameter distribution and additional metainformation about
the adaptation process.
"""
adapt(::GridAdaptor, dist::GenericParamGrid) = error("Not implemented")

"""
Dummy adaptor that never adapts a distribution.
"""
struct NoAdaptor <: GridAdaptor end

(::NoAdaptor)(dist::GenericParamGrid) = (dist, (; upgrade = false))

Base.@kwdef struct DyadicCropAdaptor <: GridAdaptor
  masstol::Float64 = 1e-3
  stdtol::Float64 = 1e-1
end

function (adaptor::DyadicCropAdaptor)(dist::GenericParamGrid)
  crop = autocrop(dist; adaptor.masstol)
  if !isnothing(crop)
    var_before = sum(abs2, var(dist).x)
    var_after = sum(abs2, var(crop.dist).x)
    std_frac = sqrt(var_after / var_before)
    if abs(1 - std_frac) <= adaptor.stdtol
      refined = refine(crop.dist; crop.dims)
      return refined, (; upgrade = true)
    end
  end

  return dist, (; upgrade = false)
end

"""
    best_crop_box(grid, boxsize)

Return a spatial box of size `boxsize` that covers the largest possible amount
of mass  of the parameter grid `grid`.
"""
function best_crop_box(
  grid::GenericParamGrid{D, F, P, V},
  boxsize::NTuple{D, Int},
) where {D, F, P, V}

  # Calculate the captured mass and left index of the box per dimension
  values = map(1:D) do dimension

    # Marginalize over all other dimensions
    dims = filter(!isequal(dimension), 1:(D + 2))
    dims = Tuple(dims)

    # Get the cumulative marginal in the respective dimension 
    marginal = marginalize(grid; dims)
    cmarginal = cumsum(marginal)

    # Make sure the boxsize is reasonable
    nsupport = size(grid, dimension)
    len = boxsize[dimension]
    @assert 1 <= len <= nsupport "Boxsize $boxsize invalid"

    pad = convert(V, zeros(F, len))
    a = vcat(pad, cmarginal)
    b = vcat(cmarginal, pad)
    mass, index = findmax(b .- a)
    index = max(1, index - len + 1)

    # Return the mass and respective index range
    return mass, index:(index + len - 1)
  end

  mass = prod(x -> x[1], values)
  box = map(x -> x[2], values)
  return box, mass
end

"""
    dyadic_crop_box(grid; masstol)

Returns a spatial box that covers `1 - masstol` of the mass of the parameter
grid `grid`. If no such box exists, `nothing` is returned.
"""
function dyadic_crop_box(grid::GenericParamGrid{D}; masstol) where {D}
  half(x) = ceil(Int, x / 2)

  sz = size(grid)
  boxsize = Tuple(half.(sz[1:D]))
  box, mass = best_crop_box(grid, boxsize)

  if mass > 1 - masstol
    return box
  end
end

function dyadic_interpolation(data::Array{F, N}; dims = 1:N) where {F, N}

  # Collect the coordinate ranges of the interpolated data
  ranges = []
  options = []
  for dim in 1:N
    if dim in dims && size(data, dim) > 1
      push!(ranges, 1:0.5:size(data, dim))
      push!(options, BSpline(Linear()))
    else
      push!(ranges, 1:1.0:size(data, dim))
      push!(options, NoInterp())
    end
  end

  # Employ the Interpolations package for the actual interpolation
  f = interpolate(data, Tuple(options))
  return map(x -> f(x...), Iterators.product(ranges...))
end

# TODO: This code still allows for refinements of single axes, as well as
# brightness and noise. Since this feature has been removed from cropping, it
# should be removed from here as well to make the code simpler.
"""
    refine(paramgrid; dims)

Return a refined parameter grid that is interpolated along the dimensions
specified by `dims`. Interpolation is realized by inserting new midpoints for
the parameters along each dimension.
"""
function refine(grid::ParamGrid{D, F}; dims = 1:(D + 2)) where {D, F}

  # Function to test if a given dimension is to be refined
  isrefined = dim -> (dim in dims && size(grid, dim) > 1)

  # Refine spatial axes
  xs = map(1:D) do dim
    return isrefined(dim) ? dyadic_interpolation(grid.x[dim]) : grid.x[dim]
  end

  # Refine brightness and noise axes
  alpha = isrefined(D + 1) ? dyadic_interpolation(grid.alpha) : grid.alpha
  noise = isrefined(D + 2) ? dyadic_interpolation(grid.noise) : grid.noise

  # Refine the weights
  dims = filter(isrefined, 1:(D + 2))
  ws = reshape(NanoLoc.weights(grid), size(grid))
  weights = dyadic_interpolation(ws; dims)

  return ParamGrid(xs...; alpha, noise, weights, atype = Array{F})
end

"""
    crop(paramgrid, box)

Restrict the parameter grid `paramgrid` to `box`, which is assumed to be an
iterable of ranges.
"""
function crop(grid::GenericParamGrid{D, F, P, V}, box) where {D, F, P, V}
  @assert length(box) == D "Expected box of length $D."

  # Restrict the grid axes
  xs = map(dim -> grid.x[dim][box[dim]], 1:D)

  # Return the cropped grid
  ws = weights(grid)
  ws = reshape(ws, size(grid)...)
  return ParamGrid(
    xs...;
    grid.alpha,
    grid.noise,
    weights = ws[box..., :, :],
    atype = V,
  )
end

"""
    autocrop(paramgrid; masstol = 1e-3)

Crop a parameter grid `paramgrid` such that at most `masstol` mass is lost.
Returns the cropped grid and the dimensions that were affected by the operation.

Currently, the cropping process works as follows: it is checked if `1 -
masstol` of the mass of the parameter grid can be covered by a box with
halved size of one or several of the grid axes (position, brightness, noise).
To keep spatial uniformity, the positional directions are either all affected
or all remain original.
If such a box exists, the support of the grid is restricted to the support of the
box.
"""
function autocrop(grid::GenericParamGrid{D}; masstol = 1e-3) where {D}
  box = dyadic_crop_box(grid; masstol)
  return if isnothing(box)
    nothing
  else
    (dist = crop(grid, box), dims = 1:D)
  end
end
