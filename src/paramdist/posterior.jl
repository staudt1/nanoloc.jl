
"""
    posteriorm!(post, prior, measurement)
    posteriorm!(post, prior, design, obs)

Returns a tuple `(posterior, mass)` containing the posterior and the marginal
mass for `measurement` / `(design, obs)` given the prior `prior`.

In contrast to [`posteriorm`](@ref), the first argument `post` may be modified or reused for performance reasons.
"""
function posteriorm!(
  post::DiscreteParamDist{D, F},
  prior::DiscreteParamDist{D, F},
  design::Design{D, R, O},
  obs::O,
)::Tuple{DiscreteParamDist{D, F}, F} where {D, R, F, O}
  ws = weights(post)
  @assert size(post) == size(prior) """
  The shape of the posterior distribution to be modified does not match the
  shape of the prior
  """
  design = setprecision(design, F)

  ws .= likelihood.(params(prior), Ref(design), Ref(obs))
  ws .*= weights(prior)
  mass = sum(ws)
  ws .= ws ./ mass

  return post, mass
end

function posteriorm!(post, prior, m::Measurement)
  return posteriorm!(post, prior, m.design, m.obs)
end

"""
    posteriorm(prior, measurement)
    posteriorm(prior, design, obs)

Returns a tuple `(posterior, mass)` containing the posterior and the marginal
mass for `measurement` / `(design, obs)` given the prior `prior`.

See also [`posterior`](@ref) if only the posterior is required.
"""
function posteriorm(
  prior::DiscreteParamDist{D, F},
  design::Design{D, R, O},
  obs::O,
)::Tuple{DiscreteParamDist{D, F}, F} where {D, R, F, O}
  post = setweights(prior, similar(prior.weights))
  return posteriorm!(post, prior, design, obs)
end

function posteriorm(prior, m::Measurement)
  return posteriorm(prior, m.design, m.obs)
end

"""
    posterior(prior, measurement)
    posterior(prior, design, obs)

Return the posterior distribution for `measurement` / `(design, obs)` given the
prior `prior`.

See also [`posterior!`](@ref), [`posteriorm`](@ref), and [`posteriorm!`](@ref).
"""
function posterior(prior::DiscreteParamDist, design::Design, obs)
  return posteriorm(prior, design, obs)[1]
end

function posterior(prior, m::Measurement)
  return posterior(prior, m.design, m.obs)
end

"""
    posterior!(post, design, prior, obs)

Return the posterior for `measurement` / `(design, obs)` given the prior
`prior`.

In contrast to [`posterior`](@ref), the first argument `post` may be modified or reused for performance reasons.
"""
function posterior!(
  post::DiscreteParamDist,
  prior::DiscreteParamDist,
  design::Design,
  obs,
)
  return posteriorm!(post, prior, design, obs)[1]
end

function posterior!(post, prior, m::Measurement)
  return posterior!(post, prior, m.design, m.obs)
end

"""
   posterior(prior, measurements) 

Return the posterior for a vector `measurements` given the prior `prior`.
"""
function posterior(prior::DiscreteParamDist, ms::Vector{<:Measurement})
  @assert !isempty(ms) """
  Cannot calculate posterior for empty measurement vector.    
  """
  ps = params(prior)
  # The distribution is calculated in the logdomain for stability
  ws = loglikelihood.(ps, Ref(ms[1].design), Ref(ms[1].obs))
  for m in ms[2:end]
    ws .= loglikelihood.(ps, Ref(m.design), Ref(m.obs))
  end
  ws .+= logweights(prior)
  ws .-= maximum(ws)
  ws .= exp.(ws)
  ws .= ws ./ sum(ws)

  return setweights(prior, ws)
end

# For compound designs, this way of calculating the posterior might be faster
# (and potentially more stable) than directly doing it via the likelihood,
# especially if optimized posterior methods are available for the primitive
# designs.
function posteriorm!(
  post::DiscreteParamDist{D, F},
  prior::DiscreteParamDist{D, F},
  design::CompoundDesign{D, F, O, N},
  obs::O,
)::Tuple{DiscreteParamDist{D, F}, F} where {D, F, O, N}
  @assert size(weights(post)) == size(weights(prior)) """
  Posterior update shape mismatch
  """
  tmp, mass = posteriorm(prior, design.designs[1], obs[1])
  total_mass = mass
  for i in 2:N
    post, mass = posteriorm!(post, tmp, design.designs[i], obs[i])
    tmp.weights .= post.weights
    total_mass *= mass
  end

  return post, total_mass
end
