
"""
Supertype for parameter distributions.

It is parameterized by the dimension `D` and the underlying floating point type
`F`.
"""
abstract type ParamDist{D, F} <: InferenceState{D} end

# abstract type DiscreteParamDist{D, F} <: InferenceState{D} end

# abstract type ContinuousParamDist{D, F} <: InferenceState{D} end

"""
    rand([rng], dist :: ParamDist, [n])

Sample `n` parameters from the distribution `dist`. Optionally, a random number
generator `rng` may be passed.
"""
function Base.rand(::AbstractRNG, ::ParamDist, ::Integer)
  return error("Not implemented")
end

function Base.rand(rng::AbstractRNG, dist::ParamDist)
  return _takenth(rand(rng, dist, 1), 1)
end

Base.rand(dist::ParamDist) = rand(Random.default_rng(), dist)
Base.rand(dist::ParamDist, n::Integer) = rand(Random.default_rng(), dist, n)

"""
    rand([rng], design :: Design, dist :: ParamDist, [n])

First sample `n` parameters via `params = rand(rng, dist, n)` and then use these
parameters to sample `n` observations.
"""
function Base.rand(
  rng::AbstractRNG,
  design::Design{D},
  dist::ParamDist{D, F},
  n::Integer,
) where {D, F}
  design = setprecision(design, F)
  indices = sample_indices(rng, weights(dist), n)
  return map(index -> rand(rng, design, params(dist)[index]), indices)
end

"""
    randbg([rng], design, dist)

First sample a parameter via `params = rand(rng, dist, n)` and then use this
parameter to sample an observations / background contribution.
"""
function randbg(
  rng::AbstractRNG,
  design::Design{D},
  dist::ParamDist{D, F},
) where {D, F}
  design = setprecision(design, F)
  param = rand(rng, dist)
  return randbg(rng, dist, param)
end

Base.:(==)(::ParamDist, ::ParamDist) = error("Not implemented")

function updatem(dist::ParamDist, design::Design, obs)
  return posterior(dist, design, obs), (;)
end

# abstract type ContinuousParamDist{D, F} <: ParamDist{D, F} end

# struct ProductDensity{D, F} <: ContinuousParamDist{D, F}
#   x :: Vector{UnivariateDistribution}
#   alpha :: UnivariateDistribution
#   noise :: UnivariateDistribution
# end

# function (density :: ProductDensity{D, F})(p :: Param{D}) where {D, F}
#   weight = prod(pdf(density.x[i], p.x[i]) for i in 1:D)
#   weight *= pdf(density.alpha, p.alpha)
#   weight *= pdf(density.noise, p.noise)
#   return weight
# end
