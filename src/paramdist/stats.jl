
"""
    mean(dist :: ParamDist)

Returns the average of the parameter distribution `dist`.
"""
function Statistics.mean(dist::DiscreteParamDist{D, F}) where {D, F}
  ps = params(dist)
  ws = weightsc(dist)

  # Julia magic: reinterpret the contiguous memory region of ps as an
  # array of F-SVectors with length(dist) elements.
  data = reinterpret(SVector{D + 2, F}, ps)
  mdata = sum(data .* ws) ./ sum(ws)
  return _takenth(reinterpret(Param{D, F}, [mdata]), 1)
end

"""
    var(dist :: ParamDist)

Returns the variance of the parameter distribution `dist`.
"""
function Statistics.var(dist::DiscreteParamDist{D, F}) where {D, F}
  ps = params(dist)
  ws = weightsc(dist)
  mass = sum(ws)

  data = reinterpret(SVector{D + 2, F}, ps)
  mdata = sum(ws .* data) ./ mass

  diff2(vec) = (vec - mdata) .^ 2
  vdata = sum(ws .* diff2.(data)) ./ mass

  return _takenth(reinterpret(Param{D, F}, [vdata]), 1)
end

"""
    std(dist :: ParamDist)

Returns the standard deviation of the parameter distribution `dist`.
"""
function Statistics.std(dist::DiscreteParamDist{D, F}) where {D, F}
  vparam = Statistics.var(dist)
  return Param(sqrt.(vparam.x), sqrt(vparam.alpha), sqrt(vparam.noise))
end

"""
    cov(dist :: ParamDist)

Returns the covariance matrix of the parameter distribution `dist`.
"""
function Statistics.cov(dist::DiscreteParamDist{D, F}) where {D, F}
  ps = params(dist)
  ws = reshape(weightsc(dist), 1, :)
  mass = sum(ws)

  data = reinterpret(reshape, F, ps)
  buffer = ws .* data
  mdata = sum(buffer; dims = 2) ./ mass

  # Use broadcasting to compute all covariance values in one go.
  # For this, we have to reshape the data suitably
  n = size(data, 1)

  ws = reshape(ws, 1, 1, :)
  mxdata = reshape(mdata, n, 1, 1)
  mydata = reshape(mdata, 1, n, 1)
  xdata = reshape(data, n, 1, :)
  ydata = reshape(data, 1, n, :)
  buffer = ws .* (xdata .- mxdata) .* (ydata .- mydata)

  vdata = sum(buffer; dims = 3) ./ mass
  return reshape(vdata, n, n)
end

#
# Faster mean and variance methods for Array-based ParamDists
#
# TODO: the Float32 errors in these faster versions are huge for large grids
#

# const ArrayParamDist{D, F} = Union{ParamList{D, F}, ParamGrid{D, F}}

# function Statistics.mean(dist :: ArrayParamDist{D, F}) where {D, F}
#   ps = params(dist)
#   ws = weightsc(dist)

#   data = reinterpret(SVector{D+2, F}, ps)
#   mdata = zero(SVector{D+2, F})

#   @inbounds for i in 1:length(ws)
#     mdata = mdata + ws[i] * data[i]
#   end

#   mdata = mdata / sum(ws)
#   _takenth(reinterpret(Param{D, F}, [mdata]), 1)
# end

# function Statistics.var(dist :: ArrayParamDist{D, F}) where {D, F}
#   ps = params(dist)
#   ws = weightsc(dist)
#   mass = sum(ws)

#   data = reinterpret(SVector{D+2, F}, ps)
#   mdata = zero(SVector{D+2, F})
#   vdata = zero(SVector{D+2, F})

#   @inbounds for i in 1:length(ws)
#     mdata = mdata .+ ws[i] .* data[i]
#   end

#   mdata = mdata / mass

#   @inbounds for i in 1:length(ws)
#     vdata = vdata .+ ws[i] .* (data[i] .- mdata).^2
#   end

#   vdata = vdata / mass

#   _takenth(reinterpret(Param{D, F}, [vdata]), 1)
# end

#
# Highest posterior density set methods
#

## TODO: write tests for these functions!

"""
    hpdset(grid :: ParamGrid, level = 0.9)

Compute the spatial highest probability density set of `grid` at level `0 <=
level <= 1`. Returned is a vector of `Position`s.

Note that this function uses the density relative to the reference measure of
`grid`.
"""
function hpdset(
  dist::ParamGrid{D, F},
  level::Real = 0.9,
)::Vector{Position{D, F}} where {D, F}
  @assert 0 <= level <= 1 "HPD set level must be between 0 and 1 (given: $level)"

  # Marginalize the weights of dist
  ref = marginalizeref(dist, :x)
  weights = marginalize(dist, :x; ref)

  weights = reshape(weights, :)
  ref = reshape(ref, :)

  # Indices of the largets to smallest weight values
  perm = sortperm(weights; rev = true)

  # Find first index with total mass >= level
  s = zero(F)
  level_index = nothing

  for index in 1:length(weights)
    pindex = perm[index]
    s += weights[pindex] * ref[pindex]
    if s >= level
      level_index = index
      break
    end
  end

  if isnothing(level_index)
    if !isapprox(s, one(F))
      @warn "hpdset: distribution weights are not normalized"
    end

    @warn "hpdset: level $level unreached"
    level_index = length(weights)
  end

  # Cartesian indices
  C = CartesianIndices(size(dist, 1:D))

  # Result vector
  points = zeros(Position{D, F}, level_index)
  dataview = reinterpret(reshape, F, points)

  # Populate the result vector
  for sorted_index in 1:level_index
    index = perm[sorted_index]
    k = Tuple(C[index])
    for ix in 1:D
      @inbounds dataview[ix, sorted_index] = dist.x[ix][k[ix]]
    end
  end

  return points
end

"""
    hpdset_value(paramgrid, level = 0.9)

Compute the weight value that corresponds to the `level`-level set of
`paramgrid`.
"""
function hpdset_value(dist::ParamGrid{D, F}, level = 0.9) where {D, F}
  vals = reshape(marginalize(dist, :x), :)
  vals_sorted = sort(vals; rev = true)

  s = zero(F)
  level_index = nothing

  for index in 1:length(vals_sorted)
    s += vals_sorted[index]
    if s >= level
      return vals_sorted[index]
    end
  end

  if !isapprox(s, one(F))
    @warn "hpdset_values: distribution weights are not normalized"
  end

  if isnothing(level_index)
    @warn "hpdset_values: level $level unreached"
  end

  return vals_sorted[end]
end

"""
    hpdsetdiamub(paramgrid, level = 0.9)

Calculate (an upper bound for) the diameter of `hpdset(paramgrid, level)`.
"""
function hpdsetdiamub(dist::ParamDist{D, F}, args...)::F where {D, F}
  points = hpdset(dist, args...)
  c = mean(points)
  rsq, _ = findmax(points) do point
    return sum(abs2, c - point)
  end
  return 2sqrt(rsq)
end

"""
    hpdsetdiamlb(paramgrid, level = 0.9)

Calculate the diameter of a sphere that occupies the same volume as
`hpdset(paramgrid, level)`.

This value is always smaller than the value returned by `hpdsetdiamub`. The
fraction `hpdsetdiamlb / hpdsetdiamub` is close to 1 if the hpdset is similar
to a sphere.

!!! Note: the returned value is only sensible for parameter grids that are
uniform in their spatial axes.
"""
function hpdsetdiamlb(dist::ParamDist{D, F}, args...)::F where {D, F}
  points = hpdset(dist, args...)
  grid_size = prod(size(dist, 1:D))

  grid_volume = prod(dist.x) do xi
    min, max = extrema(xi)
    return isapprox(max, min) ? one(F) : max - min
  end

  hpdset_volume = length(points) / grid_size * grid_volume

  # Radius of a sphere with the given volume
  radius = (hpdset_volume * Bessels.gamma(D / 2 + 1) / pi^(D / 2))^(1 / D)
  return 2radius
end
