
"""
Minflux policy that adapts positon and scale to the posterior.
"""
struct MinfluxBayes <: Policy
  rmin::Float64
  rmax::Float64
  profile::Profile{Float64}
  spots::Int
  radius_to_std::Float64
  intensity::IntensityOptions
  timing::TimingOptions
end

function MinfluxBayes(;
  rmin = 24,
  rmax = 150,
  profile = DonutProfile(; fwhm = 300),
  spots = 4,
  radius_to_std = 1,
  intensity = (sbr = 1,),
  timing = true,
)
  return MinfluxBayes(
    Float64(rmin),
    Float64(rmax),
    setprecision(profile, Float64),
    spots,
    Float64(radius_to_std),
    intensity,
    timing,
  )
end

function (p::MinfluxBayes)(trace::Trace{2})
  x = estimate(trace.state).x
  r = p.radius_to_std * estimateextent(trace.state) / 2
  r = clamp(r, p.rmin, p.rmax)

  design = MinfluxDesign(x, r, p.profile; p.spots, photons = 1)
  setintensity(design, p.intensity)
  return settiming(design, p.timing)
end

policyname(p::MinfluxBayes) = "minflux-bayes"

function policyoptions(p::MinfluxBayes)
  return [
    :radius => "$(p.rmax) to $(p.rmin)",
    :ratios => "[$(p.radius_to_std)]",
    :profile => pp(p.profile) * " ($(p.spots) spots)",
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
