
"""
Minsted-inspired policy that places the circle center on the posterior mean
and chooses a center radius proportional to the extent of the posterior.
"""
struct MinstedBayes <: Policy
  fwhm_min::Float64
  fwhm_max::Float64
  radius_to_std::Float64
  fwhm_to_radius::Float64
  intensity::IntensityOptions
  timing::TimingOptions
end

"""
    MinstedBayes(; kwargs...)

Construct a Bayesian-informed MINSTED policy that automatically adapts the
center and radius of the circle design to the posterior.

## Keyword arguments
* `fwhm_min = 40`: Minimal Gauss profile fwhm.
* `fwhm_max = 300`: Maximal Gauss profile fwhm.
* `radius_to_std = 1`: The radius `r` of the measurement circle is given by \
  `radius_to_std * s`, where `s` denotes the square root of the largest \
  eigenvalue of the spatial covariance of the inference state.
* `fwhm_to_radius = 2`: The fwhm is determined by `r * fwhm_to_radius` \
  and then clamped to the interval `[fwhm_min, fwhm_max]`.
* `intensity = (sbr = 1,)`: Design intensity specification.\
  See [`IntensityOptions`](@ref).
* `timing = true`: Design timing specification.\
  See [`TimingOptions`](@ref).
"""
function MinstedBayes(;
  fwhm_min = 40,
  fwhm_max = 300,
  radius_to_std = 1,
  fwhm_to_radius = 2,
  intensity = (sbr = 1,),
  timing = true,
  kwargs...,
)
  return MinstedBayes(
    Float64(fwhm_min),
    Float64(fwhm_max),
    Float64(radius_to_std),
    Float64(fwhm_to_radius),
    intensity,
    timing,
  )
end

function (p::MinstedBayes)(trace::Trace{D}) where {D}
  x = estimate(trace.state).x
  r = p.radius_to_std * estimateextent(trace.state) / 2
  r = clamp(r, p.fwhm_min / p.fwhm_to_radius, p.fwhm_max / p.fwhm_to_radius)

  fwhm = p.fwhm_to_radius * r
  profile = GaussProfile(; fwhm = fwhm)

  design = CircleDesign(x, r, profile)
  design = setintensity(design, p.intensity)
  design = settiming(design, p.timing)

  return design
end

policyname(p::MinstedBayes) = "minsted-bayes"

# TODO: show intensity and timeout options!
function policyoptions(p::MinstedBayes)
  return [
    :fwhm => "$(p.fwhm_max) to $(p.fwhm_min)",
    :ratios => "[$(p.radius_to_std), $(p.fwhm_to_radius)]",
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
