
"""
Localization policy modeled according to the MINSTED publications.

A circle design measures the direction in which a photon is measured. We then
take a step in this direction and reduce the radius and profile width of the
design.
"""
struct Minsted <: Policy
  xstart::Union{Nothing, Position{2, Float64}}
  fwhm_min::Float64
  fwhm_max::Float64
  shrink_ratio::Float64
  fwhm_to_radius::Float64
  step_ratio::Float64
  intensity::IntensityOptions
  timing::TimingOptions
end

"""
    Minsted(x; kwargs...)

Construct a MINSTED policy that is initially centered at `x`.

## Keyword arguments
* `fwhm_min = 40`: Minimal Gauss profile fwhm.
* `fwhm_max = 150`: Maximal Gauss profile fwhm.
* `shrink_ratio = 0.97`: Factor by which the circle radius is scaled after \
  each observation.
* `fwhm_to_radius = 2`: The Gaussian fwhm is determined by `radius * fwhm_to_radius`.
* `step_ratio = 0.15`: The step length towards the observation is determined \
  by `radius * fwhm_to_radius`.
* `intensity = (sbr = 1,)`: Design intensity specification.\
  See [`IntensityOptions`](@ref).
* `timing = true`: Design timing specification.\
  See [`TimingOptions`](@ref).
"""
function Minsted(
  x = nothing;
  fwhm_min = 40,
  fwhm_max = 150,
  shrink_ratio = 0.97,
  fwhm_to_radius = 2,
  step_ratio = 0.15,
  intensity = (sbr = 1,),
  timing = true,
)
  if !isnothing(x)
    D = length(x)
    x = SVector{D}(Float64.(x))
  end
  return Minsted(
    x,
    Float64(fwhm_min),
    Float64(fwhm_max),
    Float64(shrink_ratio),
    Float64(fwhm_to_radius),
    Float64(step_ratio),
    intensity,
    timing,
  )
end

function (p::Minsted)(trace::Trace{2})
  if trace.steps == 0
    # First step, initialize the policy
    x = isnothing(p.xstart) ? estimate(trace.initial_state).x : p.xstart
    r = p.fwhm_max / p.fwhm_to_radius
  else
    # Look at last design to obtain previous x and r values
    # Also check consistency: all designs under MINSTED must be circle designs
    m = measurements(trace)[end]
    if p.timing.timeout > 0
      @assert m.design isa TimedCircleDesign "Inconsistent previous MINSTED design"
    else
      @assert m.designisa CircleDesign "Inconsistent previous MINSTED design"
    end

    c = designcenter(m.design)
    r = designradius(m.design)

    if isnothing(m.obs) # timeout before photon was observed => enlargen the radius
      x = c
      r = r / p.shrink_ratio
    else # jump towards direction of photon observation and shrink the radius
      angle = first(m.obs)
      x = c .+ r .* p.step_ratio .* sincos(angle)
      r = r * p.shrink_ratio
    end
  end

  r = clamp(r, p.fwhm_min / p.fwhm_to_radius, p.fwhm_max / p.fwhm_to_radius)
  fwhm = r * p.fwhm_to_radius
  profile = GaussProfile(; fwhm = fwhm)

  design = CircleDesign(x, r, profile)
  design = setintensity(design, p.intensity)
  return settiming(design, p.timing)
end

policyname(p::Minsted) = "minsted"

function policyoptions(p::Minsted)
  return [
    :xstart => "$(p.xstart)",
    :fwhm => "$(p.fwhm_max) to $(p.fwhm_min)",
    :ratios => "[$(p.shrink_ratio), $(p.step_ratio), $(p.fwhm_to_radius)]",
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
