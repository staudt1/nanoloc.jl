
"""
    utilityobservations(U :: Type{<: Design})

Default observations for designs of type `U` that are used for the calculation
of the utility in SimpleBED.
"""
function utilityobservations(U::Type{<:Design})
  return error("Explicit observations must be specified for design type $U")
end

utilityobs(::Type{BernoulliDesign}) = 0:1
utilityobs(::Type{PoissonDesign}) = 0:1

"""
Policy that optimizes the expected posterior utility over a grid of simple
design choices.
"""
struct SimpleBED{R, U} <: Policy
  gridsize::Int        # how many support points in each spatial dimension?
  gridextent::Float64  # how far does the grid go (relative to design extent)?
  levels::Int          # stages of optimization
  criterion::Criterion # utility criterion
  observations::Any    # observations used to estimate utility 
  options::Vector{Any} # additional design options
  samples::Int         # subsampling when estimating utility
  intensity::IntensityOptions # autoscale the design brightness
end

"""
    SimpleBED(R :: Design, U :: Design, options; kwargs...)

A simple BED policy that optimizes utilities over designs `U(x, option)`, where `x` is a position on a spatial grid and `option` an entry of the argument `options`.
The design `R(x_opt, option_opt)` is returned, where `(x_opt, option_opt)` is the configuration that optimized the utility.

## Keyword arguments
- `gridsize = 10`: Number of gridpoints in each spatial dimension.
- `gridextent = 1.0`: Factor with which to scale the search area.
- `levels = 1`: Levels of spatial optimization. If `levels > 1`, successive \
  optimizations that refine the previous optimum take place.
- `observations = utilityobs(U)`: Observations over which utility is averaged.
- `criterion = TotalEntropy()`: Utility criterion.
* `intensity = (sbr = 1,)`: Design intensity specification.\
  See [`IntensityOptions`](@ref).
"""
function SimpleBED(
  R::Type{<:Design},
  U::Type{<:Design},
  options;
  gridsize = 10,
  gridextent = 1.0,
  levels = 1,
  criterion = TotalEntropy(),
  observations = utilityobs(U),
  samples = 0,
  intensity = (sbr = 1.6,),
)
  O = obstype(U)
  @assert all(obs -> obs isa O, observations) """
  Passed observations are incompatible with the utility design.
  """
  @assert levels >= 1 "Number of optimization levels must be positive"

  return SimpleBED{R, U}(
    gridsize,
    gridextent,
    levels,
    criterion,
    observations,
    Any[opt for opt in options],
    samples,
    intensity,
  )
end

function SimpleBED(R::Type{<:Design}, options; kwargs...)
  return SimpleBED(R, R, options; kwargs...)
end

function _optutility(
  f,
  criterion,
  dist,
  observations,
  xstart::Position{D},
  xstop::Position{D},
  options;
  gridsize,
  levels,
) where {D}
  xs = []
  for dim in 1:D
    r = range(xstart[dim], xstop[dim], gridsize)
    push!(xs, r)
  end

  xargs = Iterators.product(xs...)
  args = Iterators.product(xargs, options)
  args = collect(args)
  designs = map(a -> f(a...), args)

  uvals = utility(criterion, dist, designs, observations)
  umax, index = findmax(uvals)
  xopt = args[index][1]
  oopt = args[index][2]

  return if levels > 1
    xdelta = (xstop .- xstart) ./ (gridsize .- 1)
    xstart = xopt .- xdelta
    xstop = xopt .+ xdelta
    _optutility(
      f,
      criterion,
      dist,
      observations,
      xstart,
      xstop,
      Any[oopt];
      gridsize,
      levels = levels - 1,
    )
  else
    xopt, oopt, umax
  end
end

function _getdesign(p::SimpleBED, U, x, option, center, radius)
  # place the design at the right grid position
  design = U(x, option)
  r = radius + designextent(design) / 2
  design = setcenter(design, center .+ r .* x)
  return setintensity(design, p.intensity)
end

function (p::SimpleBED{R, U})(trace::Trace{D}) where {D, R, U}
  center = estimate(trace.state).x
  radius = estimateextent(trace.state) / 2

  u = SVector{D}(fill(1.0, D))
  xstart = -p.gridextent .* u
  xstop = p.gridextent .* u

  if p.samples <= 0
    proxy = trace.state
  else
    proxy = ParamList(rand(trace.state, p.samples))
  end

  args = (p.criterion, proxy, p.observations, xstart, xstop, p.options)
  kwargs = (; p.gridsize, p.levels)

  xopt, option, _ = _optutility(args...; kwargs...) do x, option
    return _getdesign(p, U, x, option, center, radius)
  end

  return _getdesign(p, R, xopt, option, center, radius)
end

function policyname(::SimpleBED{R, U}) where {R, U}
  return if R == U
    "simple-bed($(pp(R)))"
  else
    "simple-bed($(pp(R)), $(pp(U)))"
  end
end

function policyoptions(p::SimpleBED)
  profiles = pp.(p.options, alpha = false)
  if length(profiles) <= 4
    str = join(profiles, " ")
  else
    str = "$(length(profiles)): " * join(profiles[1:2], " ")
    str *= " .. " * profiles[end]
  end

  return [
    :gridsize => "$(p.gridsize) ($(p.levels) levels)",
    :criterion => "$(p.criterion) ($(p.observations))",
    :profiles => str,
    :intensity => pp(p.intensity),
  ]
end
