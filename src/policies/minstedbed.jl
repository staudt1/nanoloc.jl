
"""
Minsted-inspired BED policy that searches for an optimal
`CircleDesign` in a discrete set of options.
"""
struct MinstedBED <: Policy
  gridsize::Int
  gridextent::Float64
  levels::Int

  criterion::Criterion
  observations::Vector{Float64}
  samples::Int

  fwhm_min::Float64
  fwhm_max::Float64
  radius_to_std::Vector{Float64}
  fwhm_to_radius::Vector{Float64}

  intensity::IntensityOptions
  timing::TimingOptions
end

"""
    MinstedBED(kwargs...)

Construct a BED MINSTED policy that automatically picks optimal locations,
radii, and fwhms from a given range of options.

## Keyword arguments
- `gridsize = 10`: Number of gridpoints in each spatial dimension.
- `gridextent = 1.0`: Factor with which to scale the search area.
- `levels = 1`: Levels of spatial optimization. If `levels > 1`, successive \
  optimizations that refine the previous optimum take place.
- `radius_to_std = [1.0]`: Options for the value `radius_to_std`\
  (see [`MinstedBayes`](@ref)).
- `fwhm_to_radius = [2.0]`: Options for the value `fwhm_to_radius`\
  (see [`MinstedBayes`](@ref)).
- `observations = 8`: Number of angles used to compute the expected utility.
- `criterion = TotalEntropy()`: Utility criterion.
- `fwhm_min = 40`: Minimal Gauss profile fwhm.
- `fwhm_max = 300`: Maximal Gauss profile fwhm.
* `intensity = (sbr = 1,)`: Design intensity specification.\
  See [`IntensityOptions`](@ref).
* `timing = true`: Design timing specification.\
  See [`TimingOptions`](@ref)
"""
function MinstedBED(;
  gridsize = 10,
  gridextent = 1.0,
  levels = 1,
  criterion = TotalEntropy(),
  observations = 8,
  samples = 0,
  fwhm_min = 40,
  fwhm_max = 300,
  radius_to_std = [1.0],
  fwhm_to_radius = [2],
  intensity = (sbr = 1,),
  timing = true,
)
  if observations isa Integer
    observations = range(0, 2pi; length = observations + 1)[1:observations]
  end

  observations = convert(Vector{Float64}, collect(observations))
  radius_to_std = convert(Vector{Float64}, collect(radius_to_std))
  fwhm_to_radius = convert(Vector{Float64}, collect(fwhm_to_radius))

  return MinstedBED(
    gridsize,
    gridextent,
    levels,
    criterion,
    observations,
    samples,
    fwhm_min,
    fwhm_max,
    radius_to_std,
    fwhm_to_radius,
    intensity,
    timing,
  )
end

function _getdesign(p::MinstedBED, x, (r, fwhm), center, radius)
  profile = GaussProfile(; fwhm = fwhm)
  design = CircleDesign(x, r, profile)
  rr = radius + designextent(design) / 2
  design = setcenter(design, center .+ rr .* x)
  return setintensity(design, p.intensity)
end

function (p::MinstedBED)(trace::Trace{2})
  center = estimate(trace.state).x
  radius = estimateextent(trace.state) / 2

  u = SVector{2}(1.0, 1.0)
  xstart = -p.gridextent .* u
  xstop = p.gridextent .* u

  it = Iterators.product(p.radius_to_std, p.fwhm_to_radius)
  options = map(it) do (radius_to_std, fwhm_to_radius)
    r = radius_to_std * radius
    r = clamp(r, p.fwhm_min / fwhm_to_radius, p.fwhm_max / fwhm_to_radius)
    return r, fwhm_to_radius * r
  end
  options = unique(reshape(options, :))

  if p.samples <= 0
    proxy = trace.state
  else
    proxy = ParamList(rand(trace.state, p.samples))
  end

  args = (p.criterion, proxy, p.observations, xstart, xstop, options)
  kwargs = (; p.gridsize, p.levels)

  xopt, option, _ = _optutility(args...; kwargs...) do x, option
    return _getdesign(p, x, option, center, radius)
  end

  design = _getdesign(p, xopt, option, center, radius)
  return settiming(design, p.timing)
end

policyname(p::MinstedBED) = "minsted-bed"

function policyoptions(p::MinstedBED)
  return [
    :fwhm => "$(p.fwhm_min) to $(p.fwhm_max)",
    :radius_to_std => "$(p.radius_to_std)",
    :fwhm_to_radius => "$(p.fwhm_to_radius)",
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
