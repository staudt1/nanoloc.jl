
# TODO: RASTMIN is currently not supported

"""
Static RASTMIN policy.
"""
struct Rastmin{D} <: Policy
  design::Design{D}
  centered::Bool
  normalsbr::Float64
end

"""
    Rastmin([x], radius, [profile; gridsize, timed, normalsbr])
    Rastmin{D = 2}(radius, [profile; gridsize, timed, normalsbr])

Create a policy that statically returns a `RastminDesign` conditioned on one
photon.

If `x` is not provided, the center position of the initial state is used to
center the design. The argument `normalsbr`, which is set to `1` by default, can
be used to adjust the normal signal-to-background ratio (see `normalsbratio`).
By default, `timed = true`.
"""
function Rastmin(
  x,
  radius::Real,
  args...;
  normalsbr = 1,
  timed = true,
  kwargs...,
)
  @assert false "RASTMIN currently not supported"
  design = RastminDesign(x, radius, args...; photons = 1, timed, kwargs...)
  return Rastmin{length(x)}(design, false, normalsbr)
end

function Rastmin{D}(
  radius::Real,
  args...;
  normalsbr = 1,
  timed = true,
  kwargs...,
) where {D}
  @assert false "RASTMIN currently not supported"
  x = zero(SVector{D, Float64})
  design = RastminDesign(x, radius, args...; photons = 1, timed, kwargs...)
  return Rastmin{D}(design, true, normalsbr)
end

function Rastmin(radius::Real, args...; kwargs...)
  return Rastmin{2}(radius, args...; kwargs...)
end

function (p::Rastmin)(trace::Trace)
  center = p.centered ? estimate(trace.initial_state).x : designcenter(p.design)
  design = setcenter(p.design, center)
  return scalealpha(design; p.normalsbr)
end

function policyname(p::Rastmin)
  return p.design isa TimedDesign ? "t-rastmin" : "rastmin"
end

function policyoptions(p::Rastmin{D}) where {D}
  design = p.design isa TimedDesign ? p.design.base : p.design
  ndesigns = length(design.designs)
  gridsize = round(Int, ndesigns^(1 / D))
  profile = design.designs[1].profile
  center = p.centered ? "auto" : "$(designcenter(design))"

  return [:center => center, :gridsize => "$gridsize", :profile => pp(profile)]
end
