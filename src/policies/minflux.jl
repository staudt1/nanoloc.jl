
"""
Static MINFLUX policy.
"""
struct Minflux <: Policy
  design::Design{2}
  centered::Bool
  intensity::IntensityOptions
  timing::TimingOptions
end

"""
    Minflux([x,] radius, [profile; spots, intensity, timing])

Create a policy that returns a static `MinfluxDesign` conditioned on one
photon.

If `x` is not provided, the estimated parameter position of the initial state
is used to center the design. The argument `intensity`, which is set to `(sbr
= 1,)` by default, can be used to adjust the intensity of the design (see
[`IntensityOptions`](@ref)). The argument `timing` controls the timing options
of the design (see [`TimingOptions`](@ref)).
"""
function Minflux(
  x,
  radius::Real,
  profile::Profile = DonutProfile(; fwhm = 300);
  intensity = (sbr = 1,),
  timing = true,
  kwargs...,
)
  design = MinfluxDesign(x, radius, profile; photons = 1, kwargs...)
  return Minflux(design, false, intensity, timing)
end

function Minflux(
  radius::Real,
  profile::Profile = DonutProfile(; fwhm = 300);
  intensity = (sbr = 1,),
  timing = true,
  kwargs...,
)
  design = MinfluxDesign((0, 0), radius, profile; photons = 1, kwargs...)
  return Minflux(design, true, intensity, timing)
end

function (p::Minflux)(trace::SimulatedTrace)
  if p.centered
    center = metrics(trace, 0).est.x
    design = setcenter(p.design, center)
  else
    design = p.design
  end
  design = setintensity(design, p.intensity)
  return settiming(design, p.timing)
end

policyname(p::Minflux) = "minflux"

function policyoptions(p::Minflux)
  design = p.design isa TimedDesign ? p.design.base : p.design

  spots = length(design.designs)
  profile = design.designs[1].profile
  center = p.centered ? "auto" : "$(designcenter(design))"

  return [
    :center => center,
    :spots => "$spots",
    :profile => pp(profile),
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
