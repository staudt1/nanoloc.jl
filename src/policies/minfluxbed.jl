
"""
Minflux-inspired BED policy that searches for an optimal
`MinfluxDesign` in a discrete set of options.
"""
struct MinfluxBED <: Policy
  gridsize::Int
  gridextent::Float64
  levels::Int

  criterion::Criterion
  observations::Vector{NTuple{N, Int}} where {N}
  samples::Int

  rmin::Float64
  rmax::Float64
  radius_to_std::Vector{Float64}
  profile::Profile{Float64}
  spots::Int

  intensity::IntensityOptions
  timing::TimingOptions
end

"""
    MinfluxBED(kwargs...)

Construct a BED MINFLUX policy that automatically picks optimal locations and
radii from a given range of options.

## Keyword arguments
- `gridsize = 10`: Size of the spatial grid that is optimized over.
- `levels = 1`: Levels of spatial optimization. If `levels > 1`, successive \
  optimizations that refine the previous optimum take place.
- `criterion = TotalEntropy()`: Utility criterion.
- `std_ratios = [1.0]`: Selection of `std_ratio` values (see \
  [`MinfluxBayes`](@ref)) that is optimized over.
- `rmin = 20`: Minimal circle radius.
- `rmax = 300`: Maximal circle radius.
- `profile = DonutProfile(fwhm = 300)`: The profile placed at the measurement spots.
- `spots = 4`: The number of spots of the minflux design.
* `intensity = (sbr = 1,)`: Design intensity specification.\
  See [`IntensityOptions`](@ref).
* `timing = true`: Design timing specification.\
  See [`TimingOptions`](@ref).
"""
function MinfluxBED(;
  gridsize = 10,
  gridextent = 1.0,
  levels = 1,
  criterion = TotalEntropy(),
  samples = 0,
  radius_to_std = [1.0],
  rmin = 20,
  rmax = 120,
  profile = DonutProfile(; fwhm = 300),
  spots = 4,
  intensity = (sbr = 1,),
  timing = true,
)
  observations = map(1:spots) do index
    v = zeros(Int, spots)
    v[index] = 1
    return Tuple(v)
  end

  return MinfluxBED(
    gridsize,
    gridextent,
    levels,
    criterion,
    observations,
    samples,
    Float64(rmin),
    Float64(rmax),
    Float64.(collect(radius_to_std)),
    profile,
    spots,
    intensity,
    timing,
  )
end

function _getdesign(p::MinfluxBED, x, r, center, radius)
  design = MinfluxDesign(x, r, p.profile; p.spots, photons = 1)
  rr = radius + designextent(design) / 2
  design = setcenter(design, center .+ rr .* x)
  return setintensity(design, p.intensity)
end

function (p::MinfluxBED)(trace::Trace{2})
  center = estimate(trace.state).x
  radius = estimateextent(trace.state) / 2

  u = SVector{2}(1.0, 1.0)
  xstart = -p.gridextent .* u
  xstop = p.gridextent .* u

  radii = radius .* p.radius_to_std
  radii = clamp.(radii, p.rmin, p.rmax)
  options = unique(radii)

  if p.samples <= 0
    proxy = trace.state
  else
    proxy = ParamList(rand(trace.state, p.samples))
  end

  args = (p.criterion, proxy, p.observations, xstart, xstop, options)
  kwargs = (; p.gridsize, p.levels)

  xopt, option, _ = _optutility(args...; kwargs...) do x, option
    return _getdesign(p, x, option, center, radius)
  end

  design = _getdesign(p, xopt, option, center, radius)
  return settiming(design, p.timing)
end

policyname(p::MinfluxBED) = "minflux-bed"

function policyoptions(p::MinfluxBED)
  return [
    :radius => "$(p.rmax) to $(p.rmin)",
    :radius_to_std => "$(p.radius_to_std)",
    :profile => pp(p.profile) * " ($(p.spots) spots)",
    :timing => pp(p.timing),
    :intensity => pp(p.intensity),
  ]
end
