
#
# Note: Accelerated methods to calculate posteriors are defined
# in the LoopVectorization extension (ext/LoopVectorizationExt.jl)
#

"""
Design with Poisson distributed observations.
"""
struct PoissonDesign{D, F, P} <: Design{D, F, Int}
  x::Position{D, F}
  profile::P
end

"""
    PoissonDesign([F], x, profile = GaussProfile())

Create a Poisson measurement design centered at `x` with excitation profile
`profile`.
"""
function PoissonDesign(F::Type{<:AbstractFloat}, x, profile = GaussProfile())
  D = length(x)
  return PoissonDesign(SVector{D}(F.(x)), setprecision(profile, F))
end

PoissonDesign(x, args...) = PoissonDesign(Float64, x, args...)

setprecision(d::PoissonDesign{D, F}, ::Type{F}) where {D, F} = d

function setprecision(d::PoissonDesign{D}, ::Type{F}) where {D, F}
  return PoissonDesign(Position{D, F}(d.x), setprecision(d.profile, F))
end

photoncount(::PoissonDesign, obs::Int) = obs

profiles(d::PoissonDesign) = [d.profile]

function designpoints(d::PoissonDesign{D, F}) where {D, F}
  return Position{D, F}[d.x]
end

designcenter(d::PoissonDesign) = d.x

function setcenter(d::PoissonDesign{D, F}, x) where {D, F}
  return PoissonDesign(F, x, d.profile)
end

function setalpha(d::PoissonDesign{D, F}, alpha::Real) where {D, F}
  return PoissonDesign(F, d.x, setalpha(d.profile, alpha))
end

function scalealpha(d::PoissonDesign{D, F}, scale::Real) where {D, F}
  return PoissonDesign(F, d.x, scalealpha(d.profile, scale))
end

function intensity(
  design::PoissonDesign{D, F},
  param::Param{D, F},
)::F where {D, F}
  dx = design.x .- param.x
  return param.alpha * design.profile(dx) + param.noise
end

function loglikelihood(
  param::MultiParam{D, F},
  design::PoissonDesign{D, F},
  obs::Int,
  intensity = NanoLoc.intensity(design, param),
)::F where {D, F}
  return logpdf(Poisson(intensity), obs)
end

function Base.rand(
  rng::AbstractRNG,
  design::PoissonDesign{D, F},
  param::MultiParam{D, F},
  n::Integer,
) where {D, F}
  intensity = NanoLoc.intensity(design, param)
  return rand(rng, Poisson(intensity), n)
end

function randbg(
  rng::AbstractRNG,
  design::PoissonDesign{D, F},
  param::MultiParam{D, F},
) where {D, F}
  bg = rand(rng, Poisson(param.noise))
  signal = rand(rng, design, setnoise(param, 0))
  return (signal + bg, bg)
end

function Base.show(io::IO, design::PoissonDesign)
  return print(io, "PoissonDesign($(design.x), $(design.profile))")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::PoissonDesign{D, F},
) where {D, F}
  println(io, "PoissonDesign{$D, $F}:")
  println(io, " position: $(design.x)")
  return print(io, " profile: $(design.profile)")
end

pp(design::PoissonDesign) = "P" * pp(design.profile)
pp(::Type{<:PoissonDesign}) = "Poi"
pp(::PoissonDesign, obs) = string(obs)
