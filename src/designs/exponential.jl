
"""
Design with exponentially distributed observations.

This can be interpreted as the waiting time until the next photon detection.
"""
struct ExponentialDesign{D, F, P} <: Design{D, F, Float64}
  base::PoissonDesign{D, F, P}
end

"""
    ExponentialDesign([F], x, profile = GaussProfile())

Create an exponential design centered at `x` with excitation profile `profile`.
"""
function ExponentialDesign(args...; kwargs...)
  return ExponentialDesign(PoissonDesign(args...; kwargs...))
end

function setprecision(d::ExponentialDesign, ::Type{F}) where {F}
  return ExponentialDesign(setprecision(d.base, F))
end

photoncount(::ExponentialDesign, obs::Float64) = 1
profiles(d::ExponentialDesign) = profiles(d.base)
designpoints(d::ExponentialDesign) = designpoints(d.base)
designcenter(d::ExponentialDesign) = designcenter(d.base)
intensity(d::ExponentialDesign, param) = intensity(d.base, param)

setcenter(d::ExponentialDesign, x) = ExponentialDesign(setcenter(d.base, x))

function setalpha(d::ExponentialDesign, alpha::Real)
  return ExponentialDesign(setalpha(d.base, alpha))
end

function scalealpha(d::ExponentialDesign, scale::Real)
  return ExponentialDesign(scalealpha(d.base, scale))
end

function likelihood(
  param::MultiParam{D, F},
  design::ExponentialDesign{D, F},
  obs::Float64,
  intensity = NanoLoc.intensity(design, param),
)::F where {D, F}
  return if intensity <= 0
    # @warn "Encountered zero intensity in likelihood of exponential design" maxlog = 1
    zero(F)
  else
    intensity * exp(-intensity * obs)
  end
end

function loglikelihood(
  param::MultiParam{D, F},
  design::ExponentialDesign{D, F},
  obs::Float64,
  intensity = NanoLoc.intensity(design, param),
)::F where {D, F}
  return log(likelihood(param, design, obs, intensity))
end

function Base.rand(
  rng::AbstractRNG,
  design::ExponentialDesign,
  param::MultiParam,
  n::Integer,
)
  intensity = NanoLoc.intensity(design, param)
  return if intensity <= 0
    @warn "Encountered zero intensity in rand of timed design" maxlog = 1
    zeros(Float64, n)
  else
    rand(rng, Exponential(1 / intensity), n)
  end
end

function randbg(rng::AbstractRNG, design::ExponentialDesign, param::MultiParam)
  noise_intensity = param.noise
  signal_intensity = signalintensity(design, param)

  if noise_intensity >= 0
    time_bg = rand(rng, Exponential(1 / noise_intensity))
  else
    time_bg = Inf
  end

  if signal_intensity >= 0
    time_signal = rand(rng, Exponential(1 / signal_intensity))
  else
    time_signal = Inf
  end

  return if time_bg < time_signal
    (time_bg, 1)
  else
    (time_signal, 0)
  end
end

function Base.show(io::IO, design::ExponentialDesign)
  return print(
    io,
    "ExponentialDesign($(design.base.x), $(design.base.profile))",
  )
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::ExponentialDesign{D, F},
) where {D, F}
  println(io, "ExponentialDesign{$D, $F}:")
  println(io, " position: $(design.base.x)")
  return print(io, " profile: $(design.base.profile)")
end

pp(::Type{<:ExponentialDesign}) = "Exp"
pp(design::ExponentialDesign) = "E" * pp(design.base.profile)
pp(::ExponentialDesign, obs) = @sprintf "%.2f" obs
