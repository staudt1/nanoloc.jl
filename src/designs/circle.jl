
"""
Circular design used to model single-photon MINSTED measurements.

A circle is tracked with a given intensity profile until a photon is detected.
The angle of detection is returned as observation.
"""
struct CircleDesign{F, P} <: Design{2, F, Float64}
  x::Position{2, F}
  radius::F
  profile::P
end

"""
    CircleDesign([F], x, radius, profile = GaussProfile())

Create a circular measurement centered at `x` with radius `radius` and profile
`profile`.
"""
function CircleDesign(
  F::Type{<:AbstractFloat},
  x,
  radius::Real,
  profile = GaussProfile(),
)
  @assert length(x) == 2 "Circle design only supported in two dimensions"
  return CircleDesign(SVector{2}(F.(x)), F(radius), setprecision(profile, F))
end

CircleDesign(x, args...) = CircleDesign(Float64, x, args...)

setprecision(d::CircleDesign{F}, ::Type{F}) where {F} = d

function setprecision(d::CircleDesign, ::Type{F}) where {F}
  return CircleDesign(
    Position{2, F}(d.x),
    F(d.radius),
    setprecision(d.profile, F),
  )
end

photoncount(d::CircleDesign, obs) = 1

profiles(d::CircleDesign) = [d.profile]

function designpoints(d::CircleDesign{F}) where {F}
  return Position{2, F}[d.x]
end

designcenter(d::CircleDesign) = d.x
designradius(d::CircleDesign) = d.radius

function setcenter(d::CircleDesign{F}, x) where {F}
  return CircleDesign(F, x, d.radius, d.profile)
end

function setalpha(d::CircleDesign{F}, alpha::Real) where {F}
  return CircleDesign(F, d.x, d.radius, setalpha(d.profile, alpha))
end

function scalealpha(d::CircleDesign{F}, scale::Real) where {F}
  return CircleDesign(F, d.x, d.radius, scalealpha(d.profile, scale))
end

function intensity(
  design::CircleDesign{F, <:GaussProfile},
  param::Param{2, F},
)::F where {F}
  a = design.profile.width
  r = design.radius
  c = design.x

  dc = sum(abs2, param.x .- c)
  factor1 = design.profile(sqrt(dc + abs2(r)))
  factor2 = Bessels.besseli0(2r * sqrt(dc) / a^2)

  return 2pi * (param.alpha * factor1 * factor2 + param.noise)
end

function intensity(
  design::CircleDesign{F, <:QuadraticProfile},
  param::Param{2, F},
)::F where {F}
  a = design.profile.width
  r = design.radius
  c = design.x

  dc = sum(abs2, param.x .- c)
  part = design.profile.alpha * (dc + abs2(r)) / a^2

  return 2pi * (param.alpha * part + param.noise)
end

function intensity(
  design::CircleDesign{F, <:DonutProfile},
  param::Param{2, F},
)::F where {F}
  a = design.profile.width
  r = design.radius
  c = design.x

  dc = sum(abs2, param.x .- c)
  term1 = (dc + abs2(r)) / abs2(a)
  term2 = 2r * sqrt(dc) / abs2(a)

  part1 = term1 * exp(1 - term1) * Bessels.besseli0(term2)
  part2 = term2 * exp(1 - term1) * Bessels.besseli1(term2)

  return 2pi * (param.alpha * (part1 - part2) + param.noise)
end

# Specifying likelihood instead of loglikelihood for performance reasons
function likelihood(
  param::MultiParam{2, F},
  design::CircleDesign{F},
  obs::Float64,
  intensity = NanoLoc.intensity(design, param),
)::F where {F}
  return if intensity == 0
    1 / (2F(pi))
  else
    u = sincos(F(obs))
    r = sqrt(sum(abs2, design.x .+ design.radius .* u .- param.x))
    lambda = param.alpha * design.profile(r) + param.noise
    lambda / intensity
  end
end

function loglikelihood(
  param::MultiParam{2, F},
  design::CircleDesign{F},
  obs::Float64,
  intensity = NanoLoc.intensity(design, param),
)::F where {F}
  return log(likelihood(param, design, obs, intensity))
end

## TODO: this way of sampling on the circle is ugly.
## Instead, we could use a subgraph-rejection sampler, for which we would only
## need the maximum of the likelihood. For Gaussian / Quadratic profiles, the
## maximum is trivial. For Donut profiles, it is a bit harder and must probably
## be determined numerically
function Base.rand(
  rng::AbstractRNG,
  design::CircleDesign,
  param::MultiParam,
  n::Integer,
)
  h = 2pi / 1000
  range = 0:h:(2pi)
  vals = likelihood.(param, design, range)
  indices = sample_indices(rng, vals, n)
  return range[indices]
end

function randbg(
  rng::AbstractRNG,
  design::CircleDesign{F},
  param::MultiParam,
) where {F}
  intensity = NanoLoc.intensity(design, param)
  p = rand(rng)

  return if intensity <= zero(F) # random direction if there is no intensity
    obs = 2pi * rand(rng)
    (obs, 0)

  elseif p <= 2pi * param.noise / intensity # background photon
    obs = 2pi * rand(rng)
    (obs, 1)

  else # signal photon
    obs = rand(rng, design, setnoise(param, 0))
    (obs, 0)
  end
end

function Base.show(io::IO, design::CircleDesign)
  return print(
    io,
    "CircleDesign($(design.x), $(design.radius), $(design.profile))",
  )
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::CircleDesign{F},
) where {F}
  println(io, "CircleDesign{$F}:")
  println(io, " position: $(design.x)")
  println(io, " radius: $(design.radius)")
  return print(io, " profile: $(design.profile)")
end

pp(::Type{<:CircleDesign}) = "Circle"

function pp(design::CircleDesign)
  prefix = @sprintf "C(%.1f)" design.radius
  return prefix * pp(design.profile)
end

pp(::CircleDesign, obs) = @sprintf "%.0f°" obs / pi * 180
