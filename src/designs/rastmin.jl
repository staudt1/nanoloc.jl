
"""
    RastminDesign([F], x, radius, [profile]; gridsize = 5, [photons])

Create a RASTMIN-inspired measurement design.

A grid of Poisson designs (with `profile = DonutProfile(fwhm = 300)` by
default) is centered at the location `x`. The grid has a diameter of `2radius`
and has `gridsize` support points in each dimension.

See the similar [`MinfluxDesign`](@ref) for the meaning of the remaining
arguments.
"""
function RastminDesign(
  F::Type{<:AbstractFloat},
  x,
  radius::Real,
  profile = DonutProfile(; fwhm = 300);
  gridsize = 5,
  photons = nothing,
)
  pdesigns = []

  D = length(x)
  grid = [range(x[i] - radius, x[i] + radius; length = gridsize) for i in 1:D]

  for pos in Iterators.product(grid...)
    pos = Position{D, F}(pos...)
    push!(pdesigns, PoissonDesign(F, pos, profile))
  end

  # If photons == nothing, the measurements are taken to be independent.
  # Otherwise, the number of photons is conditioned on.
  return if isnothing(photons)
    CompoundDesign(pdesigns...)
  else
    MultinomialDesign(photons, pdesigns...)
  end
end

function RastminDesign(x, r::Real, args...; kwargs...)
  return RastminDesign(Float64, x, r::Real, args...; kwargs...)
end
