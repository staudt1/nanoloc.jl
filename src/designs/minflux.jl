
"""
    MinfluxDesign([F], x, radius, [profile]; spots = 4, [photons])

Create a MINFLUX-inspired measurement design.

A number of `spots - 1` Poisson designs (with `profile = DonutProfile(fwhm =
300)` by default) surround a central design (at location `x`) at a distance of
`radius`. If `photons` is a positive integer, the observation is conditioned
on this number of photons (see [`MultinomialDesign`](@ref)). In this case,
`timed` can be used to create a [`TimedMultinomialDesign`](@ref). Otherwise, a
[`CompoundDesign`](@ref) is returned.

The optional floating point type `F` defaults to `Float64`.
"""
function MinfluxDesign(
  F::Type{<:AbstractFloat},
  x,
  radius::Real,
  profile = DonutProfile(; fwhm = 300);
  spots = 4,
  photons = nothing,
)

  # The central design point
  pdesigns = [PoissonDesign(F, x, profile)]

  # Design points that surround the central one
  n = spots - 1
  for index in 1:n
    angle = (index - 1) / n * 2pi
    spot = x .+ radius .* sincos(angle)
    push!(pdesigns, PoissonDesign(F, spot, profile))
  end

  # If photons == nothing, the measurements are assumed to be independent.
  # Otherwise, we condition on the number of photons.
  return if isnothing(photons)
    CompoundDesign(pdesigns...)
  else
    MultinomialDesign(photons, pdesigns...)
  end
end

function MinfluxDesign(x, r::Real, args...; kwargs...)
  return MinfluxDesign(Float64, x, r::Real, args...; kwargs...)
end
