
#
# Bernoulli measurement design
#
# Note that accelerated methods to calculate posteriors are defined
# in the LoopVectorization extension of NanoLoc
#

"""
Design with Bernoulli distributed observations.

This design can be interpreted as a Poisson design that only distinguishes if
zero or a positive number of photons have been observed.
"""
struct BernoulliDesign{D, F, P} <: Design{D, F, Int}
  base::PoissonDesign{D, F, P}
end

"""
    BernoulliDesign([F], x, profile = GaussProfile())

Create a Bernoulli measurement design centered at `x` with a given excitation `profile`.
"""
function BernoulliDesign(args...; kwargs...)
  return BernoulliDesign(PoissonDesign(args...; kwargs...))
end

function setprecision(d::BernoulliDesign, ::Type{F}) where {F}
  return BernoulliDesign(setprecision(d.base, F))
end

photoncount(::BernoulliDesign, obs::Int) = obs
profiles(d::BernoulliDesign) = profiles(d.base)
designpoints(d::BernoulliDesign) = designpoints(d.base)
designcenter(d::BernoulliDesign) = designcenter(d.base)
intensity(d::BernoulliDesign, param) = intensity(d.base, param)

setcenter(d::BernoulliDesign, x) = BernoulliDesign(setcenter(d.base, x))

function setalpha(d::BernoulliDesign, alpha::Real)
  return BernoulliDesign(setalpha(d.base, alpha))
end

function scalealpha(d::BernoulliDesign, scale::Real)
  return BernoulliDesign(scalealpha(d.base, scale))
end

function loglikelihood(
  param::MultiParam{D, F},
  design::BernoulliDesign{D, F},
  obs::Int,
  intensity = NanoLoc.intensity(design, param),
)::F where {D, F}
  return if intensity < 0
    typemin(F)
  elseif obs == 0
    -intensity
  elseif obs == 1
    log(one(F) - exp(-intensity))
  else
    typemin(F)
  end
end

function Base.rand(
  rng::AbstractRNG,
  design::BernoulliDesign,
  param::MultiParam,
  n::Integer,
)
  obs = rand(rng, design.base, param, n)
  return min.(obs, 1)
end

function randbg(rng::AbstractRNG, design::BernoulliDesign, param::MultiParam)
  obs, bg = randbg(rng, design.base, param)
  return if obs - bg > 0
    (1, 0) # Background does not matter in this case, since we also got signal
  elseif bg > 0
    (1, 1)
  else
    (0, 0)
  end
end

function Base.show(io::IO, design::BernoulliDesign)
  return print(io, "BernoulliDesign($(design.base.x), $(design.base.profile))")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::BernoulliDesign{D, F},
) where {D, F}
  println(io, "BernoulliDesign{$D, $F}")
  println(io, " position: $(design.base.x)")
  return print(io, " profile: $(design.base.profile)")
end

pp(design::BernoulliDesign) = "B" * pp(design.base.profile)
pp(::Type{<:BernoulliDesign}) = "Ber"
pp(::BernoulliDesign, obs) = string(obs)
