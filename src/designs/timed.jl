
"""
Design that extends a base design by including measurement duration information
to the observation.

The photon arrival time `time` is modeled as exponentially distributed with
intensity parameter derived by the sum of the intensity values returned by
[`intensity`](@ref). It is assumed to be statistically independent of the base
observation `obs`.

A time limit `timeout`, after which an uncompleted measurement is regarded
as failure, can optionally be specified. The extended observations are tuples
`(obs, time)` if `time <= timeout` and `nothing` if `time > timeout`.

---

    TimedDesign(base::Design, timeout = Inf)

Create a `TimedDesign` with base design `base`. The absolute timeout for this
design can  be set via `timeout`.

This constructor is meant for internal usage mainly. To create a timed design
with flexible timing options use [`settiming`](@ref) instead.
"""
struct TimedDesign{D, F, O, B <: Design{D, F, O}} <:
       Design{D, F, Union{Nothing, Tuple{O, Float64}}}
  base::B
  timeout::Float64

  function TimedDesign(
    base::B,
    timeout::Real = Inf,
  ) where {D, F, O, B <: Design{D, F, O}}
    @assert base isa Union{CircleDesign, MultinomialDesign} """
    Timing is not supported for designs of type $(typeof(base)).
    """
    @assert timeout > 0 """
    Expected a positive timeout value, got $timeout.
    """
    return new{D, F, O, B}(base, timeout)
  end
end

"""
Convenience type alias for `TimedDesign{..., CircleDesign}`.
"""
const TimedCircleDesign{F, P} = TimedDesign{2, F, Float64, CircleDesign{F, P}}

function TimedCircleDesign(args...; timeout = Inf, kwargs...)
  return TimedDesign(CircleDesign(args...; kwargs...), timeout)
end

"""
Convenience type alias for `TimedDesign{..., MultinomialDesign}`.
"""
const TimedMultinomialDesign{D, F, N, T} =
  TimedDesign{D, F, NTuple{N, Int}, MultinomialDesign{D, F, N, T}}

function TimedMultinomialDesign(args...; timeout = Inf, kwargs...)
  return TimedDesign(MultinomialDesign(args...; kwargs...), timeout)
end

basedesign(d::TimedDesign) = d.base

function setprecision(d::TimedDesign, ::Type{F}) where {F}
  return TimedDesign(setprecision(d.base, F), d.timeout)
end

photoncount(d::TimedDesign, obs) = photoncount(d.base, obs[1])
photoncount(d::TimedDesign, ::Nothing) = 0

profiles(d::TimedDesign) = profiles(d.base)
designpoints(d::TimedDesign) = designpoints(d.base)
designcenter(d::TimedDesign) = designcenter(d.base)
designradius(d::TimedDesign) = designradius(d.base)
intensity(d::TimedDesign, param::Param) = intensity(d.base, param)

function setcenter(d::TimedDesign, x)
  return TimedDesign(setcenter(d.base, x), d.timeout)
end

function setalpha(d::TimedDesign, alpha::Real)
  return TimedDesign(setalpha(d.base, alpha), d.timeout)
end

function scalealpha(d::TimedDesign, scale::Real)
  return TimedDesign(scalealpha(d.base, scale), d.timeout)
end

function loglikelihood(
  param::MultiParam,
  design::TimedDesign{D, F, O},
  obs::Tuple{O, Float64},
  intensity = NanoLoc.intensity(design.base, param),
) where {D, F, O}
  if obs[2] > design.timeout
    return typemin(F)
  else
    intensity_sum = sum(intensity)
    if intensity_sum < 0
      # @warn "Encountered zero intensity in likelihood of timed design" maxlog = 1
      return typemin(F)
    else
      val_base = loglikelihood(param, design.base, obs[1], intensity)
      val_time = log(intensity_sum) - intensity_sum * obs[2]
      return val_base + val_time
    end
  end
end

function loglikelihood(
  param::MultiParam,
  design::TimedDesign{D, F, O},
  ::Nothing,
  intensity = NanoLoc.intensity(design.base, param),
) where {D, F, O}
  intensity_sum = sum(intensity)
  if intensity_sum < 0
    return typemin(F)
  else
    return - intensity_sum * design.timeout
  end
end

# Factor 2 speedup for TimedCircleDesigns if the likelihood is not calculated
# via the loglikelihood.
function likelihood(
  param::MultiParam,
  design::TimedDesign{D, F, O},
  obs::Tuple{O, Float64},
  intensity = NanoLoc.intensity(design.base, param),
) where {D, F, O}
  if obs[2] > design.timeout
    return zero(F)
  else
    intensity_sum = sum(intensity)
    if intensity_sum < 0
      # @warn "Encountered zero intensity in likelihood of timed design" maxlog = 1
      return zero(F)
    else
      val_base = likelihood(param, design.base, obs[1], intensity)
      val_time = intensity_sum * exp(- intensity_sum * obs[2])
      return val_base * val_time
    end
  end
end

function Base.rand(
  rng::AbstractRNG,
  design::TimedDesign,
  param::MultiParam,
  n::Integer,
)
  intensity = NanoLoc.intensity(design.base, param)
  intensity = sum(intensity)

  if intensity <= 0
    @warn "Encountered zero intensity in rand of timed design" maxlog = 1
    times = zeros(Float64, n)
  else
    times = rand(rng, Exponential(1 / intensity), n)
  end

  observations = rand(rng, design.base, param, n)
  return map(observations, times) do obs, time
    return time > design.timeout ? nothing : (obs, time)
  end
end

function randbg(rng::AbstractRNG, design::TimedDesign, param::MultiParam)
  intensity = NanoLoc.intensity(design.base, param)
  intensity = sum(intensity)

  if intensity <= 0
    @warn "Encountered zero intensity in randbg of timed design" maxlog = 1
    time = zero(Float64)
  else
    time = rand(rng, Exponential(1 / intensity))
  end

  if time > design.timeout
    return (nothing, 0)
  else
    obs, bg = randbg(rng, design.base, param)
    return ((obs, time), bg)
  end
end

function Base.show(io::IO, design::TimedDesign)
  print(io, "Timed")
  return print(io, design.base)
end

function Base.show(io::IO, mime::MIME"text/plain", design::TimedDesign)
  print(io, "Timed")
  show(io, mime, design.base)
  return print(io, "\n timeout: $(design.timeout)")
end

pp(::Type{<:TimedDesign}) = "Timed"
pp(design::TimedDesign) = "t" * pp(design.base)

function pp(design::TimedDesign, ::Nothing)
  return @sprintf "()"
end

function pp(design::TimedDesign, obs)
  return @sprintf "(%s, %.2f)" pp(design.base, obs[1]) obs[2]
end

"""
Specification of timing options for a design.
  
Designs can be untimed, absolutely timed, or relatively timed. Relative timings
are relative to the expected return time for a given reference parameter.

For example, a relative timeout of `2.5` for a given reference `param :: Param`
corresponds to an absolute timeout of `2.5 / signalintensity(design, param)`.

---
    TimingOptions(; timeout, [relative, reference])
    TimingOptions(timeout :: Real)
    TimingOptions(timed :: Bool)

Create timing options.

By default, `relative = true` and `reference = nothing`, which corresponds to a
reference parameter at the design center with brightness `alpha = 1`.

If only a real value `timeout` is passed, the options are equivalent to
`(;timeout, relative = false, reference = nothing)`.

If only a bool `timed` is passed, the timing is either inactive
(`timed = false`, equivalent to `timeout <= 0`), or active without timeout
(`timed = true`, equivalent to `timeout = Inf`).
"""
struct TimingOptions
  timeout::Float64 # negative: no TimedDesign
  relative::Bool
  reference::Union{Nothing, Param} # reference to be used if relative = true
end

TimingOptions(timeout::Real) = TimingOptions(timeout, true, nothing)
TimingOptions(timed::Bool) = timed ? TimingOptions(Inf) : TimingOptions(-1)

function TimingOptions(; timeout, relative = true, reference = nothing)
  return TimingOptions(timeout, relative, reference)
end

Base.convert(::Type{TimingOptions}, timeout::Real) = TimingOptions(; timeout)
Base.convert(::Type{TimingOptions}, timed::Bool) = TimingOptions(timed)

function Base.convert(::Type{TimingOptions}, kwargs::NamedTuple)
  return TimingOptions(; kwargs...)
end

"""
    settiming(design :: Design)
    settiming(design :: Design, options)

Convert `design` to a [`TimedDesign`](@ref) with timing options specified by
[`TimingOptions`](@ref). If no options are provided, this is equivalent to `TimingOptions(true)`.
"""
function settiming(design::TimedDesign, options)
  return settiming(design.base, options)
end

function settiming(design::Design, options = true)
  return settiming(design, convert(TimingOptions, options))
end

function settiming(design::Design, options::TimingOptions)
  if options.timeout <= 0 # no timing applied
    return design
  elseif !options.relative
    return TimedDesign(design, options.timeout)
  else
    if isnothing(options.reference)
      reference = Param(designcenter(design); alpha = 1)
    else
      reference = options.reference
    end
    expected_time = 1 / sum(signalintensity(design, reference))
    return TimedDesign(design, options.timeout * expected_time)
  end
end

function pp(timing::TimingOptions)
  if timing.timeout <= 0
    "untimed"
  elseif timing.timeout == Inf
    "no timeout"
  elseif !timing.relative
    @sprintf "timeout %.2f (abs)" timing.timeout
  else
    @sprintf "timeout %.2f (rel)" timing.timeout
  end
end
