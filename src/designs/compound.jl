
"""
Compound design that combines independent measurements.

The observation under this design is a tuple of individual observations.

---

    CompoundDesign(designs...)

Returns a compound design that independently applies all `designs`.
"""
struct CompoundDesign{D, F, O, N, T} <: Design{D, F, O}
  designs::T

  function CompoundDesign(designs::Design{D, F}...) where {D, F}
    N = length(designs)
    obstypes = map(obstype, designs)
    O = Tuple{obstypes...}
    T = typeof(designs)
    return new{D, F, O, N, T}(designs)
  end
end

setprecision(d::CompoundDesign{D, F}, ::Type{F}) where {D, F} = d

function setprecision(d::CompoundDesign{D}, ::Type{F}) where {D, F}
  return CompoundDesign(setprecision.(d.designs, F)...)
end

function photoncount(d::CompoundDesign{D, F, O, N}, obs::O) where {D, F, O, N}
  return sum(1:N) do index
    return photoncount(d.designs[index], obs[index])
  end
end

profiles(d::CompoundDesign) = mapreduce(profiles, vcat, d.designs)

function designpoints(d::CompoundDesign{D, F}) where {D, F}
  return mapreduce(designpoints, vcat, d.designs)
end

designcenter(d::CompoundDesign) = mean(designpoints(d))

function designradius(d::CompoundDesign)
  c = designcenter(d)
  return maximum(designpoints(d)) do x
    return sqrt(sum(abs2, c .- x))
  end
end

function setcenter(d::CompoundDesign{D, F}, x) where {D, F}
  offset = Position{D, F}(x...) .- designcenter(d)
  designs = map(d.designs) do design
    center = designcenter(design)
    return setcenter(design, center + offset)
  end
  return CompoundDesign(designs...)
end

function setalpha(d::CompoundDesign{D, F}, alpha::Real) where {D, F}
  designs = setalpha.(d.designs, alpha)
  return CompoundDesign(designs...)
end

function scalealpha(d::CompoundDesign{D, F}, scale::Real) where {D, F}
  designs = scalealpha.(d.designs, scale)
  return CompoundDesign(designs...)
end

function intensity(design::CompoundDesign, param::Param)
  return map(d -> intensity(d, param), design.designs)
end

function loglikelihood(
  param::MultiParam,
  design::CompoundDesign{D, F, O, N},
  obs::O,
)::F where {D, F, O, N}
  l = zero(F)
  for i in 1:N
    l += loglikelihood(param, design.designs[i], obs[i])
  end
  return l
end

function loglikelihood(
  param::MultiParam,
  design::CompoundDesign{D, F, O, N},
  obs::O,
  intensity,
)::F where {D, F, O, N}
  l = zero(F)
  for i in 1:N
    l += loglikelihood(param, design.designs[i], obs[i], intensity[i])
  end
  return l
end

Base.length(::CompoundDesign{D, F, O, N}) where {D, F, O, N} = N

function Base.rand(
  rng::AbstractRNG,
  design::CompoundDesign{D, F, O, N},
  param::MultiParam,
  n::Integer,
) where {D, F, O, N}
  outcomes = map(d -> Base.rand(rng, d, param, n), design.designs)
  return collect(zip(outcomes...))
end

function randbg(
  rng::AbstractRNG,
  design::CompoundDesign{D, F, O, N},
  param::MultiParam,
) where {D, F, O, N}
  bgcount = 0
  obs = map(design.designs) do d
    obs, bg = randbg(rng, d, param)
    bgcount += bg
    return obs
  end
  return obs, bgcount
end

function Base.show(io::IO, ::CompoundDesign{D, F, O, N}) where {D, F, O, N}
  return print(io, "CompoundDesign($N designs)")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::CompoundDesign{D, F, O, N},
) where {D, F, O, N}
  print(io, "CompoundDesign{$D, $F}($N designs):")
  for d in design.designs
    print(io, "\n $d")
  end
end

pp(::Type{<:CompoundDesign}) = "Compound"

function pp(design::CompoundDesign)
  len = length(design)
  subdesign = design.designs[1]
  prefix = "C$len"
  return prefix * pp(subdesign) * "..."
end

function pp(design::CompoundDesign, obs)
  return join([pp(d, o) for (d, o) in zip(design.designs, obs)])
end
