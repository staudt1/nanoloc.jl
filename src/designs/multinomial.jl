
#
# Multinomial compound design
#

"""
Compound multinomial design.

Several independent `PoissonDesign`s are conditioned on a total number of
collected photons. This results in a a multinomial measurement.

---
    MultinomialDesign(N, designs...)

Combine Poisson designs `designs` and condition their sum on `N`.
"""
struct MultinomialDesign{D, F, N, T} <: Design{D, F, NTuple{N, Int}}
  photons::Int
  designs::T
end

function MultinomialDesign(
  photons::Int,
  designs::PoissonDesign{D, F}...,
) where {D, F}
  @assert photons > 0 "MultinomialDesign expects positive number of total photons"
  N = length(designs)
  T = typeof(designs)
  return MultinomialDesign{D, F, N, T}(photons, designs)
end

setprecision(d::MultinomialDesign{D, F}, ::Type{F}) where {D, F} = d

function setprecision(d::MultinomialDesign{D}, ::Type{F}) where {D, F}
  return MultinomialDesign(d.photons, setprecision.(d.designs, F)...)
end

function photoncount(
  d::MultinomialDesign{D, F, N},
  obs::NTuple{N, Int},
) where {D, F, N}
  @assert sum(obs) == d.photons
  return d.photons
end

profiles(d::MultinomialDesign) = mapreduce(profiles, vcat, d.designs)

function designpoints(d::MultinomialDesign{D, F}) where {D, F}
  return mapreduce(designpoints, vcat, d.designs)
end

designcenter(d::MultinomialDesign) = mean(designpoints(d))

function designradius(d::MultinomialDesign)
  c = designcenter(d)
  return maximum(designpoints(d)) do x
    return sqrt(sum(abs2, c .- x))
  end
end

function setcenter(d::MultinomialDesign{D, F}, x) where {D, F}
  offset = Position{D, F}(x...) .- designcenter(d)
  designs = map(d.designs) do design
    center = designcenter(design)
    return setcenter(design, center + offset)
  end
  return MultinomialDesign(d.photons, designs...)
end

function setalpha(d::MultinomialDesign{D, F}, alpha::Real) where {D, F}
  designs = setalpha.(d.designs, alpha)
  return MultinomialDesign(d.photons, designs...)
end

function scalealpha(d::MultinomialDesign{D, F}, scale::Real) where {D, F}
  designs = scalealpha.(d.designs, scale)
  return MultinomialDesign(d.photons, designs...)
end

function intensity(design::MultinomialDesign, param::Param)
  return map(d -> intensity(d, param), design.designs)
end

function loglikelihood(
  param::MultiParam{D, F},
  design::MultinomialDesign{D, F, N},
  obs::NTuple{N, Int},
  intensity = NanoLoc.intensity(design, param),
)::F where {D, F, N}
  if sum(obs) != design.photons
    typemin(F)
  else
    intensity_sum = sum(intensity)
    if intensity_sum <= zero(F)
      # @warn "Encountered zero total intensity in likelihood" maxlog = 1
      probs = ones(SVector{N, F}) ./ N
    else
      probs = SVector(intensity ./ intensity_sum)
    end
    logpdf(Multinomial(design.photons, probs), SVector(obs))
  end
end

function Base.rand(
  rng::AbstractRNG,
  design::MultinomialDesign{D, F, N},
  param::MultiParam{D, F},
  n::Integer,
) where {D, F, N}
  intensity = NanoLoc.intensity(design, param)
  total_intensity = sum(intensity)
  if total_intensity <= zero(F)
    @warn "Encountered zero total intensity in rand" maxlog = 1
    probs = fill(F(1 / N), N)
  else
    probs = collect(intensity ./ total_intensity)
  end

  observations = rand(rng, Multinomial(design.photons, probs), n)
  return NTuple{N, Int}.(eachcol(observations))
end

function randbg(
  rng::AbstractRNG,
  design::MultinomialDesign{D, F, N},
  param::MultiParam{D, F},
) where {D, F, N}
  signal_intensity = intensity(design, setnoise(param, 0))
  signal_intensity = Float64.(signal_intensity) # Distributions.jl expects Float64
  total_signal_intensity = sum(signal_intensity)
  total_intensity = total_signal_intensity + N * param.noise

  if total_intensity <= 0
    @warn "Encountered zero total intensity in randbg"
    bg = 0
    bgobs = zeros(Int, N)
  else
    bgprob = N * param.noise / total_intensity
    bg = rand(rng, Binomial(design.photons, bgprob))
    bgobs = rand(rng, Multinomial(bg, [1 / N for _ in 1:N]))
  end

  if total_signal_intensity <= zero(F)
    @warn "Encountered zero signal intensity in randbg"
    probs = fill(F(1 / N), N)
  else
    probs = collect(signal_intensity ./ total_signal_intensity)
  end

  signal_photons = design.photons - bg
  signal = rand(rng, Multinomial(signal_photons, probs))

  obs = NTuple{N, Int}(signal .+ bgobs)
  return (obs, bg)
end

Base.length(::MultinomialDesign{D, F, N, T}) where {D, F, N, T} = N

function Base.show(io::IO, design::MultinomialDesign{D, F, N}) where {D, F, N}
  return print(io, "MultinomialDesign($(design.photons) photons, $N positions)")
end

function Base.show(
  io::IO,
  ::MIME"text/plain",
  design::MultinomialDesign{D, F, N},
) where {D, F, N}
  print(
    io,
    "MultinomialDesign{$D, $F}($(design.photons) photons, $N positions):",
  )
  for d in design.designs
    print(io, "\n $(d.profile) at $(d.x)")
  end
end

function pp(design::MultinomialDesign)
  len = length(design)
  photons = design.photons

  subdesign = design.designs[1]
  prefix = "M$len-$photons"
  return prefix * pp(subdesign) * "..."
end

pp(::Type{<:MultinomialDesign}) = "Mult"

function pp(design::MultinomialDesign, obs)
  return join([pp(d, o) for (d, o) in zip(design.designs, obs)])
end
